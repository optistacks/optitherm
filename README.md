OptiTherm:
.NET C# library:
- library for calculating architectural window color and thermal properties
- holds classes DICT.Color and DICT.Thermal exported from StackAnalysis original
- defines class "SpectralData" to hold wavelength intensity data of measurements
- allows interpolation and reading various formats like PE, txt files etc.
- calculate color values based on spectral data and provided light sources and standards
- subdirectory ColorStandards: light sources / CIE observers and solar energy references
- build monolithic, double and triple glass units and calculate thermal performance based on EN and NFRC standards
- uses ISO 15099 (NFRC)  and EN410/637:2011 calculation procedures
---
external libraries:
* alglib.net V 3.18.2 (nuget) (https://www.alglib.net/download.php)

* Licence: GNU GPL 3.0+ see <http://www.gnu.org/licenses/>
