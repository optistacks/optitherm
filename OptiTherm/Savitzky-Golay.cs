using System;
using MathNet.Numerics.LinearAlgebra;

namespace OptiTherm
{
    /// <summary>
    /// <para>Implements a Savitzky-Golay smoothing filter, as found in [1].</para>
    /// <para>[1] Sophocles J.Orfanidis. 1995. Introduction to Signal Processing.
    /// Prentice-Hall, Inc., Upper Saddle River, NJ, USA.</para>
    /// </summary>
    public sealed class SavitzkyGolayFilter
    {
        private readonly int _sidePoints;

        private Matrix<double> _coefficients = null!;

        public SavitzkyGolayFilter(int sidePoints, int polynomialOrder)
        {
            this._sidePoints = sidePoints;
            Design(polynomialOrder);
        }

        /// <summary>
        /// Smooth the input data samples.
        /// </summary>
        /// <param name="samples">raw data array</param>
        /// <returns>smoothed array</returns>
        public double[] Process(double[] samples)
        {
            int length = samples.Length;
            double[] output = new double[length];
            int frameSize = (_sidePoints << 1) + 1;
            double[] frame = new double[frameSize];

            Array.Copy(samples, frame, frameSize);

            for (int i = 0; i < _sidePoints; ++i)
            {
                output[i] = _coefficients.Column(i).DotProduct(Vector<double>.Build.DenseOfArray(frame));
            }

            for (int n = _sidePoints; n < length - _sidePoints; ++n)
            {
                Array.ConstrainedCopy(samples, n - _sidePoints, frame, 0, frameSize);
                output[n] = _coefficients.Column(_sidePoints).DotProduct(Vector<double>.Build.DenseOfArray(frame));
            }

            Array.ConstrainedCopy(samples, length - frameSize, frame, 0, frameSize);

            for (int i = 0; i < _sidePoints; ++i)
            {
                output[length - _sidePoints + i] = _coefficients.Column(_sidePoints + 1 + i).DotProduct(Vector<double>.Build.Dense(frame));
            }

            return output;
        }

        private void Design(int polynomialOrder)
        {
            double[,] a = new double[(_sidePoints << 1) + 1, polynomialOrder + 1];

            for (int m = -_sidePoints; m <= _sidePoints; ++m)
            {
                for (int i = 0; i <= polynomialOrder; ++i)
                {
                    a[m + _sidePoints, i] = Math.Pow(m, i);
                }
            }

            Matrix<double> s = Matrix<double>.Build.DenseOfArray(a);
            _coefficients = s.Multiply(s.TransposeThisAndMultiply(s).Inverse()).Multiply(s.Transpose());
        }
    }
}

