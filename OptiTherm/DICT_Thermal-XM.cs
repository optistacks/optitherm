﻿/* ====================================================================
 *
 * Author: Anton Dietrich
 * Date:
 *
 * Copyright (C) 2008-2022 Anton Dietrich
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 Modifications:
 * 2011-05-25: changed Solar Radiation Table to EN410:1998 AM1 for EN std
 * 2011-07-29: modified gap distance access: count consistent from 1 for gastype and distance
 * 2011-07-30: add calculation of multi sheet optics based on ISO 15099
 * 2012-09-04: add EN410/639 2011 standard, switch to a ComboBox from the RB
 * 2022-03-19:	transfer module from PE-Thermal (2021) to OptiTherm
 * 2022-07-09:  update the ISO 13837 to 2021 version
 * 2024-01-03: update NFRC calculation module using MathNet.Numerics - remove alglib
 *
 *  ======================================================================
*/
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Globalization;
using MathNet.Numerics.LinearAlgebra;

namespace OptiTherm
{

    /// <summary>
    /// thermal standards available
    /// </summary>
    public enum ThermalStandard
    {
        [Description("EN 410:2011 / EN 673:2011")]
        EN410_673_2011,
        [Description("NFRC 2010")]
        NFRC_2001,
        [Description("EN 410:1998 / EN 673:1997")]
        EN410_673,
        [Description("GOST EN 410-2014")]
        GOST,
        [Description("ISO 9050:2003")]
        ISO_9050_2003,
        [Description("DIN 67507")]
        ISO_9050_90,
        [Description("VIG FOX 600")]
        VIG_FOX600,
        [Description("ISO 13837-2021")]
        ISO_13837,
        [Description("ISO 15099:2003")]
        ISO_15099
    };
    /// <summary>
    /// type of UV standard references
    /// </summary>
    public enum UVStandard { TUV, UVDF, SDF };
    /// <summary>
    /// filling gas types
    /// </summary>
    public enum FillingGas {
        Air, //!< Air filling
        Ar,  //!< Argon filling
        Kr, //!< Krypton filling
        Ar90Air10, //!< Ar 90% - Air 10%
        VIG //!< Vacuum Insulated Gap
    };
    /// <summary>
    /// types of glass units
    /// </summary>
    public enum UnitType {
        Mono, //!< coated monolithic glass
        Mate, //!< uncoated glass
        Lami, //!< laminated glass unit
        IGU,  //!< double glazing unit
        Triple //!< triple glazing unit
    };
    /// <summary>
    /// thermal units for reporting: metric (SI) or BTU (IP)
    /// </summary>
    public enum ThermalUnits { SI, IP };        // thermal reporting units for U-value

    /// <summary>
    /// defines properties for the VIG spacers
    /// </summary>
    public static class VIG_spacer
    {
        // all dimensions in mm
        /// <summary>
        /// lenght of spacer in mm
        /// </summary>
        public static double lenght = 0.55;
        /// <summary>
        /// width of spcaer in mm
        /// </summary>
        public static double width = 0.55;
        /// <summary>
        /// height of spacer in mm
        /// </summary>
        public static double height = 0.3;
        /// <summary>
        /// thermal conductance of spacer in W /(m °K): default Borosilicate
        /// </summary>
        public static double lambda = 1.2;
        /// <summary>
        /// distance between spacer posts in m: default 40 mm
        /// </summary>
        public static double spacer_distance = 0.040;
        /// <summary>
        /// pressure in gap: in Pa
        /// </summary>
        public static double pressure = 0.01;
    }

    /// <summary>
    /// helper class holds filling gas properties for Ar/ Air based on ISO15099:2003
    /// </summary>
    public static class GasProperties
    {
        const double GasConstant = 8.31441e3; //!< universal gas constant J/kmol
        /// <summary>
        /// gas properties: thermal conductance
        /// </summary>
        /// <param name="gas">type of filling gas</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double lambda(FillingGas gas, double Temp)
        {
            double val;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 2.285e-3 + (Temp) * 5.149e-5;
                    break;
                case FillingGas.Air:
                    val = 2.873e-3 + (Temp) * 7.76e-5;
                    break;
                case FillingGas.Kr:
                    val = 9.443e-4 + (Temp) * 2.826e-5;
                    break;
                case FillingGas.Ar90Air10:
                    // calculation based on ISO 15099
                    var l_Ar = 2.285e-3 + (Temp) * 5.149e-5;
                    var l_Air = 2.873e-3 + (Temp) * 7.76e-5;
                    var M_Ar = Mweight(FillingGas.Ar);
                    var M_Air = Mweight(FillingGas.Air);
                    var ld_Ar = 15.0 / 4.0 * GasConstant / M_Ar * mue(FillingGas.Ar, Temp);
                    var ld_Air = 15.0 / 4.0 * GasConstant / M_Air * mue(FillingGas.Air, Temp);
                    var psi_Ar = Math.Pow(1.0 + Math.Pow(ld_Ar / ld_Air, 0.5) * Math.Pow(M_Ar / M_Air, 0.25), 2.0) / (2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + M_Ar / M_Air, 0.5));
                    psi_Ar *= 1.0 + 2.41 * ((M_Ar - M_Air) * (M_Ar - 0.142 * M_Air)) / Math.Pow(M_Ar + M_Air, 2.0);
                    var ldmix = ld_Ar / (1.0 + psi_Ar * 0.1 / 0.9);
                    var ldd_Ar = l_Ar - ld_Ar;
                    // calc for Air
                    var psi_Air = Math.Pow(1.0 + Math.Pow(ld_Air / ld_Ar, 0.5) * Math.Pow(M_Air / M_Ar, 0.25), 2.0) / (2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + M_Air / M_Ar, 0.5));
                    psi_Air *= 1.0 + 2.41 * ((M_Air - M_Ar) * (M_Air - 0.142 * M_Ar)) / Math.Pow(M_Air + M_Ar, 2.0);
                    ldmix += ld_Air / (1.0 + 0.9 / 0.1 * psi_Air);
                    var ldd_Air = l_Air - ld_Air;
                    // calculate the l'' mix values
                    var phi_Ar = Math.Pow(1.0 + Math.Pow(ld_Ar / ld_Air, 0.5) * Math.Pow(M_Ar / M_Air, 0.25), 2.0);
                    phi_Ar /= (2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + M_Ar / M_Air, 0.5));
                    var lddmix = ldd_Ar / (1.0 + phi_Ar * 0.1 / 0.9);
                    var phi_Air = Math.Pow(1.0 + Math.Pow(ld_Air / ld_Ar, 0.5) * Math.Pow(M_Air / M_Ar, 0.25), 2.0);
                    phi_Air /= (2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + M_Air / M_Ar, 0.5));
                    lddmix += ldd_Air / (1.0 + 0.9 / 0.1 * phi_Air);
                    val = ldmix + lddmix;
                    //val = 0.9 * (2.285e-3 + (Temp) * 5.149e-5) + 0.1 * (2.873e-3 + (Temp) * 7.76e-5);
                    break;
                default:
                    val = 2.873e-3 + (Temp) * 7.76e-5;
                    break;
            }
            return val;
        }

        /// <summary>
        /// dynamic viscosity of filling gas
        /// </summary>
        /// <param name="gas">gast type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double mue(FillingGas gas, double Temp)
        {
            double val;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 3.379e-6 + Temp * 6.451e-8;
                    break;
                case FillingGas.Air:
                    val = 3.723e-6 + Temp * 4.94e-8;
                    break;
                case FillingGas.Kr:
                    val = 2.213e-6 + Temp * 7.777e-8;
                    break;
                case FillingGas.Ar90Air10:
                    // calculation based on ISO 15099
                    var mueAr = 3.379e-6 + Temp * 6.451e-8;
                    var mueAir = 3.723e-6 + Temp * 4.94e-8;
                    var MAr_Air = Mweight(FillingGas.Ar) / Mweight(FillingGas.Air);
                    var phii = Math.Pow((1.0 + Math.Pow(mueAr / mueAir, 0.5) * Math.Pow(1.0 / MAr_Air, 0.25)), 2.0);
                    phii /= 2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + MAr_Air, 0.5);
                    val = mueAr / (1.0 + phii * 0.1 / 0.9);
                    phii = Math.Pow((1.0 + Math.Pow(mueAir / mueAr, 0.5) * Math.Pow(MAr_Air, 0.25)), 2.0);
                    phii /= 2.0 * Math.Pow(2.0, 0.5) * Math.Pow(1.0 + 1.0 / MAr_Air, 0.5);
                    val += mueAir / (1.0 + phii * 0.9 / 0.1);
                    //val = 0.9 * (3.379e-6 + Temp * 6.451e-8) + 0.1 * (3.723e-6 + Temp * 4.94e-8);
                    break;
                default:
                    val = 3.723e-6 + Temp * 4.94e-8;
                    break;
            }
            return val;
        }

        static double Mweight(FillingGas gas)
        {
            double MW;
            switch (gas)
            {
                case FillingGas.Ar:
                    MW = 39.948;
                    break;
                case FillingGas.Air:
                    MW = 28.97;
                    break;
                case FillingGas.Kr:
                    MW = 83.80;
                    break;
                case FillingGas.Ar90Air10:
                    // return mixture molecualr mass
                    MW = 0.9 * 39.948 + 0.1 * 28.97;
                    break;
                default:
                    MW = 28.97;
                    break;
            }
            return MW;
        }

        /// <summary>
        /// density of filling gas
        /// </summary>
        /// <param name="gas">type of gas</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double rho(FillingGas gas, double Temp)
        {
            // calculated from molecular weight using GasConstant R at Temperature
            double rho;
            if (gas == FillingGas.Ar90Air10)
            {
                rho = 0.9 * Mweight(FillingGas.Ar) * 101300 / Temp / GasConstant;
                rho += 0.1 * Mweight(FillingGas.Air) * 101300 / Temp / GasConstant;
            }
            else
            {
                var MW = Mweight(gas);
                rho = 101300 * MW / Temp / GasConstant;

            }
            return rho;
        }

        /// <summary>
        /// specific heat of filling gas
        /// </summary>
        /// <param name="gas">gas type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double specHeat(FillingGas gas, double Temp)
        {
            double val;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 521.9285;
                    break;
                case FillingGas.Air:
                    val = 1002.737 + 1.2324e-2 * Temp;
                    break;
                case FillingGas.Kr:
                    val = 248.0907;
                    break;
                case FillingGas.Ar90Air10:
                    // ISO 15099 procedure
                    var Mmix = Mweight(FillingGas.Ar90Air10);
                    var cpdAr = Mweight(FillingGas.Ar) * 521.9285;
                    var cpdAir = Mweight(FillingGas.Air) * 1002.737 + 1.2324e-2 * Temp;
                    var cpdMix = 0.9 * cpdAr + 0.1 * cpdAir;
                    var cpMIx = cpdMix / Mmix;
                    val = cpMIx;
                    break;
                default:        // just use Air
                    val = 1002.737 + 1.2324e-2 * Temp;
                    break;
            }
            return val;
        }

        /// <summary>
        /// thermal conducitivty of filling gas: EN standards
        /// </summary>
        /// <param name="gas">gas type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double lambda_EN(FillingGas gas, double Temp)
        {
            double val;
            double tc = Temp - 273;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 1.634 + (tc) * 0.005;
                    break;
                case FillingGas.Air:
                    val = 2.416 + (tc) * 0.008;
                    break;
                case FillingGas.Kr:
                    val = 0.8704 + (tc) * 0.00282;
                    break;
                case FillingGas.Ar90Air10:
                    val = 0.9 * (1.634 + (tc) * 0.005) + 0.1 * (2.416 + (tc) * 0.008);
                    break;
                default:
                    val = 1.634 + (tc) * 0.005;
                    break;
            }
            return val * 0.01;
        }


        /// <summary>
        /// viscosity of filling gas EN standard
        /// </summary>
        /// <param name="gas">gas type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double mue_EN(FillingGas gas, double Temp)
        {
            double val;
            double tc = Temp - 273;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 2.1011 + tc * 0.00633;
                    break;
                case FillingGas.Air:
                    val = 1.711 + tc * 0.005;
                    break;
                case FillingGas.Kr:
                    val = 2.33 + tc * 0.007;
                    break;
                case FillingGas.Ar90Air10:
                    val = 0.9 * (2.1011 + tc * 0.00633) + 0.1 * (1.711 + tc * 0.005);
                    break;
                default:
                    val = 1.711 + tc * 0.005;
                    break;
            }
            return val * 1.0e-5;
        }


        /// <summary>
        /// specific heat of filling gas: EN standard
        /// </summary>
        /// <param name="gas">gas type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double specHeat_EN(FillingGas gas, double Temp)
        {
            double val;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 519.0;
                    break;
                case FillingGas.Air:
                    val = 1008;
                    break;
                case FillingGas.Kr:
                    val = 245;
                    break;
                case FillingGas.Ar90Air10:
                    val = 0.9 * 519 + 0.1 * (1008);
                    break;
                default:        // just use Air
                    val = 1008;
                    break;
            }
            return val;
        }


        /// <summary>
        /// density filling gas: EN standard
        /// </summary>
        /// <param name="gas">gas type</param>
        /// <param name="Temp">temperature</param>
        /// <returns></returns>
        public static double rho_EN(FillingGas gas, double Temp)
        {
            double val;
            double tc = Temp - 273;
            switch (gas)
            {
                case FillingGas.Ar:
                    val = 1.764 - tc * 0.0063;
                    break;
                case FillingGas.Air:
                    val = 1.2788 - tc * 0.00456;
                    break;
                case FillingGas.Kr:
                    val = 3.6948 - tc * 0.01336;
                    break;
                case FillingGas.Ar90Air10:
                    val = 0.9 * (1.764 - tc * 0.0063) + 0.1 * (1.2788 - tc * 0.00456);
                    break;
                default:
                    val = 1.2788 - tc * 0.00456;
                    break;
            }
            return val;
        }


    }

    /// <summary>
    /// standardized reference for calculation: defines type and boundary conditions
    /// </summary>
    public static class TCalcStandard
    {
        /// <summary>
        /// zero Kelvin
        /// </summary>
        public const double KelvinZero = 273.16;    // zero K in C scale
                                                    //default boundary conditions based on ISO 9050-300
        public static ThermalStandard CurrentStandard = ThermalStandard.ISO_9050_2003;
        public static string Name = GetDescriptionFromEnumValue(CurrentStandard); // "ISO 9050 - 2003";
        /// <summary>
        /// inside temperature
        /// </summary>
        public static double Tin_U = 290.5;     // inside Temperature K for U-Value calculation
        /// <summary>
        /// outside temperature
        /// </summary>
        public static double Tout_U = 275.5;   // oustide Temperature K U-Value
        /// <summary>
        /// outside heat transfer coefficient W/m^2/K
        /// </summary>
        public static double he_U = 23.0;
        /// <summary>
        /// inside convection coefficient W/m^2/K
        /// </summary>
        public static double hi_U = 3.6;
        /// <summary>
        /// inside radiative coefficient in W/m^2/K
        /// </summary>
        public static double hi_R = 4.1;
        /// <summary>
        /// inside Temperature for SHGC
        /// </summary>
        public static double Tin_SHG = 290.5;
        /// <summary>
        /// outside temperature for SHGC
        /// </summary>
        public static double Tout_SHG = 275.5;
        /// <summary>
        /// default inside heat transfer: actual is calculated
        /// </summary>
        public static double hi_SHG = 3.6;
        /// <summary>
        /// outside heat transfer coefficient for SHGC
        /// </summary>
        public static double he_SHG = 23.0;
        /// <summary>
        /// incident solar energy  in W/m^2
        /// </summary>
        public static double SolarE = 500;
        /// <summary>
        /// file name holding solar energy incidence
        /// </summary>
        public static string SolarFile = "Solar-ISOAM15";
        /// <summary>
        /// file for table for TUV calculations
        /// </summary>
        public static string UVTable = "TUV-ISOAM15";
        /// <summary>
        /// thermal units used for U-value: SI or IU
        /// </summary>
        public static ThermalUnits Units = ThermalUnits.SI;
        /// <summary>
        /// flag indicating ISO /EN calculation
        /// </summary>
        public static bool ISOcalc = true;
        /// <summary>
        /// conversion metric values to BTU
        /// </summary>
        internal const double MetricBTU = 0.1761;

        /// <summary>
        /// outside temperature of unit
        /// </summary>
        public static double ThermalOutsideTemp_U
        {
            get { return Tout_U - 273.16; }
            set { Tout_U = value + 273.16; }
        }

        /// <summary>
        /// inside temperature of unit
        /// </summary>
        public static double ThermalInsideTemp_U
        {
            get { return Tin_U - 273.16; }
            set { Tin_U = value + 273.16; }
        }

        /// <summary>
        /// adjust summer outside convection coefficient based on windspeed data
        /// this is defined only for NFRC and ISO-13837 cases
        /// all others stick with default settings
        /// </summary>
        public static double ThermalWindspeed_SHG
        {
            get
            {
                var ws = CurrentStandard == ThermalStandard.NFRC_2001 ? (he_SHG - 4.0) / 4.0 : (he_SHG - 5.6) / 3.9;
                return ws > 0 ? ws : 0.0;
            }
            set 
            {
                he_SHG = CurrentStandard switch
                {
                    ThermalStandard.NFRC_2001 => 4.0 * value * 4.0,
                    ThermalStandard.ISO_13837 => value > 5.0 ? 7.2 * Math.Pow(value, 0.78) : 21,
                    _ => he_SHG,  // other cases keep the default 
                };
            }
        }

        /// <summary>
        /// adjust winter outside convection coefficient based on windspeed data
        /// this is defined only for NFRC and ISO-13837 cases
        /// all others stick with default settings
        /// </summary>
        public static double ThermalWindspeed_U
        {
            get
            {
                var ws = CurrentStandard == ThermalStandard.NFRC_2001 ? (he_U - 4.0) / 4.0 : (he_U - 5.6) / 3.9;
                return ws > 0 ? ws : 0.0;
            }
            set
            {
                he_U = CurrentStandard switch
                {
                    ThermalStandard.NFRC_2001 => 4.0 * value * 4.0,
                    ThermalStandard.ISO_13837 => value > 5.0 ? 7.2 * Math.Pow(value, 0.78) : 21,
                    _ => he_U,  // other cases keep the default 
                };
            }
        }

        /// <summary>
        /// holding data for selected thermal standard
        /// </summary>
        public static ThermalStandard CalculationStandard
        {
            // get / set the calculation standard
            get { return TCalcStandard.CurrentStandard; }
            set
            {
                if (value == TCalcStandard.CurrentStandard)
                {
                    return;
                }

                switch (value)
                {
                    case ThermalStandard.NFRC_2001:  // set the data for NFRC 2001
                        TCalcStandard.CurrentStandard = ThermalStandard.NFRC_2001;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value); // "NFRC 2001";
                        TCalcStandard.Tin_U = KelvinZero + 21.0;   // 21 C
                        TCalcStandard.Tout_U = KelvinZero - 18.0; // -18C
                        TCalcStandard.hi_U = 6.5;        // free convection is calculated internally
                        TCalcStandard.he_U = 26.0;       // convection: Ws = 5.5 m/s -> 4 + 4 * 5.5 = 26
                        // summer conditions
                        TCalcStandard.Tin_SHG = KelvinZero + 24.0;     // 24 °C -> 297K
                        TCalcStandard.Tout_SHG = KelvinZero + 32.0;    // 32 °C -> 305 K
                        TCalcStandard.hi_SHG = 4.0;      // natural convection: is calculated
                        TCalcStandard.he_SHG = 15.0;       // based on 2.75 m/sec windspeed
                        TCalcStandard.SolarE = 783.0;
                        TCalcStandard.SolarFile = "Solam15Nrml"; // ASTME891-87 -> "Solam15Nrml" - iso USA  ISO 13837 -> "SolarAM15"
                        TCalcStandard.UVTable = "TUV-ISOAM15";
                        TCalcStandard.Units = ThermalUnits.IP;
                        TCalcStandard.ISOcalc = false;
                        break;
                    case ThermalStandard.VIG_FOX600:     // conditions defined in ISO 15099:2003
                        TCalcStandard.CurrentStandard = ThermalStandard.VIG_FOX600;
                        TCalcStandard.Name = "ISO 20K fixed no conv.";
                        TCalcStandard.Tin_U = 293;   // 20 C
                        TCalcStandard.Tout_U = 273; // 0 C
                        TCalcStandard.hi_U = 0.0;    // this will be calculated
                        TCalcStandard.he_U = 0.0;   // convection
                                                    // summer conditions
                        TCalcStandard.Tin_SHG = 297;
                        TCalcStandard.Tout_SHG = 305;
                        TCalcStandard.hi_SHG = 2.5;
                        TCalcStandard.he_SHG = 15.0;
                        TCalcStandard.SolarE = 0;    // 500.0;
                        TCalcStandard.SolarFile = "Solam15Nrml";
                        TCalcStandard.UVTable = "TUV-ISOAM15";
                        TCalcStandard.Units = ThermalUnits.IP;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.EN410_673:      // conditions defined in EN410 ISO 637:1997
                    case ThermalStandard.GOST:           // GOST has the same base definitions, but may vary Tout and he
                        TCalcStandard.CurrentStandard = ThermalStandard.EN410_673;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value);
                        TCalcStandard.Tin_U = 290.5;   // 15 C
                        TCalcStandard.Tout_U = 275.5; // 0 C
                        TCalcStandard.hi_R = 4.4;       // radiativ coeeficient intern
                        TCalcStandard.hi_U = 3.6;    // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_U = 23.0;   // outside convection based on wind speed
                        TCalcStandard.Tin_SHG = 290.5;
                        TCalcStandard.Tout_SHG = 275.5;
                        TCalcStandard.hi_SHG = 3.6; // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_SHG = 23.0;
                        TCalcStandard.SolarE = 500.0;
                        // "SolarAM1EN" -> EN673-1994 , "Solar-ISOAM15" ISO 9050-2003
                        TCalcStandard.SolarFile = "SolarAM1EN";
                        TCalcStandard.UVTable = "UV-EN410AM10";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.ISO_9050_2003:     // conditions defined in ISO9050 2003
                        TCalcStandard.CurrentStandard = ThermalStandard.ISO_9050_2003;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value);
                        TCalcStandard.Tin_U = 290.5;   // 15 C
                        TCalcStandard.Tout_U = 275.5; // 0 C
                        TCalcStandard.hi_R = 4.1;       // radiativ coeeficient intern
                        TCalcStandard.hi_U = 3.6;    // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_U = 23.0;   // convection
                        TCalcStandard.Tin_SHG = 290.5;
                        TCalcStandard.Tout_SHG = 275.5;
                        TCalcStandard.hi_SHG = 3.6; // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_SHG = 23.0;
                        TCalcStandard.SolarE = 500.0;
                        TCalcStandard.SolarFile = "Solar-ISOAM15";
                        TCalcStandard.UVTable = "TUV-ISOAM15";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.ISO_9050_90:     // conditions defined in ISO9050 1st edition 1990
                        TCalcStandard.CurrentStandard = ThermalStandard.ISO_9050_90;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value); // "ISO 9050 - 1990 (DIN 67507)";
                        TCalcStandard.Tin_U = 290.5;   // 15 C
                        TCalcStandard.Tout_U = 275.5; // 0 C
                        TCalcStandard.hi_R = 4.4;       // radiativ coeeficient intern
                        TCalcStandard.hi_U = 3.6;    // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_U = 23.0;   // convection
                        TCalcStandard.Tin_SHG = 290.5;
                        TCalcStandard.Tout_SHG = 275.5;
                        TCalcStandard.hi_SHG = 3.6; // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_SHG = 23.0;
                        TCalcStandard.SolarE = 500.0;
                        TCalcStandard.SolarFile = "SolarAM1";
                        TCalcStandard.UVTable = "SolarUV_EU";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.ISO_13837:      // conditions defined in ISO13837:2021 Method A (Vehicle glazing)
                        TCalcStandard.CurrentStandard = ThermalStandard.ISO_13837;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value);
                        TCalcStandard.Tin_U = 290.5;   // 15 C
                        TCalcStandard.Tout_U = 275.5; // 0 C
                        TCalcStandard.hi_R = 4.4;       // radiativ coeeficient intern
                        TCalcStandard.hi_U = 3.6;    //  this convective heat transfer intern
                        TCalcStandard.he_U = 21.0;   // convection ouside can be calculated using windspeed settings: defulat is 4 m/min
                        TCalcStandard.Tin_SHG = 290.5;
                        TCalcStandard.Tout_SHG = 275.5;
                        TCalcStandard.hi_SHG = 3.6; // this is hr for uncoated glass: hc is constant 3.6
                        TCalcStandard.he_SHG = 21.0;
                        TCalcStandard.SolarE = 500.0;
                        TCalcStandard.SolarFile = "Solar-ISOAM15";
                        TCalcStandard.UVTable = "TUV-ISOAM15";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.EN410_673_2011:      // conditions defined in EN410:2011 and EN673:2011-04
                        TCalcStandard.CurrentStandard = ThermalStandard.EN410_673_2011;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value); 
                        TCalcStandard.Tin_U = 290.5;   // 15 C
                        TCalcStandard.Tout_U = 275.5; // 0 C
                        TCalcStandard.hi_R = 4.1;       // radiativ coefficient intern
                        TCalcStandard.hi_U = 3.6;    // this convective heat transfer intern
                        TCalcStandard.he_U = 25.0;   // convection -> changed from 23.0
                        TCalcStandard.Tin_SHG = 290.5;
                        TCalcStandard.Tout_SHG = 275.5;
                        TCalcStandard.hi_SHG = 3.6; // this is hcv convective heat transfer
                        TCalcStandard.he_SHG = 25.0;
                        TCalcStandard.SolarE = 500.0;
                        // "SolarAM1EN" -> EN673-1994 , "Solar-ISOAM15" ISO 9050-2003
                        TCalcStandard.SolarFile = "SolarAM1EN";
                        TCalcStandard.UVTable = "UV-EN410-11";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                    case ThermalStandard.ISO_15099:      // conditions defined in ISO 15099:2003
                        TCalcStandard.CurrentStandard = ThermalStandard.ISO_15099;
                        TCalcStandard.Name = GetDescriptionFromEnumValue(value); 
                        TCalcStandard.Tin_U = KelvinZero + 20;   // 20 C
                        TCalcStandard.Tout_U = KelvinZero + 0; // 0 C
                        TCalcStandard.hi_R = 4.1;       // radiativ coeeficient intern
                        TCalcStandard.hi_U = 3.6;    // this is hcv convective heat transfer
                        TCalcStandard.he_U = 20.0;   // convection outside
                        TCalcStandard.Tin_SHG = 273.16 + 25; // Tin 25°C
                        TCalcStandard.Tout_SHG = 273.16 + 30; // Tout  30°C
                        TCalcStandard.hi_SHG = 2.5; // this is hcv convective heat transfer
                        TCalcStandard.he_SHG = 8.0;
                        TCalcStandard.SolarE = 500.0;
                        TCalcStandard.SolarFile = "SolarAM1EN";
                        TCalcStandard.UVTable = "UV-EN410-11";
                        TCalcStandard.Units = ThermalUnits.SI;
                        TCalcStandard.ISOcalc = true;
                        break;
                }
            }
        }


        /// <summary>
        /// get the description from an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionFromEnumValue(Enum value)
        {
            var attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }


        /// <summary>
        /// get enum value from a description text
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="description"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static T? GetEnumValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException();
            }

            FieldInfo[] fields = type.GetFields();
            var field = (from f in fields
                         from a in f.GetCustomAttributes(typeof(DescriptionAttribute), false)
                         select new
                         {
                             Field = f,
                             Att = a
                         } into a
                         where ((DescriptionAttribute)a.Att).Description == description
                         select a).SingleOrDefault();
            return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        }
    }

    /// <summary>
    /// abstract base class as template for glass assembly
    /// </summary>
    public abstract class GlassUnit
    {
        /// <summary>
        /// IGU unit type: mono, IG, Lami, triple
        /// </summary>
        protected UnitType MeType = UnitType.Mono;
        /// <summary>
        /// IGU unit type: mono, IG, Lami, triple
        /// </summary>
        public UnitType TypeOfUnit { get { return MeType; } }
        protected SpectralData? Tran, R_out, R_in; // spectral characteristics outer / inner surface
        protected CIELightSources LS = CIELightSources.C; // use a default lightsource and observer
        protected CIEObservers OBS = CIEObservers.CIE_1931;
        /// <summary>
        /// thickness of substrate
        /// </summary>
        protected double SubstrateThickness = 0.04;  // glass thickness in m: just a default value
        /// <summary>
        /// name of substarte
        /// </summary>
        protected string SubstrateName = "Clear";
        /// <summary>
        /// title of unit
        /// </summary>
        protected string UName = "Mono";
        /// <summary>
        /// design on front / outside
        /// </summary>
        protected string FrontCoat = "none";    // name of coatings: for each surface one (default none)
        /// <summary>
        /// design on back/inside
        /// </summary>
        protected string BackCoat = "none";

        // some calculation constants
        /// <summary>
        /// Stefan Bolzmann constant W/(m^2K^4)
        /// </summary>
        protected const double SigmaB = 5.6697e-8;
        /// <summary>
        /// heat conductance float glass W/m/K
        /// </summary>
        protected const double LamdaGlass = 1.0;
                                                   // reference tables for calculating solar performance data
        /// <summary>
        /// reference table spectral data for visible properties
        /// </summary>
        protected static ReferenceTable? ISO_Visible;
        /// <summary>
        /// reference table spectral data for solar properties
        /// </summary>
        protected static ReferenceTable? SolarRadiation = null;
        /// <summary>
        ///  reference table spectral data for UV properties
        /// </summary>
        protected static ReferenceTable? UVRadiation = null;
        // charactersistics for the multi glass unit: these are calculated whenever the IGU is instantiated
        /// <summary>
        /// holds the individual glasses of a multi glass unit: inside to outside
        /// </summary>
        public List<GlassUnit> Components = new List<GlassUnit>();
        /// <summary>
        /// this is a list of air gaps (m), inside -> outside
        /// </summary>
        protected List<double> gaps = new List<double>();
        /// <summary>
        /// default FillingGas gas for gaps
        /// </summary>
        protected List<FillingGas> gas = new List<FillingGas>();
        /// <summary>
        /// array holding solar absorptance for each component
        /// </summary>
        protected double[]? absSolar;
        // parameters for database stoarge of measured spectra and results
        /// <summary>
        /// name of coater
        /// </summary>
        public string CoaterName = "NA";
        /// <summary>
        /// production date
        /// </summary>
        public DateTime ProdDate = DateTime.Now;
        /// <summary>
        /// batch number
        /// </summary>
        protected int BatchID = 0;  // place holder for the LoadID

        /// <summary>
        /// holds a set of color values for a glass unit: T/Rfront/Rback
        /// </summary>
        public ColorSet? UnitColor
        {
            get
            {
                if (Tran == null || R_out == null || R_in == null)
                {
                    return null;
                }
                else
                {
                    return new ColorSet(GetCIETran, GetCIERin, GetCIERout);
                }
            }
        }


        /// <summary>
        /// static class to get references to the table file names for Illuminants and Observers
        /// uses an automatic file extension ".ref"
        /// </summary>
        protected static class ISO9050Tables
        {
            public const string SpectralPowerD65 = "SpectralPwrD65";
            public const string TUV_ISO = "TUV-ISOAM15", UVDF_ISO = "UVDF-ISO", UV_SDF_ISO = "SKINUV-ISO";
            public const string FloatIndex = "Float-ndx";   // float glass index file
        }


        /// <summary>
        /// calculation standard name
        /// </summary>
        public static string CalcStdName { get { return TCalcStandard.Name; } }

        // make the spectral data available as read only
        /// <summary>
        /// spectral data of transmission
        /// </summary>
        public SpectralData TR_Spd
        {
            get { return Tran; }
        }
        /// <summary>
        /// spectral data for reflection outside
        /// </summary>
        public SpectralData RO_Spd
        {
            get { return R_out; }
        }
        /// <summary>
        /// spectral data for reflection inside
        /// </summary>
        public SpectralData RI_Spd
        {
            get { return R_in; }
        }



        /// <summary>
        /// get / set the color properties in transmission
        /// </summary>
        public LabYData GetCIETran
        {
            get
            {
                LabYData cs = Tran.CIEcolor(LS, OBS);
                cs.CRI = CIEColor.CRI_EN410(Tran);  // get the CRI value
                return cs;
            }
        }

        /// <summary>
        /// get / set the color properties in reflection outside
        /// </summary>
        public LabYData GetCIERout
        {
            get
            {
                LabYData cs = R_out.CIEcolor(LS, OBS);
                cs.CRI = 0;     //CIEColor.CRI_EN410(R_out);	// get the CRI value
                return cs;
            }
        }

        /// <summary>
        /// get / set the color properties in reflection inside
        /// </summary>
        public LabYData GetCIERin
        {
            get
            {
                LabYData cs = R_in.CIEcolor(LS, OBS);
                cs.CRI = 0;     //CIEColor.CRI_EN410(R_in);	// get the CRI value
                return cs;
            }
        }

        /// <summary>
        /// CIE observer type
        /// </summary>
        public CIEObservers CIE_Observer
        {
            get { return OBS; }
            set { OBS = value; }
        }

        /// <summary>
        /// CIE light source type
        /// </summary>
        public CIELightSources CIE_Illuminant
        {
            get { return LS; }
            set { LS = value; }
        }
        /// <summary>
        /// set scaling in all spectral data to generic 0-1 format
        /// </summary>
        protected void MakeSpectraGeneric()
        {
            // make sure spectral data of glass sheets are in generic 0-1 form:
            for (int i = 0; i < Components.Count; i++)
            {
                if (Components[i].TR_Spd.IntensityPercent)
                {
                    Components[i].TR_Spd.IntensityPercent = false;
                }

                if (Components[i].RO_Spd.IntensityPercent)
                {
                    Components[i].RO_Spd.IntensityPercent = false;
                }

                if (Components[i].RI_Spd.IntensityPercent)
                {
                    Components[i].RI_Spd.IntensityPercent = false;
                }
            }
        }

        /// <summary>
        /// extrapolite the spectral data to the wavelength rnage supplied
        /// </summary>
        /// <param name="unit">a glass unit</param>
        /// <param name="wlRng"> wavelength range to cover</param>
        /// <returns>a GlassUnit (MonoUnit or MateGlass)</returns>
        protected static GlassUnit Extrapolite(GlassUnit unit, double[] wlRng)
        {

            double[] trn = new double[wlRng.Length];
            double[] rgn = new double[wlRng.Length];
            double[] rfn = new double[wlRng.Length];
            for (int i = 0; i < wlRng.Length; i++)
            {
                trn[i] = unit.TR_Spd.GetYvalPercent(wlRng[i]);
                rgn[i] = unit.RO_Spd.GetYvalPercent(wlRng[i]);
                rfn[i] = unit.RI_Spd.GetYvalPercent(wlRng[i]);
            }
            var mdt = new SpectralData(wlRng, trn, unit.TR_Spd.AName, ColorType.TR, true);    // create a spectral data structure
            var mdr = new SpectralData(wlRng, rgn, unit.RO_Spd.AName, unit.RO_Spd.TypeOfSpectra, true);    // create a spectral data structure
            var mdf = new SpectralData(wlRng, rfn, unit.RI_Spd.AName, unit.RI_Spd.TypeOfSpectra, true);
            mdr.Eps_Hemi = unit.RO_Spd.Eps_Hemi;
            mdr.IsRFilm = unit.RO_Spd.IsRFilm;
            mdf.IsRFilm = unit.RI_Spd.IsRFilm;
            mdf.Eps_Hemi = unit.RI_Spd.Eps_Hemi;
            GlassUnit nip;
            if (unit.TypeOfUnit == UnitType.Mate)
            {
                nip = new MateGlass(mdt, mdr, unit.UnitName, unit.UnitThickness);
            }
            else
            {
                nip = new MonoUnit(mdt, mdr, mdf, unit.UnitName,
                                   unit.SubName, unit.UnitThickness)
                {
                    LoadID = unit.LoadID,
                    ProdDate = unit.ProdDate,
                    CoaterName = unit.CoaterName
                };
            }
            nip.FrontCoating = unit.FrontCoating;
            nip.BackCoating = unit.BackCoating;
            return nip;     // return the adjusted GlassUnit
        }

        double LookUpWl(double wl, double[] wvl, double[] Yv)
        {
            // obtain the corresponding Y value for a given wvl: if needed extrapolate
            // assumes wvl && Yv are same length and sorted
            if (wvl.Length != Yv.Length)
            {
                return 0;
            }
            // return limits if wl is outside the boundaries
            if (wl >= wvl[wvl.Length - 1])
            {
                return (double)Yv[Yv.Length - 1];
            }

            if (wl <= wvl[0])
            {
                return (double)Yv[0];
            }
            // get the value if wl matches a value
            int pos = Array.IndexOf(wvl, wl);
            if (pos >= 0)
            {
                return (double)Yv[pos];
            }
            // otherwise we have to extrapolate:
            for (int i = 0; i < wvl.Length; i++)
            {
                if (wvl[i] >= wl)
                {
                    pos = i;    // remember index
                    break;  // just search the next largest X value
                }
            }
            double rval = (Yv[pos] - Yv[pos - 1]) / (wvl[pos] - wvl[pos - 1]) * (wl - wvl[pos - 1]) + Yv[pos - 1];
            return rval;
        }


        /// <summary>
        /// calculate multi sheet optical characteristics
        /// based on ISO 15099:2003 pp60
        /// </summary>
        /// <param name="units">list of sheets in to out</param>
        /// <param name="TR">out T</param>
        /// <param name="Rout">out Rfront</param>
        /// <param name="Rin">our Rback</param>
        /// <param name="absUsolar">out solar absorptance</param>
        internal void CalcMultiSheetOptics(List<GlassUnit> units, out SpectralData TR, out SpectralData Rout,
                                            out SpectralData Rin, out double[] absUsolar)
        {
            // prepare the arrays:
            double[] rfU = new double[units.Count];
            double[] rbU = new double[units.Count];
            double[] tauU = new double[units.Count];
            double[] Iplus = new double[units.Count + 1];
            double[] Iminus = new double[units.Count + 1];
            double[] rft = new double[units.Count + 1];
            double[] rbk = new double[units.Count + 1];
            double[] tint = new double[units.Count + 1];
            // take the first unit in the list to generate the wavelength steps
            double[] wvl = new double[units[0].Tran.Elements];
            double[] TRA = new double[units[0].Tran.Elements];
            double[] R_out = new double[units[0].Tran.Elements];
            double[] R_in = new double[units[0].Tran.Elements];
            // create arrays for the absorption for each individual lite
            double[][] ABS = new double[units.Count][];
            for (int i = 0; i < units.Count; i++)
            {
                ABS[i] = new double[units[0].Tran.Elements];
            }
            double[] absU = new double[units.Count];
            MakeSpectraGeneric();
            // goto through all wavelengts
            for (int w = 0; w < units[0].Tran.Elements; w++)
            {
                wvl[w] = units[0].Tran[w];      // get the wavelength
                for (int i = 0; i < units.Count; i++)
                {
                    // get the characteristics for unit i at wavelength wvl
                    tauU[i] = units[i].Tran.GetYval(wvl[w]);
                    rfU[i] = units[i].RO_Spd.GetYval(wvl[w]);
                    rbU[i] = units[i].RI_Spd.GetYval(wvl[w]);
                }
                rft[units.Count] = 0;       // inside reflectance is 0
                rbk[0] = 0;                 // calculate also reflectance from backside
                                            //calculate from inside out accumulated reflectance values
                for (int i = units.Count - 1; i >= 0; i--)
                {
                    rft[i] = rfU[i] + tauU[i] * tauU[i] * rft[i + 1] /
                        (1.0 - rbU[i] * rft[i + 1]);
                }
                // calculate backwards for inside reflectance
                for (int i = 1; i < units.Count + 1; i++)
                {
                    rbk[i] = rbU[i - 1] + tauU[i - 1] * tauU[i - 1] * rbk[i - 1] /
                        (1.0 - rfU[i - 1] * rbk[i - 1]);
                }
                // get the internal tran values
                for (int i = 0; i < units.Count; i++)
                {
                    tint[i] = tauU[i];
                    tint[i] /= (1.0 - rbU[i] * rft[i + 1]);
                }
                // calculate absorption
                Iplus[0] = rft[0];
                Iminus[0] = 1.0;    // incoming intensity set to 1.0
                for (int i = 1; i < units.Count + 1; i++)
                {
                    Iminus[i] = tint[i - 1] * Iminus[i - 1];
                    Iplus[i] = rft[i] * Iminus[i];
                }
                for (int i = 0; i < units.Count; i++)
                {
                    ABS[i][w] = Iminus[i] - Iplus[i] + Iplus[i + 1] - Iminus[i + 1];
                }
                TRA[w] = Iminus[units.Count];
                R_out[w] = rft[0];
                R_in[w] = rbk[units.Count];
            }
            for (int i = 0; i < units.Count; i++)
            {
                //absU[i] = AbsSolar(wvl, ABS[units.Count - 1 - i
                absU[i] = AbsSolar(wvl, ABS[i]);
            }
            absUsolar = absU;
            string lbl = this.UnitName;
            TR = new SpectralData(wvl, TRA, "T-" + lbl, ColorType.TR, false);
            Rout = new SpectralData(wvl, R_out, "Rft-" + lbl, ColorType.IGU_Rout, false);
            Rin = new SpectralData(wvl, R_in, "Rbk-" + lbl, ColorType.IGU_Rin, false);
        }

        // return spectral / thermal properties
        /// <summary>
        /// Visible Light Transmittance ISO
        /// </summary>
        public double tauVIS
        {
            get { return VisibleCharacteristic(Tran); }
        }

        /// <summary>
        /// visible reflectance outside
        /// </summary>
        public double rho_o
        {
            get { return VisibleCharacteristic(R_out); }
        }

        /// <summary>
        /// visible refelectance inside
        /// </summary>
        public double rho_i
        {
            get { return VisibleCharacteristic(R_in); }
        }

        /// <summary>
        /// solar transmittance
        /// </summary>
        /// <returns>T solar</returns>
        public double tau_Solar()
        {
            return SolarCharacteristic(Tran);
        }

        /// <summary>
        /// calculate solar absorption: assuming radiation from outside: this is unit specific
        /// </summary>
        /// <returns>solar absorptance for each component</returns>
        protected virtual double[] SolarAbsorptance()
        {
            string solfile = TCalcStandard.SolarFile;    // get the file for the currently set calculation standard
            if ((SolarRadiation == null) || (SolarRadiation.AName != solfile))  // load the correct table if not there
            {
                SolarRadiation = new ReferenceTable(solfile);
            }

            double vv = 0, Sl = 0;
            MakeSpectraGeneric();
            for (int w = 0; w < SolarRadiation.Elements; w++)
            {
                double wl = SolarRadiation[w]; // get the wavelength
                vv += (1.0 - Tran.GetYval(wl) - R_out.GetYval(wl)) * SolarRadiation.GetYval(wl);
                Sl += SolarRadiation.GetYval(wl);
            }
            double[] sab = new double[1];
            sab[0] = vv / Sl;
            return sab;
        }

        /// <summary>
        /// calculate solar absorptance for a given absorption spectum (multi glazing)
        /// </summary>
        /// <param name="wvl">wavelength array</param>
        /// <param name="absw">absorption array</param>
        /// <returns></returns>
        protected double AbsSolar(double[] wvl, double[] absw)
        {
            if (wvl.Length != absw.Length)
            {
                OptiUtils.OptiThermMessageSend("wvl array does not match absorption array: Calculating Solar Absorption");
                return 0.0;
            }
            string solfile = TCalcStandard.SolarFile;    // get the file for the currently set calculation standard
            if ((SolarRadiation == null) || (SolarRadiation.AName != solfile))  // load the correct table if not there
            {
                SolarRadiation = new ReferenceTable(solfile);
            }

            double vv = 0, Sl = 0;
            for (int w = 0; w < SolarRadiation.Elements; w++)
            {
                double wl = SolarRadiation[w]; // get the wavelength
                vv += LookUpWl(wl, wvl, absw) * SolarRadiation.GetYval(wl);
                Sl += SolarRadiation.GetYval(wl);
            }
            //calculate based on the spectral data wavelength interval
            //         for (int w = 0; w < wvl.Length; w++)
            //{
            //	double wl = wvl[w]; // get the wavelength
            //	vv += absw[w] * SolarRadiation.GetYval(wl);
            //	Sl += SolarRadiation.GetYval(wl);
            //}
            double sab = vv / Sl;
            return sab;
        }
        /// <summary>
        /// solar reflectance outside surface
        /// </summary>
        /// <returns>R-solar</returns>
        public double rho_Solar()
        {
            return SolarCharacteristic(R_out);
        }
        public double rho_Solar_back() //!< solr reflection inside surface
        {
            return SolarCharacteristic(R_in);
        }
        /// <summary>
        /// UV transmittance ISO 9845:1:1992
        /// </summary>
        public double T_UV
        {
            get { return UVCharacteristic(Tran); }
        }

        public double R_UV_OUT //!< UV reflectance outside
        {
            get { return UVCharacteristic(R_out); }
        }

        public double R_UV_IN //!< UV reflectance inside
        {
            get { return UVCharacteristic(R_in); }
        }


        /// <summary>
        /// calculate visible tau/rho using normalized ISO reference (D65)
        /// </summary>
        /// <param name="sp">spectral data</param>
        /// <returns></returns>
        protected static double VisibleCharacteristic(SpectralData sp)
        {
            if (sp.IntensityPercent)
            {
                sp.IntensityPercent = false; // tranform to generic data
            }

            if (ISO_Visible == null)
            {
                ISO_Visible = new ReferenceTable(ISO9050Tables.SpectralPowerD65);
            }
            double vv = 0;
            for (int w = 0; w < ISO_Visible.Elements; w++)
            {
                double wl = ISO_Visible[w]; // get the wavelength
                vv += (sp.GetYval(wl) * ISO_Visible.GetYval(wl));
            }
            return vv / 100.0; // ISO table data are *100
        }


        /// <summary>
        /// calculate solar characteristic T/R for given spectral data set
        /// </summary>
        /// <param name="sp">spectral data</param>
        /// <param name="std">thermal standard</param>
        /// <returns>solar energy transmittance or reflectance</returns>
        protected static double SolarCharacteristic(SpectralData sp)
        {
            string solfile = TCalcStandard.SolarFile;    // get the file for the currently set calculation standard
            if ((SolarRadiation == null) || (SolarRadiation.AName != solfile))  // load the correct table if not there
            {
                SolarRadiation = new ReferenceTable(solfile);
            }

            if (sp.IntensityPercent)
            {
                sp.IntensityPercent = false;
            }
            // use the triangular method for the spectral data
            var wlp = SolarRadiation[0]; // first wavelength element
            var vv = sp.GetYval(wlp) * SolarRadiation.GetYval(wlp);
            var Sl = SolarRadiation.GetYval(wlp);
            for (int w = 1; w < SolarRadiation.Elements; w++)
            {
                double wl = SolarRadiation[w]; // get the wavelength
                double dw = (wl - wlp) / 2;
                vv += (sp.GetYval(wl - dw) + sp.GetYval(wl + dw)) / 2.0 * SolarRadiation.GetYval(wl);
                Sl += SolarRadiation.GetYval(wl);
                wlp = wl;
            }
            return vv / Sl;
        }



        /// <summary>
        /// returns internal surface heat transfer coefficient based on EN637
        /// when internal surface has a low emissivity
        /// </summary>
        /// <param name="ehemi">normal emissivity</param>
        /// <returns></returns>
        protected static double hr_intSurface(double ehemi)
        {
            // hi_U contains the radiation component for uncoated glass!!
            //var hi = TCalcStandard.hi_U / 0.837 * enormal;
            var hi = TCalcStandard.hi_R / 0.837 * ehemi;
            return hi;
        }

        /// <summary>
        /// calculate the U-Value: heat transfer coefficient
        /// </summary>
        /// <param name="T_out">outside temperature</param>
        /// <param name="T_in">inside temperature</param>
        /// <returns></returns>
        protected virtual double U_Val(double T_out, double T_in)
        {
            double sh = 0;
            if (!TCalcStandard.ISOcalc)
            {
                return U_NFRC_ISO(false, out sh, 0);  // use the NFRC method: winter U-value
            }
            double he = TCalcStandard.he_U;
            // get the total internal transfer coefficient: convection + radiative component
            double hi = TCalcStandard.hi_U + hr_intSurface(Components[0].RI_Spd.Eps_Hemi);
            // add the thermal transfer coefficent for the glass sheets:  rj = 1.0 m K/W
            double hs = Components[0].UnitThickness / 1000.0; // for the base unit there is only one
                                                              //return the U-value: in the base class for the monolithic glass
            double U = 1.0 / (1.0 / he + 1.0 / hi + hs);
            return U;
        }

        /// <summary>
        /// get UV T/R characteristic for spectral data set
        /// </summary>
        /// <param name="sp">spectral data set</param>
        /// <returns></returns>
        protected static double UVCharacteristic(SpectralData sp)
        {
            if (sp.IntensityPercent)
            {
                sp.IntensityPercent = false;
            }

            string uvfile = TCalcStandard.UVTable;       // take the standard defined in the reference
            if ((UVRadiation == null) || (UVRadiation.AName != uvfile))
            {
                UVRadiation = new ReferenceTable(uvfile);
            }

            double vv = 0;
            double SUV = 0;
            for (int w = 0; w < UVRadiation.Elements; w++)
            {
                double wl = UVRadiation[w]; // get the wavelength
                                            //double spint = sp.GetYval(wl);
                double slUV = UVRadiation.GetYval(wl);
                if (wl >= 300)
                {
                    vv += sp.GetYval(wl) * UVRadiation.GetYval(wl);   // skip lower values == 0
                }

                SUV += UVRadiation.GetYval(wl);
            }
            return vv / SUV;
        }
        protected double eps_c(SpectralData refl)
        {
            return refl.Eps_Hemi;
        }

        /// <summary>
        /// set the emissivity value for surface no: 1 is outside/ 2 is inside
        /// 0 is special to selected the coated side, -1 or any other estimates eps based on NIR
        /// </summary>
        /// <param name="eps">emissivity</param>
        /// <param name="surface">surface to set</param>
        /// <param name="hemi">is hemisherical?</param>
        /// <returns></returns>
        public virtual double SetEpsilon(double eps, int surface, bool hemi)
        {
            // set emissivity for surface number: 1 is outside, 0 is a special looking for a film side
            // return the hemisherical value in case it was set or estimated
            if (surface > 2)
            {
                return 0.837; // invalid parameter
            }

            SpectralData? StoSet = null;
            switch (surface)
            {
                case 0:
                    // look for the "coated" side:
                    if (this.RO_Spd.IsRFilm)
                    {
                        StoSet = this.RO_Spd;
                    }
                    else
                        if (this.RI_Spd.IsRFilm)
                    {
                        StoSet = this.RI_Spd;
                    }

                    break;
                case 1:
                    StoSet = this.RO_Spd;
                    break;
                case 2:
                    StoSet = this.RI_Spd;
                    break;
                default:
                    // estimate the emissivity for the coated side
                    hemi = true;    // return hemisherical emissivity
                    if (this.RI_Spd.IsRFilm)
                    {
                        this.RI_Spd.IsRFilm = true;  // just use the setting to recalculate the estimate
                        StoSet = this.RI_Spd;
                        eps = this.RI_Spd.Eps_Hemi;
                    }
                    else
                    {
                        if (this.RO_Spd.IsRFilm)
                        {
                            this.RO_Spd.IsRFilm = true;  // just use the setting to recalculate the estimate
                            StoSet = this.RO_Spd;
                            eps = RO_Spd.Eps_Hemi;
                        }
                    }
                    break;
            }
            if (StoSet != null)
            {
                if (!hemi)
                {
                    StoSet.Eps_Hemi = TCalcStandard.CurrentStandard == ThermalStandard.NFRC_2001 ? SpectralData.EpsH_LBNL(eps) : SpectralData.EpsConversionEN(eps, false);
                }
                return StoSet.Eps_Hemi;
            }
            else
            {
                return RO_Spd.Eps_Hemi; // returns by default glass surface value
            }
        }

        /// <summary>
        /// get the IGU space width (1-2) in mm
        /// </summary>
        /// <param name="spaceID">position of gap</param>
        /// <returns>gap in mm</returns>
        public double AirGap_mm(int spaceID)
        {
            //if( spaceID <= gaps.Count) return gaps[spaceID-1]*1000;
            // reverse sequence for triple glazing
            if (spaceID <= gaps.Count)
            {
                return gaps[gaps.Count - spaceID] * 1000;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// set the gap distance
        /// </summary>
        /// <param name="spaceID">gap ID:1..2</param>
        /// <param name="gap"> width in mm </param>
        /// <returns></returns>
        public bool SetAirGap_mm(int spaceID, double gap)
        {
            if (spaceID <= gaps.Count)
            {
                //gaps[spaceID-1] = gap/1000;
                gaps[gaps.Count - spaceID] = gap / 1000;
                return true;    // successful value
            }
            return false;
        }
        /// <summary>
        /// get the radation  heat transfer coefficinet between two surfaces
        /// </summary>
        /// <param name="Tavg">average gap temperature</param>
        /// <param name="epsq">emissivity glass 1</param>
        /// <param name="eps2">emissivity glass 2</param>
        /// <returns>hr radiative heat conductance</returns>
        protected static double hr_int(double Tavg, double eps1, double eps2)
        {
            // the radiation component: EN673:1997 p7
            double hrint = 4.0 * SigmaB * 1.0 / (1.0 / eps1 + 1.0 / eps2 - 1) *
                Math.Pow(Tavg, 3);
            return hrint;
        }

        /// <summary>
        /// get/set the filling gas type for gap
        /// </summary>
        /// <param name="gapnumber">gap id</param>
        /// <returns>type of filling gas</returns>
        public FillingGas GasFilling(int gapnumber)
        {
            //if( Components.Count > gapnumber) return gas[gapnumber-1];
            // inside a unit the gaps and uniots are in reverse order
            if (gas.Count >= gapnumber)
            {
                return gas[gas.Count - gapnumber];
            }
            else
            {
                return FillingGas.Air;
            }
        }

        /// <summary>
        /// set the filling gas type for gap nr:
        /// </summary>
        /// <param name="gapnumber">gap 1 to x</param>
        /// <param name="gastype">TypeofGas</param>
        /// <returns>false if gapnumber not found</returns>
        public bool SetGasType(int gapnumber, FillingGas gastype)
        {
            if (gapnumber <= gas.Count && gapnumber > 0)
            {
                gas[gas.Count - gapnumber] = gastype;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// front (outward) surface temperature resulting from solar calculations
        /// </summary>
        public double Tfront { get; set; }
        /// <summary>
        /// back (inward) surface temperature
        /// </summary>
        public double Tback { get; set; }

        /// <summary>
        /// get/set hickness of the component: here the default is substrate thickness
        /// for a monolithic unit
        /// </summary>
        public virtual double UnitThickness
        {
            get { return this.SubstrateThickness * 1000.0; }
            set { this.SubstrateThickness = value / 1000.0; }
        }
        //virtual functions to retrieve names and coating names for individual units
        public string UnitName //!< title of unit
        {
            get { return this.UName; }
            set { this.UName = (value == null) ? "NA" : value; }
        }
        public string SubName   //!< substrate name
        {
            get { return this.SubstrateName; }
            set { this.SubstrateName = value; }
        }
        public string FrontCoating  //!< front side design name
        {
            get { return this.FrontCoat; }
            set { this.FrontCoat = value; }
        }
        public string BackCoating  //!< back side design name
        {
            get { return this.BackCoat; }
            set { this.BackCoat = value; }
        }
        public int LoadID   //! batch or load iD
        {
            get { return this.BatchID; }
            set { this.BatchID = value; }
        }

        /// <summary>
        /// flip unit 180° -> inside->out
        /// </summary>
        public virtual void ReverseUnit()
        {
            SpectralData tmp = (SpectralData)R_out.Clone();
            string cname = FrontCoating;
            R_out = (SpectralData)R_in.Clone();
            // spectra type moves with the flip: RF <->RG
            R_in = tmp;
            FrontCoating = BackCoating; // reverse also the coating names
            BackCoating = cname;
            //CalcMultiSheetOptics(Components, out Tran, out R_out, out R_in, out absSolar);
            absSolar = SolarAbsorptance();  // recalculate solar absorptance for single sheet
        }


        /// <summary>
        /// save the current spectral data in a Optics5 compatible text file
        /// utility for export to LBNL and Optics 5 or a 4 column txt 
        /// </summary>
        /// <param name="FullFileName">full file path</param>
        /// <param name="lbnl">save in lbnl format or tab limited txt format</param>
        /// <returns>success</returns>
        public bool SaveAsText(string FullFileName, bool lbnl)
        {
            if ((TR_Spd.wvl[0] > 350 || TR_Spd.wvl[TR_Spd.Elements-1] < 2450) && lbnl)
            {
                OptiUtils.OptiThermMessageSend("Not enough data for thermal calculations!: can't export to LBNL file");
                return false;
            }
            //string? fp = Path.GetDirectoryName(FullFileName);
            StreamWriter writer;
            try
            {
                writer = File.CreateText(FullFileName); // create or overwrite the file
            }
            catch
            {
                OptiUtils.OptiThermMessageSend("Can't create Text file:" + FullFileName + "Export spectral data");
                return false;
            }
            if (lbnl)
            {
                // write the header for lbnl
                string subfile = this.SubName.Replace(" ", "") + this.UnitThickness.ToString("F0",CultureInfo.InvariantCulture) + Path.GetExtension(FullFileName);
                writer.WriteLine("{ Units, Wavelength Units } SI Microns");
                writer.WriteLine("{ Thickness } " + this.UnitThickness.ToString("F2"));
                writer.WriteLine("{ Conductivity } 1");
                writer.WriteLine("{ Emissivity, front back } Emis= " + R_in.Eps_Hemi.ToString("F3", CultureInfo.InvariantCulture) + " " + 
                    R_out.Eps_Hemi.ToString("F3", CultureInfo.InvariantCulture));
                writer.WriteLine("{ }");
                string product = this.FrontCoating + " on Float " + this.SubName + " " + this.UnitThickness.ToString("F0", CultureInfo.InvariantCulture) + " mm";
                writer.WriteLine("{ Product Name: " + product + " }");
                string manu = Path.GetExtension(FullFileName).Contains("gre") ? " Guardian Europe" : " Guardian";
                writer.WriteLine("{ Manufacturer: " + manu + " }");
                writer.WriteLine("{ Type: Coated }");
                writer.WriteLine("{ Material: N/A }");
                writer.WriteLine("{ Coating Name: " + this.FrontCoating + " }");
                writer.WriteLine("{ Coated Side: Front }");
                writer.WriteLine("{ Substrate: " + this.SubName + " }");
                writer.WriteLine("{ Substrate Filename: " + subfile + " }");
                writer.WriteLine("{ Appearance: Clear }");
                writer.WriteLine("{ Acceptance: # }");
            }
            else
            {
                // for a txt file just write the name and substrate thickness as header
                writer.WriteLine("Name: " + this.FrontCoating + " on " + this.UnitThickness.ToString("F2", CultureInfo.InvariantCulture) + " mm");
                writer.WriteLine("Lambda (nm) - T - RGlass - RFilm)");
            }

            if (Tran.IntensityPercent)
            {
                Tran.IntensityPercent = false;
            }

            if (R_in.IntensityPercent)
            {
                R_in.IntensityPercent = false;
            }

            if (R_out.IntensityPercent)
            {
                R_out.IntensityPercent = false;
            }

            double wl = lbnl ? 300: Tran.wvl[0];        // write data in specified intervals
            double wlmax = lbnl ? 2505 : Tran.wvl[^1] + 5;  // add the last step to get all data
            do
            {
                if (lbnl)
                {
                    writer.Write((wl / 1000).ToString("F3", CultureInfo.InvariantCulture) + '\t');
                }
                else
                {
                    writer.Write(wl.ToString("F3", CultureInfo.InvariantCulture) + '\t');
                }
                writer.Write(Tran.GetYval(wl).ToString("F4", CultureInfo.InvariantCulture) + '\t');
                double rf = R_in.GetYval(wl);
                // for reflectance keep values below 98%
                rf = rf > 0.98 ? 0.98 : rf;
                double rg = R_out.GetYval(wl);
                rg = rg > 0.98 ? 0.98 : rg;
                if (lbnl)
                {
                    writer.Write(rf.ToString("F4", CultureInfo.InvariantCulture) + '\t');
                    writer.Write(rg.ToString("F4", CultureInfo.InvariantCulture));
                }
                else
                {
                    writer.Write(rg.ToString("F4", CultureInfo.InvariantCulture) + '\t');
                    writer.Write(rf.ToString("F4", CultureInfo.InvariantCulture));
                }
                writer.Write(Environment.NewLine);
                wl += 5;    // use all over 5 nm
            } while (wl <= wlmax);
            writer.Close();
            return true;
        }
        /// <summary>
        /// copy the spectral data of the sheet to the clipboard
        /// </summary>
        public string CopySpectraToText()
        {
            StringBuilder buffer = new ();
            buffer.Append("Wvl" + "\t" + "TR" + "\t" + "RF" + "\t" + "RG");
            double wlbegin = this.TR_Spd[0];
            double wlend = this.TR_Spd[TR_Spd.Elements-1];
            for (double wl = wlbegin; wl <= wlend; wl += 10)
            {
                buffer.Append("\n" + wl.ToString("F1") + "\t" +
                              this.TR_Spd.GetYvalPercent(wl).ToString("F1") + "\t" +
                              this.RI_Spd.GetYvalPercent(wl).ToString("F1") + "\t" +
                              this.RO_Spd.GetYvalPercent(wl).ToString("F1"));
            }
            return buffer.ToString();
            //Clipboard.SetDataObject(buffer.ToString(), false);
        }
        /// <summary>
        /// returns a string indicating the evaluation conditions
        /// </summary>
        /// <returns>a string describing the IGU configuration and standard used</returns>
        public virtual string EvalConditions()
        {
            string etxt = "Mono using " + this.UnitThickness.ToString("F1") + " mm " + this.SubName + " "
                + TCalcStandard.CurrentStandard.ToString();
            return etxt;
        }


        /// <summary>
        /// color calculation properties used
        /// </summary>
        /// <returns>string describing color calculation parameters</returns>
        public virtual string ColorScale()
        {
            string clrhdr = "Color Scale: ";
            clrhdr += this.CIE_Illuminant.ToString() + "/";
            if (this.CIE_Observer == CIEObservers.CIE_1931)
            {
                clrhdr += "2°";
            }
            else
            {
                clrhdr += "10°";
            }

            return clrhdr;
        }

        // support functions for thermal evaluations
        // supporting functions for ASHRAE calculations of U / SHGC
        protected static double BlackEmi(double T)
        {
            return SigmaB * Math.Pow(T, 4.0);     // calculate black emissivity based on Temperature
        }
        protected static double TfromBE(double BE)
        {
            double bs = BE / SigmaB;
            bs = Math.Pow(bs, 0.25);     // return Temperature based on black emissivity
            return bs;
        }
        /// <summary>
        /// return transformed internal heat tranfer coefficient h
        /// based on black emissivity
        /// </summary>
        /// <param name="h">heat transfer coefficient</param>
        /// <param name="Tf">T frontside</param>
        /// <param name="Tb">T backside</param>
        protected static double h_tran(double h, double Tf, double Tb)
        {
            if (Tf == Tb)
            {
                return 0;
            }
            else
            {
                return h * (Tf - Tb) / (BlackEmi(Tf) - BlackEmi(Tb));
            }
        }
        /// <summary>
        /// heat transfer by natural convection and radiation on inside: ISO 15099:2003 8.3.2.2
        /// </summary>
        /// <param name="Tb1"> inside glass temperature</param>
        /// <param name="Tint">Room Temperature</param>
        /// <returns></returns>
        protected static double hcv_int(double Tb1, double Tint)
        {
            double Tm = Tint +  0.25 * (Tb1 - Tint);        // estimates mean film temperature
            double unitHeight = TCalcStandard.ISOcalc ? 1.0 : 1.0;  // height of window unit - edges (NFRC: 600 W x 1500H)
            double rho = GasProperties.rho(FillingGas.Air, Tm);
            double mue = GasProperties.mue(FillingGas.Air, Tm);
            double lam = GasProperties.lambda(FillingGas.Air, Tm);
            // use corresponding data for gas properties -> Grashof and Prandl numbers
            double Gr = 9.81 * Math.Abs(Tint - Tb1) * Math.Pow(rho, 2.0) * Math.Pow(unitHeight, 3)
                / (Tm * Math.Pow(mue, 2.0));
            double Pr = mue * GasProperties.specHeat(FillingGas.Air, Tm) / lam;
            double RaH = Gr * Pr;        // Rayleigh number for internal surface
            double Racv = 2.5e5 * Math.Pow(Math.Exp(0.72 * 90), 1.0 / 5.0); // Ra cv
            double Nu;
            /*
			if (RaH > Racv)
				Nu = 0.13 * (Math.Pow(RaH, 1.0 / 3.0) - Math.Pow(Racv, 1.0/3.0)) + 0.56 * Math.Pow(Racv, 1.0/4.0);
			else
				Nu = 0.56 * Math.Pow(RaH, 1.0 / 4.0); */
            // old variant
            if (Tint > Tb1)  // winter case Tin is warmer
            {
                if (RaH > Racv)
                {
                    Nu = 0.13 * (Math.Pow(RaH, 1.0 / 3.0) - Math.Pow(Racv, 1.0 / 3.0)) + 0.56 * Math.Pow(Racv, 1.0 / 4.0);
                }
                else
                {
                    Nu = 0.56 * Math.Pow(RaH, 1.0 / 4.0);
                }
            }
            else
            {
                Nu = 0.56 * Math.Pow(RaH, 1.0 / 4.0);      // summer case ISO 15099 8.3.2.2
            }
            Nu *= (lam / unitHeight);
            return Nu;
        }

        protected double hcv_VIG(double gap)
        {
            // heat transfer through conductance of spacers
            // arranged in sqares distance from each other nbr -> per m^2
            // conductance through the spacer body can be neglected for glass thicker than 3 mm
            double contactArea = (VIG_spacer.lenght * VIG_spacer.width) * 1.0e-6;
            // build the thermal conductance for glass to pillar contribution based on N.Ng and Collins 2005
            // calculates an equivalent circular contact radius for the square pillars
            double hcp = 2.0 * LamdaGlass * Math.Sqrt(contactArea / Math.PI) / (VIG_spacer.spacer_distance * VIG_spacer.spacer_distance);
            // conductance of the spacers themselfs over a 1 m^2 area (included for completeness)
            double hcpd = (VIG_spacer.lambda * gap) / contactArea * 1.0 / (VIG_spacer.spacer_distance * VIG_spacer.spacer_distance);
            // return the total average conductance caused by the spacers: based on paper from Beijing Synergy Vacuum Glazing
            double hcpr = 0.6 * VIG_spacer.pressure;
            //			double hct = 1.0/(1.0/hcp + 1.0 /hcpd + 1.0/hcpr);
            double hct = (hcp + 1.0 / hcpd + hcpr);
            return hct;
        }
        /// <summary>
        /// convective heat transfer coefficient of the gas gap: hg
        /// </summary>
        /// <param name="deltaT">Delta T</param>
        /// <param name="Tgap">average gap Temperature K</param>
        /// <param name="gapID">ID of gap inside out</param>
        /// <returns></returns>
        protected double hcv_ISO(double Tfront, double Tb_p, double gap, FillingGas gType)
        {
            // estimate the thermal conductance coefficient for a IG unit with given gap (in m) and filling gas
            // uses Standard references from EN673:1997 section 5.3 / ISO15099:2003
            if (gType == FillingGas.VIG)
            {
                return hcv_VIG(gap);       // VIG: just heat transfer by conduction of posts
            }

            double Tgap = (Tfront + Tb_p) / 2.0;  // take average Temperature
            double deltaT = Math.Abs(Tfront - Tb_p);   // calculate delta T
            double unitHeight = 1.0;    // height of window unit - edges (NFRC: 600 W x 1500H)
            double Nu, Gr, Pr, hg;
            double rho, mue, lam, cp; // gas constants
                                      // get the gas properties
                                      // use corresponding data for gas properties -> Grashof and Prandl numbers
            if (TCalcStandard.ISOcalc)
            {
                rho = GasProperties.rho_EN(gType, Tgap);
                mue = GasProperties.mue_EN(gType, Tgap);
                lam = GasProperties.lambda_EN(gType, Tgap);
                cp = GasProperties.specHeat_EN(gType, gap);
                Gr = 9.81 * Math.Pow(gap, 3.0) * deltaT * Math.Pow(rho, 2.0) / (Tgap * Math.Pow(mue, 2.0));
                Pr = mue * cp / lam;
                double Ral = Gr * Pr;
                Nu = 0.035 * Math.Pow(Gr * Pr, 0.38);      // Nusselt number
                if (Nu < 1.0)
                {
                    Nu = 1.0;       //exception in EN673:1997
                }
            }
            else
            {
                unitHeight = 1.0;
                // use the ISO 15099 values for NFRC
                rho = GasProperties.rho(gType, Tgap);
                mue = GasProperties.mue(gType, Tgap);
                lam = GasProperties.lambda(gType, Tgap);
                cp = GasProperties.specHeat(gType, gap);
                // calculate based on ISO150099 Vertical cavities 5.3.3.5
                double RaI = 9.81 * Math.Pow(rho, 2.0) * Math.Pow(gap, 3.0) * cp * deltaT;
                RaI /= (Tgap * mue * lam);
                var Ar = unitHeight / gap;  // aspect ratio
                double Nu1, Nu2;
                if (RaI > 5.0e4)
                {
                    Nu1 = 0.0673838 * Math.Pow(RaI, 1.0 / 3.0);
                }
                else
                    if ((RaI > 1.0e4))
                {
                    Nu1 = 0.028154 * Math.Pow(RaI, 0.4134);
                }
                else
                {
                    Nu1 = 1.0 + 1.7596678e-10 * Math.Pow(RaI, 2.2984755);
                }
                Nu2 = 0.242 * Math.Pow(RaI / Ar, 0.272);
                Nu = Math.Max(Nu1, Nu2);
            }
            hg = Nu * lam / gap;
            return hg;
        }

        /// <summary>
        /// calculate the radative heat transfer coefficient
        /// </summary>
        /// <param name="Tgap">medium gap temperature</param>
        /// <param name="gapID">ID of gap: indentify surfaces</param>
        /// <returns></returns>
        protected double hr(double Tgap, int gapID)
        {
            double hrad = 0;
            if (gapID < Components.Count)
            {
                double eps1 = eps_c(Components[gapID].RI_Spd);  // get emissivity for correct surface
                double eps2 = eps_c(Components[gapID + 1].RO_Spd);
                hrad = 4.0 * SigmaB * Math.Pow(Tgap, 3.0) / (1.0 / eps1 + 1.0 / eps2 - 1.0);
            }
            return hrad;
        }


        /// <summary>
        /// virtual function to calculate solar gain
        /// overwritten for IGU and triple units
        /// </summary>
        /// <param name="U_summer">returns summer U-vale</param>
        /// <returns>winter U-value</returns>
        public virtual double SolarGain(out double U_summer)
        {
            //if (absSolar == null)
            //{
            //}
            double gv = 0;
            if (!TCalcStandard.ISOcalc)
            {
                absSolar = SolarAbsorptance();
                double US = U_NFRC_ISO(true, out gv, TCalcStandard.SolarE);  // get the SHGC calculated according to ASHRAE
                U_summer = US; // * MetricBTU;
            }
            else
            {
                //absSolar = SolarAbsorptance();
                double alpha = 1.0 - tau_Solar() - rho_Solar();     // get solar absorptance
                double he = TCalcStandard.he_SHG;    // outside heat transfer factor
                double hi = TCalcStandard.hi_SHG + hr_intSurface(Components[0].RI_Spd.Eps_Hemi);
                gv = tau_Solar() + alpha * hi / (he + hi);      // single glazing g-value secondary heat transfer coefficient
                U_summer = U_Value_Winter; // no summer U defined
            }
            return gv;
        }

        /// <summary>
        /// type of solar properties: EN: g-val - NFRC: SHGC
        /// </summary>
        public string SolarGainType
        {
            get
            {
                if (!TCalcStandard.ISOcalc)
                {
                    return "SHGC";
                }
                else
                {
                    return "g-val";
                }
            }
        }

        /* <summary>
        /// calculation based on ISO 15099 and Window 6/7 reference manual
        /// </summary>
        /// <param name="summer">flag for summer conditions</param>
        /// <param name="SHGC"> returns SHGC for summer conditions</param>
        /// <param name="Isolar">incident solar radiation</param>
        /// <returns>U-value NFRC calculation</returns>
        protected double UX_NFRC_ISO(bool summer, out double SHGC, double Isolar)
        {
            // calculation is forward: outside glass first
            var eps_f = new double[Components.Count]; // front emissivity
            var eps_b = new double[Components.Count]; // back emissivities
            for (int i = 0; i < Components.Count; i++)
            {
                eps_f[i] = Components[i].R_out.Eps_Hemi;
                eps_b[i] = Components[i].R_in.Eps_Hemi;
            }
            var SolarTrans = tau_Solar();
            double[] AbsUnits = absSolar;
            // initialize start values and inside / outside temperatures
            var Tout = summer ? TCalcStandard.Tout_SHG : TCalcStandard.Tout_U;
            var Tin = summer ? TCalcStandard.Tin_SHG : TCalcStandard.Tin_U;
            SHGC = 0.0;
            // front and back glass temperatures
            var Tg_f = new double[Components.Count]; // front and back temperatures of glazings
            var Tg_b = new double[Components.Count]; // back temperatures for glazings
            var Tg_f_old = new double[Components.Count]; // back up temperatures from previous solution
            var Tg_b_old = new double[Components.Count];
            var Tg_ctr = new double[Components.Count];  // center glass temperatures
            // set starting surface temperatures: distribute temperatures across glasses and gaps
            var Unit_d = 4.0;
            for (int i = 0; i < Components.Count; i++)
            {
                Unit_d += Components[i].UnitThickness;
                Unit_d += i + 1 < Components.Count ? 12.0 : 0;
            }
            var dt = ((Tin - Tout) / Unit_d);
            var dist = 2.0; // to start with a glass temperature != Tout
            for (int i = 0; i < Components.Count; i++)
            {
                Tg_f[i] = Tout + dist * dt;
                Tg_b[i] = Tg_f[i] + (dist - 2.0 + Components[i].UnitThickness) * dt;
                dist += Components[i].UnitThickness;
                dist += i + 1 < Components.Count ? 12.0 : 0;
                Tg_ctr[i] = (Tg_f[i] + Tg_b[i]) / 2.0; // start value
            }
            var Gin = BlackEmi(Tin); // indoor irradiance
            //var Tiavg = (Tg_f[Components.Count - 1] + Tin) / 2.0;
            var hr_in = 4.0 * SigmaB * eps_b[Components.Count - 1] * Math.Pow(Tin, 3.0); // initial inside radiant heat transfer coeffcient
            var hc_in = hcv_int(Tg_b[Components.Count - 1], Tin); // natural convection inside
            var h_in = hc_in + hr_in; // initial
            var Troom = Tin;
            // outside convection defined by windspeed
            var hc_out = summer ? TCalcStandard.he_SHG : TCalcStandard.he_U;
            // Outoor mean radiant temperature for clear sky
            //var Trm_out = Math.Pow((0.5 * SigmaB * Math.Pow(Tout, 4) + 0.5 * 5.31e-13 * Math.Pow(Tout, 6)) / SigmaB, 0.25);
            var Trm_out = Tout; // boundary condition NRFC 100/200
            var Gout = BlackEmi(Trm_out);  // outdoor irradiance
            // initial radiative transfer coefficient for outdoor
            var hr_out = 4.0 * SigmaB * eps_f[0] * Math.Pow((Trm_out + Tg_f[0]) / 2.0, 3.0);
            //var h_out = hc_out + hr_out; // initial outdoor heat transfer
            var Tamb = Tout; // NFRC boundary
            var h_d = new double[Components.Count + 1]; // convective heat transfer coefficients for air gaps + inside / outside
                                                        // set up A matrix and right hand side vector B: for solving the 4n independent heat balance equations at each glass unit
            int order = Components.Count * 4; // matrix size
            double[,] A = new double[order, order];
            double[] B = new double[order];
            double[] XS = new double[order]; // solution vector
            double U_Value = 0;
            var SolRadiance = Isolar; // solar irradiance
            var qtsolar = 0.0; // glass resistance data fot SHGC calculations
            double[] Rg_I = new double[Components.Count];
            double[] Rg_0 = new double[Components.Count];
            // if solar irradiance is present calculate with and without solar irradiance to get SHGC
            var runs = SolRadiance > 0 ? 2 : 1;
            do
            {

                // get solar absorption for all glazings
                var Sabs = AbsUnits;
                for (int i = 0; i < Sabs.Length; i++)   // calculate solar absorption for each glass
                {
                    Sabs[i] *= SolRadiance;
                }
                var iter = 0; // iterations for solution
                XS.Initialize();
                A.Initialize();
                B.Initialize();
                // outside transfer heat transfer coefficients: initial value
                hr_out = 4 * SigmaB * eps_f[0] * Math.Pow((Trm_out + Tg_f[0]) / 2.0, 3.0); // initial outside radiation transfer

                double deltaT;
                do
                {
                    // outside heat transfer coefficient
                    h_d[0] = hc_out * (Tg_f[0] - Tamb) / (BlackEmi(Tg_f[0]) - Gout);
                    // inside transfer coefficient
                    //hc_in = hcv_int(Tg_b[Components.Count - 1], Tin); // natural convection inside: recalc using new temperature
                    h_d[Components.Count] = hc_in * (Tin - Tg_b[Components.Count - 1]) / (BlackEmi(Tin) - BlackEmi(Tg_b[Components.Count - 1]));
                    for (int k = 1; k < Components.Count; k++)
                    {
                        var h = hcv_ISO(Tg_f[k], Tg_b[k - 1], gaps[k - 1], gas[k - 1]); // convective heat transfer
                        h_d[k] = h * (Tg_f[k] - Tg_b[k - 1]) / (BlackEmi(Tg_f[k]) - BlackEmi(Tg_b[k - 1]));
                    }
                    // set the matrix and B vector elements
                    for (int i = 0; i < order / 4; i++)
                    {
                        var rc = 4 * i;
                        {
                            A[rc, rc] = 1.0;
                            A[rc, rc + 1] = h_d[i]; // h_out for I = 0
                            A[rc, rc + 2] = h_d[i + 1];
                            A[rc, rc + 3] = 1;
                            A[rc + 1, rc] = -1.0;
                            A[rc + 1, rc + 1] = eps_f[i];
                            A[rc + 2, rc + 2] = eps_b[i];
                            A[rc + 2, rc + 3] = -1.0;
                            // calculate glass transfer: assume center glass absorption: slice glass in center
                            var hglf = (Tg_ctr[i] - Tg_f[i]) / (BlackEmi(Tg_ctr[i]) - BlackEmi(Tg_f[i]));
                            var hgl = (Tg_b[i] - Tg_ctr[i]) / (BlackEmi(Tg_b[i]) - BlackEmi(Tg_ctr[i]));
                            var gd2 = LamdaGlass / Components[i].UnitThickness * 1000.0 / 2.0;
                            hgl = hgl * gd2 + hglf * gd2;
                            A[rc + 3, rc + 1] = -hgl;
                            A[rc + 3, rc + 2] = hgl + h_d[i + 1];
                            A[rc + 3, rc + 3] = 1.0;
                            // set outside columns
                            if (i < Components.Count - 1)
                            {
                                var col = (i + 1) * 4 - 1;
                                A[rc, col + 2] = -h_d[i + 1];
                                A[rc, col + 1] = -1;
                                A[rc + 2, col + 1] = 1.0 - eps_b[i]; // rho
                                A[rc + 3, col + 2] = -h_d[i + 1];
                                A[rc + 3, col + 1] = -1;
                            }
                            if (i > 0)
                            {
                                A[rc, rc - 1] = -1.0;
                                A[rc, rc - 2] = -h_d[i];
                                A[rc + 1, rc - 1] = 1.0 - eps_f[i];
                            }
                        }
                        // set the B vector components
                        if (i == 0)
                        {
                            // first entry: outside interface
                            B[rc] = Sabs[i] + Gout + h_d[0] * Gout;
                            if (order == 4)
                            {
                                B[rc] += Gin + h_d[i + 1] * Gin;
                            }
                            B[rc + 1] = -(1.0 - eps_f[i]) * Gout;
                            B[rc + 2] = order == 4 ? -(1.0 - eps_b[i]) * Gin : 0;
                            B[rc + 3] = order == 4 ? 0.5 * Sabs[i] + Gin + h_d[i + 1] * Gin : 0.5 * Sabs[i];
                        }
                        else
                            if (i == Components.Count - 1)
                        {
                            // last entry: inside interface
                            B[rc] = Sabs[i] + Gin + h_d[i + 1] * Gin;
                            B[rc + 1] = 0; // IR transmittance = 0
                            B[rc + 2] = -(1.0 - eps_b[i]) * Gin; // IR transmittance = 0
                            B[rc + 3] = 0.5 * Sabs[i] + Gin + h_d[i + 1] * Gin;
                        }
                        else
                        {
                            B[rc] = Sabs[i];
                            B[rc + 1] = 0;
                            B[rc + 2] = 0;
                            B[rc + 3] = 0.5 * Sabs[i];
                        }
                    }
                    // solve the system
                    alglib.rmatrixsolve(A, B.Length, B, out int solverinfo, out alglib.densesolverreport Sreport, out XS);
                    // get the new Temperatures from the Eb's
                    for (int i = 0; i < Components.Count; i++)
                    {
                        Tg_f[i] = TfromBE(XS[4 * i + 1]);
                        Tg_b[i] = TfromBE(XS[4 * i + 2]);
                        // calculate center glass temperatures: assumes absorption happens in mid glass
                        var d2k = Components[i].UnitThickness / 1000.0 / 2.0; // thickness of slice: Window 7 doc
                        Tg_ctr[i] = Tg_f[i] + Sabs[i] * d2k / 2.0 + d2k * XS[4 * i];
                        if (i == 0)
                        {
                            // first interface: outside
                            Tg_ctr[i] += hc_out * (Tg_f[i] - Tout) * d2k - Gout * d2k;
                        }
                        else
                        {
                            var hc = hcv_ISO(Tg_f[i], Tg_b[i - 1], gaps[i - 1], gas[i - 1]);
                            Tg_ctr[i] += d2k * (hc * (Tg_f[i] - Tg_b[i - 1]) - XS[4 * i - 1]);
                        }
                    }
                    Tamb = Tout;
                    // calculate delta temperatures to previous calculation
                    deltaT = 0;
                    for (int i = 0; i < Tg_f.Length; i++)
                    {
                        deltaT += Math.Abs(Tg_f[i] - Tg_f_old[i]) + Math.Abs(Tg_b[i] - Tg_b_old[i]);
                        // preserve current as old temperatures
                        Tg_f_old[i] = Tg_f[i];
                        Tg_b_old[i] = Tg_b[i];
                    }
                    // inside transfer coefficient
                    hc_in = hcv_int(Tg_b[Components.Count - 1], Tin); // natural convection inside: recalc using new temperature
                    iter++;

                } while (deltaT > 0.0001 && iter < 50);

                // calculate the total inside flowing heat flux qt for SHGC
                var qt = hc_in * (Tg_b[Components.Count - 1] - Tin) + XS[XS.Length - 1];
                if (SolRadiance > 0)
                {
                    qtsolar = qt;
                }
                else
                {
                    var sfl = qtsolar - qt;
                    SHGC = qtsolar == 0 ? 0 : SolarTrans + sfl / Isolar; // SHG as difference between zero and actual radiation
                }
                // calculate U value
                var Rtot = (Tg_f[0] - Tamb) / (hc_out * (Tg_f[0] - Tout) + XS[0] - Gout); // outdoor side
                var Rint = (Troom - Tg_b[Components.Count - 1]) / (hc_in * (Troom - Tg_b[Components.Count - 1]) + Gin - XS[XS.Length - 1]); // indoor
                for (int i = 1; i < Components.Count; i++) // air gaps
                {
                    var hc = hcv_ISO(Tg_f[i], Tg_b[i - 1], gaps[i - 1], gas[i - 1]); // convective heat transfer
                    Rtot += (Tg_f[i] - Tg_b[i - 1]) / (hc * (Tg_f[i] - Tg_b[i - 1]) + XS[4 * i] - XS[4 * i - 1]);
                }
                var Rgl = 0.0;
                for (int i = 0; i < Components.Count; i++)
                {
                    var rg = Components[i].UnitThickness / 1000.0 / LamdaGlass;
                    Rgl += rg; // glass resistance
                }
                Rtot += Rint + Rgl;
                if (SolRadiance == 0)
                {
                    U_Value = Math.Abs(1.0 / Rtot);

                }
                SolRadiance = 0; // in case of a second run
                runs--;

            } while (runs > 0);
            return U_Value;

        }

        */

        /// <summary>
        /// calculation based on ISO 15099 and Window 6/7 reference manual
        /// calculation uses outside -> in sequence of glasses
        /// </summary>
        /// <param name="summer">flag for summer conditions</param>
        /// <param name="SHGC"> returns SHGC for summer conditions</param>
        /// <param name="Isolar">incident solar radiation</param>
        /// <returns></returns>
        protected double U_NFRC_ISO(bool summer, out double SHGC, double Isolar)
        {
            // calculation is forward: outside glass first
            var eps_f = new double[Components.Count]; // front emissivity
            var eps_b = new double[Components.Count]; // back emissivities
            for (int i = 0; i < Components.Count; i++)
            {
                eps_f[i] = Components[i].R_out.Eps_Hemi;
                eps_b[i] = Components[i].R_in.Eps_Hemi;
            }
            var SolarTrans = tau_Solar();
            double[] AbsUnits = absSolar;
            // initialize start values and inside / outside temperatures
            var Tout = summer ? TCalcStandard.Tout_SHG : TCalcStandard.Tout_U;
            var Tin = summer ? TCalcStandard.Tin_SHG : TCalcStandard.Tin_U;
            SHGC = 0.0;
            // front and back glass temperatures
            var Tg_f = new double[Components.Count]; // front and back temperatures of glazings
            var Tg_b = new double[Components.Count]; // back temperatures for glazings
            var Tg_f_old = new double[Components.Count]; // back up temperatures from previous solution
            var Tg_b_old = new double[Components.Count];
            var Tg_ctr = new double[Components.Count];  // center glass temperatures
                                                        // set starting surface temperatures: distribute temperatures across glasses and gaps
            var Unit_d = 4.0;
            for (int i = 0; i < Components.Count; i++)
            {
                Unit_d += Components[i].UnitThickness;
                Unit_d += i + 1 < Components.Count ? 12.0 : 0;
            }
            var dt = ((Tin - Tout) / Unit_d);
            var dist = 2.0; // to start with a glass tempeature != Tout
            for (int i = 0; i < Components.Count; i++)
            {
                Tg_f[i] = Tout + dist * dt;
                Tg_b[i] = Tg_f[i] + (dist - 2.0 + Components[i].UnitThickness) * dt;
                dist += Components[i].UnitThickness;
                dist += i + 1 < Components.Count ? 12.0 : 0;
                Tg_ctr[i] = (Tg_f[i] + Tg_b[i]) / 2.0; // start value
            }
            // indoor irradiance
            var Gin = BlackEmi(Tin);
            // initial inside radiant heat transfer coeffcient
            var hr_in = 4 * SigmaB * eps_b[Components.Count - 1] * Math.Pow(Tin, 3.0);
            // natural convection inside
            var hc_in = hcv_int(Tg_b[Components.Count - 1], Tin);
            var h_in = hc_in + hr_in; // initial
            var Troom = Tin;
            // outside convection defined by windspeed
            var hc_out = summer ? TCalcStandard.he_SHG : TCalcStandard.he_U;
            // Outoor mean radiant temperature for clear sky
            //var Trm_out = Math.Pow((0.5 * SigmaB * Math.Pow(Tout, 4) + 0.5 * 5.31e-13 * Math.Pow(Tout, 6)) / SigmaB, 0.25);
            var Trm_out = Tout; // boundary condition NRFC 100/200
            // outdoor irradiance
            var Gout = BlackEmi(Trm_out);
            // initial radiative transfer coefficient for outdoor
            var hr_out = 4.0 * SigmaB * eps_f[0] * Math.Pow((Trm_out + Tg_f[0]) / 2.0, 3.0);
            //var h_out = hc_out + hr_out; // initial outdoor heat transfer
            var Tamb = Tout; // NFRC boundary
            // convective heat transfer coefficients for air gaps + inside / outside
            var h_d = new double[Components.Count + 1];
            // set up AM matrix and right hand side vector BV:
            // for solving the 4n independent heat balance equations at each glass unit
            int order = Components.Count * 4; // matrix size
            Matrix<double> AM = Matrix<Double>.Build.Dense(order, order, 0);
            var BV = MathNet.Numerics.LinearAlgebra.Vector<double>.Build.Dense(order, 0);
            var XS = MathNet.Numerics.LinearAlgebra.Vector<double>.Build.Dense(order, 0);
            double U_Value = 0;
            var SolRadiance = Isolar; // solar irradiance
                                      // inside heat transfer value for incident solar radiation for SHGC calculations
            var qtsolar = 0.0;
            // if solar irradiance is present calculate with and without solar irradiance to get SHGC
            var runs = SolRadiance > 0 ? 2 : 1;
            do
            {

                var deltaT = 1.0; // delta temperatures for ending iteration
                                  // get solar absorption for all glazings
                var Sabs = AbsUnits;
                for (int i = 0; i < Sabs.Length; i++)
                {
                    Sabs[i] *= SolRadiance;
                }
                var iter = 0; // iterations for solution
                XS.Clear();
                AM.Clear();
                BV.Clear();
                // outside transfer heat transfer coefficients: initial value
                hr_out = 4 * SigmaB * eps_f[0] * Math.Pow((Trm_out + Tg_f[0]) / 2.0, 3.0);
                do
                {
                    // outside heat transfer coefficient
                    h_d[0] = hc_out * (Tg_f[0] - Tamb) / (BlackEmi(Tg_f[0]) - Gout);
                    // inside transfer coefficient natural convection inside:recalc using new temperature
                    // hc_in = hcv_int(Tg_b[Components.Count - 1], Tin);
                    // inside radiant heat transfer coeffcient
                    hr_in = 4 * SigmaB * eps_b[Components.Count - 1] * Math.Pow(Tg_b[Components.Count - 1], 3.0);
                    h_d[Components.Count] = hc_in * (Tin - Tg_b[Components.Count - 1]) / (BlackEmi(Tin) - BlackEmi(Tg_b[Components.Count - 1]));
                    for (int k = 1; k < Components.Count; k++)
                    {
                        // convective heat transfer on gap i
                        var h = hcv_ISO(Tg_f[k], Tg_b[k - 1], gaps[k - 1], gas[k - 1]);
                        h_d[k] = h * (Tg_f[k] - Tg_b[k - 1]) / (BlackEmi(Tg_f[k]) - BlackEmi(Tg_b[k - 1]));
                    }
                    // set the matrix and B vector elements
                    for (int i = 0; i < order / 4; i++)
                    {
                        var rc = 4 * i;
                        {
                            AM[rc, rc] = 1.0;
                            AM[rc, rc + 1] = h_d[i];
                            AM[rc, rc + 2] = h_d[i + 1];
                            AM[rc, rc + 3] = 1;
                            AM[rc + 1, rc] = -1.0;
                            AM[rc + 1, rc + 1] = eps_f[i];
                            AM[rc + 2, rc + 2] = eps_b[i];
                            AM[rc + 2, rc + 3] = -1.0;
                            // calculate glass transfer: assume center glass absorption: slice glass in center
                            var hglf = (Tg_ctr[i] - Tg_f[i]) / (BlackEmi(Tg_ctr[i]) - BlackEmi(Tg_f[i]));
                            var hgl = (Tg_b[i] - Tg_ctr[i]) / (BlackEmi(Tg_b[i]) - BlackEmi(Tg_ctr[i]));
                            var gd2 = LamdaGlass / Components[i].UnitThickness * 1000.0 / 2.0;
                            hgl = hgl * gd2 + hglf * gd2;
                            AM[rc + 3, rc + 1] = -hgl;
                            AM[rc + 3, rc + 2] = hgl + h_d[i + 1];
                            AM[rc + 3, rc + 3] = 1.0;
                            // set outside columns
                            if (i < Components.Count - 1)
                            {
                                var col = (i + 1) * 4 - 1;
                                AM[rc, col + 2] = -h_d[i + 1];
                                AM[rc, col + 1] = -1;
                                AM[rc + 2, col + 1] = 1.0 - eps_b[i]; // rho
                                AM[rc + 3, col + 2] = -h_d[i + 1];
                                AM[rc + 3, col + 1] = -1;
                            }
                            if (i > 0)
                            {
                                AM[rc, rc - 1] = -1.0;
                                AM[rc, rc - 2] = -h_d[i];
                                AM[rc + 1, rc - 1] = 1.0 - eps_f[i];
                            }
                        }
                        // set the B vector components
                        if (i == 0)
                        {
                            // first entry: outside interface
                            BV[rc] = Sabs[i] + Gout + h_d[0] * Gout;
                            if (order == 4)
                            {
                                BV[rc] += Gin + h_d[i + 1] * Gin;
                            }
                            BV[rc + 1] = -(1.0 - eps_f[i]) * Gout;
                            BV[rc + 2] = order == 4 ? -(1.0 - eps_b[i]) * Gin : 0;
                            BV[rc + 3] = order == 4 ? 0.5 * Sabs[i] + Gin + h_d[i + 1] * Gin : 0.5 * Sabs[i];
                        }
                        else
                            if (i == Components.Count - 1)
                        {
                            // last entry: inside interface
                            BV[rc] = Sabs[i] + Gin + h_d[i + 1] * Gin;
                            BV[rc + 1] = 0; // IR transmittance = 0
                            BV[rc + 2] = -(1.0 - eps_b[i]) * Gin; // IR transmittance = 0
                            BV[rc + 3] = 0.5 * Sabs[i] + Gin + h_d[i + 1] * Gin;
                        }
                        else
                        {
                            BV[rc] = Sabs[i];
                            BV[rc + 1] = 0;
                            BV[rc + 2] = 0;
                            BV[rc + 3] = 0.5 * Sabs[i];
                        }
                    }
                    // solve the MathNet matrix equation
                    XS = AM.Solve(BV);
                    // get the new Temperatures from the Eb's
                    for (int i = 0; i < Components.Count; i++)
                    {
                        Tg_f[i] = TfromBE(XS[4 * i + 1]);
                        Tg_b[i] = TfromBE(XS[4 * i + 2]);
                        // calculate center glass temperatures: assumes absorption happens in mid glass
                        //Tg_ctr[i] = 0.5 * (Tg_f[i] - Tg_b[i]) + Tg_b[i] + Sabs[i] * Components[i].UnitThickness / 1000.0 / LamdaGlass / 8.0;
                        var d2k = Components[i].UnitThickness / 1000.0 / 2.0; // thickness of slice: Window 7 doc
                        Tg_ctr[i] = Tg_f[i] + Sabs[i] * d2k / 2.0 + d2k * XS[4 * i];
                        if (i == 0)
                        {
                            // first interface: outside
                            Tg_ctr[i] += hc_out * (Tg_f[i] - Tout) * d2k - Gout * d2k;
                        }
                        else
                        {
                            var hc = hcv_ISO(Tg_f[i], Tg_b[i - 1], gaps[i - 1], gas[i - 1]);
                            Tg_ctr[i] += d2k * (hc * (Tg_f[i] - Tg_b[i - 1]) - XS[4 * i - 1]);
                        }
                    }
                    Tamb = Tout;
                    // calculate delta temperatures to previous calculation
                    deltaT = 0;
                    for (int i = 0; i < Tg_f.Length; i++)
                    {
                        deltaT += Math.Abs(Tg_f[i] - Tg_f_old[i]) + Math.Abs(Tg_b[i] - Tg_b_old[i]);
                        // preserve current as old temperatures
                        Tg_f_old[i] = Tg_f[i];
                        Tg_b_old[i] = Tg_b[i];
                    } //
                    // inside natural convection recalc using new temperature
                    hc_in = hcv_int(Tg_b[Components.Count - 1], Tin);
                    iter++;

                } while (deltaT > 0.0001 && iter < 50);

                // calculate the total inside flowing heat flux qt for SHGC
                var qt = hc_in * (Tg_b[Components.Count - 1] - Tin) + XS[XS.Count - 1];
                if (SolRadiance > 0)
                {
                    // preserve the value for incident radiation
                    qtsolar = qt;
                }
                else
                {
                    // SHGC is calculated from the inside heat flux difference with and without incident solar radiation
                    var sfl = qtsolar - qt;
                    SHGC = qtsolar == 0 ? 0 : SolarTrans + sfl / Isolar;
                }
                // calculate U value: from thermal resistance values
                // outdoor and indoor side
                var Rtot = (Tg_f[0] - Tamb) / (hc_out * (Tg_f[0] - Tout) + XS[0] - Gout);
                var Rint = (Troom - Tg_b[Components.Count - 1]) / (hc_in * (Troom - Tg_b[Components.Count - 1]) + Gin - XS[XS.Count - 1]); // indoor
                                                                                                                                           // air gaps
                for (int i = 1; i < Components.Count; i++)
                {
                    // convective heat transfer
                    var hc = hcv_ISO(Tg_f[i], Tg_b[i - 1], gaps[i - 1], gas[i - 1]);
                    Rtot += (Tg_f[i] - Tg_b[i - 1]) / (hc * (Tg_f[i] - Tg_b[i - 1]) + XS[4 * i] - XS[4 * i - 1]);
                }
                var Rgl = 0.0;
                for (int i = 0; i < Components.Count; i++)
                {
                    var rg = Components[i].UnitThickness / 1000.0 / LamdaGlass;
                    Rgl += rg; // glass resistance
                }
                Rtot += Rint + Rgl;
                if (SolRadiance == 0)
                {
                    U_Value = Math.Abs(1.0 / Rtot);
                }
                // set solar irradiance = 0 for second run to get SHGC
                SolRadiance = 0;
                runs--;
            } while (runs > 0);
            return U_Value;
        }



        /// <summary>
        /// returns U-Vale for Winter conditions
        /// </summary>
        public double U_Value_Winter
        {
            get
            {
                if (TCalcStandard.CurrentStandard == ThermalStandard.NFRC_2001)
                {
                    //double shg = 0;
                    double uv = U_NFRC_ISO(false, out double shg, 0); // summer U, no incident solar radiation
                    return uv; // * MetricBTU; // * 0.1761; // BTU
                }
                else
                {
                    // just return normal U-value for EN
                    return U_Val(TCalcStandard.Tout_SHG, TCalcStandard.Tin_SHG);
                }
            }
        }


    }


    #region classes to hold informations for "Units" i.e. single and IGU glasses (may be extended to Triple
    // thermal calculations
    /// <summary>
    /// monolithic assembly calculations
    /// </summary>
    public class MonoUnit : GlassUnit
    {

        /// <summary>
        /// create a monoloithic glass unit which may have coatings on both sides
        /// </summary>
        /// <param name="tr">transmision spectra</param>
        /// <param name="rout">reflection outside</param>
        /// <param name="rin">reflection inside</param>
        /// <param name="UName">title of unit</param>
        /// <param name="substrName">type of substrate</param>
        /// <param name="subtrThickness">thickness of substrate</param>
        public MonoUnit(SpectralData tr, SpectralData rout, SpectralData rin,
                        string UName, string substrName, double subtrThickness)
        {
            this.Tran = tr;
            this.R_out = rout;
            if (rout.IsRFilm)
            {
                this.FrontCoating = rout.AName;
            }

            this.R_in = rin;
            if (rin.IsRFilm)
            {
                this.BackCoating = rin.AName;
            }

            this.MeType = UnitType.Mono;
            this.UnitName = UName;
            this.SubName = substrName;
            this.SubstrateThickness = subtrThickness / 1000;
            //			this.UnitThickness = subtrThickness;
            // set unit = substrate thickness in m
            Components.Add(this);   // add the current unit as a component
                                    // add some default identifier
            CoaterName = "NA";
            ProdDate = DateTime.Now;
            LoadID = 0;
            absSolar = SolarAbsorptance();  // calculate solar absorption
        }
        // all other functions / properties are already defined in the base class

        /// <summary>
        /// Create a new unit using a different substrate
        /// </summary>
        /// <param name="current">actual glass substrate</param>
        /// <param name="newglass">new glass substrate</param>
        /// <returns>a new coated glass unit</returns>
        public MonoUnit ChangeSubstrate(MateGlass current, MateGlass newglass)
        {
            double[] wl = new double[TR_Spd.Elements];
            double[] Tn = new double[TR_Spd.Elements];
            double[] RF = new double[TR_Spd.Elements];
            double[] RG = new double[TR_Spd.Elements];
            this.TR_Spd.IntensityPercent = false;       // use generic values: 0-1.0
            this.RI_Spd.IntensityPercent = false;
            this.RO_Spd.IntensityPercent = false;
            // calculate the new spectra
            for (int i = 0; i < TR_Spd.Elements; i++)
            {
                double wvl = TR_Spd[i];
                double rhos = current.rho_S(wvl);   // get the air-glass reflectance: assume both glasses have same index
                double rho_f = RI_Spd.IsRFilm ? RI_Spd.GetYval(wvl) : RO_Spd.GetYval(wvl); // get film side reflectance
                double rho_g = RO_Spd.IsRFilm ? RI_Spd.GetYval(wvl) : RO_Spd.GetYval(wvl); // glass side reflectance
                                                                                           // strip the glass off
                double tixc = current.Tintern(wvl);
                tixc = tixc > 0.01 ? tixc : 0.01;
                double D = rhos * (rho_g - rhos) + Math.Pow(1.0 - rhos, 2.0);
                double r1 = rho_f - (rhos * TR_Spd.GetYval(wvl) * TR_Spd.GetYval(wvl)) / D;
                double r2 = (rho_g - rhos) / (D * Math.Pow(tixc, 2.0));
                if (r2 > 1.0)
                {
                    r2 = rhos;
                }
                double tc = TR_Spd.GetYval(wvl) * (1.0 - rhos) / (D * tixc);
                // now add the new glass to the coating using the data of the new glass
                rhos = newglass.rho_S(wvl);
                double tixn = newglass.Tintern(wvl);
                // tixn = tixn > 0.05 ? tixn : 0.05;
                double DD = 1.0 - rhos * r2 * Math.Pow(tixn, 2.0);
                RF[i] = r1 + rhos * Math.Pow(tc, 2.0) * Math.Pow(tixn, 2.0) / DD;
                RG[i] = rhos + (r2 * Math.Pow(1.0 - rhos, 2.0) * Math.Pow(tixn, 2.0)) / DD;
                Tn[i] = (1.0 - rhos) * tixn * tc / DD;
                wl[i] = wvl;
            }
            SpectralData NewTR = new SpectralData(wl, Tn, this.TR_Spd.Filename, ColorType.TR, false);
            SpectralData NewRG = new SpectralData(wl, RG, this.R_out.Filename, ColorType.RG, false);
            SpectralData NewRF = new SpectralData(wl, RF, this.R_in.Filename, ColorType.RF, false);
            // build the new unit
            NewRF.IsRFilm = true;
            // set the current emissivity for film side
            NewRF.Eps_Hemi = this.RI_Spd.IsRFilm ? this.RI_Spd.Eps_Hemi : this.RO_Spd.Eps_Hemi;
            MonoUnit MN = new MonoUnit(NewTR, NewRG, NewRF, this.UnitName + "-M", newglass.SubName, newglass.UnitThickness);
            return MN;
        }

        /// <summary>
        /// Create a new unit using a different substrate for a double sided coating
        /// assumes the coating is symmetric and absorption free EN410 B4.2.1
        /// </summary>
        /// <param name="dorg">actual glass substrate</param>
        /// <param name="dnew">new glass substrate</param>
        /// <returns>a new coated glass unit</returns>
        public MonoUnit ChangeSubstrateDS(double dorg, double dnew)
        {
            double[] wl = new double[TR_Spd.Elements];
            double[] Tn = new double[TR_Spd.Elements];
            double[] RF = new double[TR_Spd.Elements];
            double[] RG = new double[TR_Spd.Elements];
            this.TR_Spd.IntensityPercent = false;       // use generic values: 0-1.0
            this.RI_Spd.IntensityPercent = false;
            this.RO_Spd.IntensityPercent = false;
            // calculate the new spectra
            for (int i = 0; i < TR_Spd.Elements; i++)
            {
                double wvl = TR_Spd[i];
                double rT = (RI_Spd.GetYval(wvl) + RO_Spd.GetYval(wvl)) / 2.0; // take the average for outside R
                double TT = TR_Spd.GetYval(wvl);    // total transmittance
                TT = TT > 0.0001 ? TT : 0.0001; // avoid division by 0
                                                // calculate the boundary reflectance: symmetric conditions r1 = r2 -> r12
                /*
				double a = 2.0 - rT;
				double b = rT * rT - TT * TT - 2* rT - 1.0;
				double r12 = rT > 0.0001? (-b - Math.Sqrt(b*b - 4*a*rT))/(2*a) : 0;	// this is the boundary reflectance
				// now calculate the internal transmittance of the sheet using the boundary reflectance value */
                double a = 1;
                double b = (1.0 + rT * rT - TT * TT - 2 * rT) / TT;
                double c = -1.0;
                double tiorg = (-1 * b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                double r12 = rT / (1 + tiorg * TT);
                // replace T-intern by the new thickness: recalculate based on thickness ratio
                double t1 = Math.Pow(tiorg, dnew / dorg);
                // the boundary transmittance t1*t2  is calculated as TT/Tint
                // now add the new glass to the coating using the data of the new glass
                RF[i] = r12 + TT / tiorg * t1 * t1 * r12 / (1 - r12 * r12 * t1 * t1);
                RG[i] = RF[i];
                Tn[i] = t1 * TT / tiorg / (1.0 - r12 * r12 * t1 * t1);
                wl[i] = wvl;
            }
            SpectralData NewTR = new SpectralData(wl, Tn, this.TR_Spd.Filename, ColorType.TR, false);
            SpectralData NewRG = new SpectralData(wl, RG, this.R_out.Filename, ColorType.RG, false);
            SpectralData NewRF = new SpectralData(wl, RF, this.R_in.Filename, ColorType.RF, false);
            // build the new unit
            NewRF.IsRFilm = true;
            // set the current emissivity for film side
            NewRF.Eps_Hemi = this.RI_Spd.IsRFilm ? this.RI_Spd.Eps_Hemi : this.RO_Spd.Eps_Hemi;
            MonoUnit MN = new MonoUnit(NewTR, NewRG, NewRF, this.UnitName + "-M", this.SubName, dnew);
            return MN;
        }

        /// <summary>
        /// create a double side coated unit based on a single side reference with/without a PVB interleave
        /// Assumed the coating on both sides is identical: the ' newglass" already contains the PVB in case of a lami
        /// </summary>
        /// <param name="current">current substrate reference</param>
        /// <param name="newglass">new substrate reference</param>
        public MonoUnit MakeDS(MateGlass current, MateGlass newglass)
        {
            ReferenceTable Fndx = new ReferenceTable(ISO9050Tables.FloatIndex);     // get the float index values
                                                                                    // use generic values: 0-1.0
            this.TR_Spd.IntensityPercent = false;
            this.RI_Spd.IntensityPercent = false;
            this.RO_Spd.IntensityPercent = false;
            current.TR_Spd.IntensityPercent = false;
            current.RI_Spd.IntensityPercent = false;
            newglass.TR_Spd.IntensityPercent = false;
            newglass.RI_Spd.IntensityPercent = false;
            double[] wl = new double[this.TR_Spd.Elements];
            double[] TLn = new double[this.TR_Spd.Elements];
            double[] Rfn = new double[this.TR_Spd.Elements];
            double[] Rgn = new double[this.TR_Spd.Elements];
            for (int i = 0; i < this.TR_Spd.Elements; i++)
            {
                double wvl = this.TR_Spd[i];     // get the wavelength
                                                 //glass reflectance based on Rubin index table
                double rhos = Math.Pow((Fndx.GetYval(wvl) - 1.0) / (Fndx.GetYval(wvl) + 1.0), 2.0);
                double tx = this.TR_Spd.GetYval(wvl);
                // R_in is assumed to hold the film side transmittance
                double rf = this.RI_Spd.GetYval(wvl);   // film side reflectance
                double rg = this.RO_Spd.GetYval(wvl);   // glass side reflectance
                double ti_curr = current.Tintern(wvl);  // internal transmittance of current substrate
                ti_curr = ti_curr < 0.0001 ? 0.0001 : ti_curr;  // avoid div 0
                double ti_new = newglass.Tintern(wvl);  // internal transmittance of new substrate
                ti_new = ti_new < 0.0001 ? 0.0001 : ti_new;
                // spectral reflection for current plain substrate at given thickness
                double rho_curr = rhos * (1 + (1 - rhos) * (1 - rhos) * ti_curr * ti_curr / (1 - rhos * rhos * ti_curr * ti_curr));
                //spectral parameter for the coated interface: based on EN410 A2.1: A.6-A.8
                double D = rhos * (rg - rhos) + (1 - rhos) * (1 - rhos);
                double r1 = rf - rhos * tx * tx / D;        // surface reflectance of coated side
                double r2 = (rg - rhos) / (D * ti_curr * ti_curr);      // surface reflectance for back-reflected component at film/glass
                double tc = tx * (1 - rhos) / D / ti_curr;  // coating internal transmittance
                                                            // now calculate the new configuration assuming reflectance for both interfaces is identical
                double DD = 1 - r2 * r2 * ti_new * ti_new;
                Rgn[i] = r1 + (r2 * tc * tc * ti_new * ti_new) / DD;
                Rfn[i] = Rgn[i];
                TLn[i] = (tc * tc * ti_new) / DD;
                wl[i] = wvl;
            }
            SpectralData NT = new SpectralData(wl, TLn, this.TR_Spd.AName + "-DS", ColorType.TR, false);
            SpectralData NRI = new SpectralData(wl, Rfn, this.RI_Spd.AName + "-DS", ColorType.RF, false);
            SpectralData NRO = new SpectralData(wl, Rgn, this.RO_Spd.AName + "-DS", ColorType.RG, false);
            NRI.IsRFilm = true;
            NRI.Eps_Hemi = this.RI_Spd.IsRFilm ? this.RI_Spd.Eps_Hemi : this.RO_Spd.Eps_Hemi;
            NRO.Eps_Hemi = NRI.Eps_Hemi;
            this.UnitName = this.UnitName;
            MonoUnit MN = new MonoUnit(NT, NRO, NRI, this.UnitName + "-M", newglass.SubName, newglass.UnitThickness);
            return MN;
        }


        /// <summary>
        /// Create a new unit using a different substrate for a double sided coation
        /// assumes the coating is symmetric and absorption free EN410 B4.2.1
        /// </summary>
        /// <param name="oldglass">actual glass substrate</param>
        /// <param name="newglass">new glass substrate</param>
        /// <returns>a new coated glass unit</returns>
        public MonoUnit ChangeSubstrateDS2(MateGlass oldglass, MateGlass newglass)
        {
            double[] wl = new double[TR_Spd.Elements];
            double[] Tn = new double[TR_Spd.Elements];
            double[] RF = new double[TR_Spd.Elements];
            double[] RG = new double[TR_Spd.Elements];
            this.TR_Spd.IntensityPercent = false;       // use generic values: 0-1.0
            this.RI_Spd.IntensityPercent = false;
            this.RO_Spd.IntensityPercent = false;
            // calculate the new spectra
            for (int i = 0; i < TR_Spd.Elements; i++)
            {
                double wvl = TR_Spd[i];
                double rT = (RI_Spd.GetYval(wvl) + RO_Spd.GetYval(wvl)) / 2.0; // take the average for outside R
                double TT = TR_Spd.GetYval(wvl);    // total transmittance
                TT = TT > 0.0001 ? TT : 0.0001; // avoid division by 0
                                                // calculate the boundary reflectance: symmetric conditions r1 = r2 -> r12
                                                // calculate the internal transmittance of the sheet using the boundary reflectance value
                double a = 1;
                double b = (1.0 + rT * rT - TT * TT - 2 * rT) / TT;
                double c = -1.0;
                double tiorg = (-1 * b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                double r12 = rT / (1 + tiorg * TT);
                // get the transmittance for the two coated interfaces
                // the boundary transmittance t1*t2  is calculated as TT/Tint
                double t12 = TT / oldglass.Tintern(wvl);
                // replace T-intern by the new thickness: recalculate based on thickness ratio
                double ti = newglass.Tintern(wvl);      // internal transmittance of replacement glass
                                                        // now add the new glass to the coating using the data of the new glass
                RF[i] = r12 + t12 * ti * ti * r12 / (1 - r12 * r12 * ti * ti);
                RG[i] = RF[i];
                Tn[i] = ti * t12 / (1.0 - r12 * r12 * ti * ti);
                wl[i] = wvl;
            }
            SpectralData NewTR = new (wl, Tn, this.TR_Spd.Filename, ColorType.TR, false);
            SpectralData NewRG = new (wl, RG, this.R_out.Filename, ColorType.RG, false);
            SpectralData NewRF = new(wl, RF, this.R_in.Filename, ColorType.RF, false)
            {
                // build the new unit
                IsRFilm = true,
                // set the current emissivity for film side
                Eps_Hemi = this.RI_Spd.IsRFilm ? this.RI_Spd.Eps_Hemi : this.RO_Spd.Eps_Hemi
            };
            MonoUnit MN = new (NewTR, NewRG, NewRF, this.UnitName + "-M", newglass.SubName, newglass.UnitThickness);
            return MN;
        }

    }


    /// <summary>
    /// a mate glass unit: clear glass no coating
    /// </summary>
    public class MateGlass : GlassUnit
    {
        private ReferenceTable Fndx; // = null;
        private ReferenceTable LamiTinternal; // = null;

        /// <summary>
        /// create a clear glass unit as mate
        /// </summary>
        /// <param name="tr">transmission spectra</param>
        /// <param name="rg">reflection spectra</param>
        /// <param name="substrName">substrate name </param>
        /// <param name="substrThickness">substzrate thickness</param>
        public MateGlass(SpectralData tr, SpectralData rg, string substrName, double substrThickness)
        {
            // create a bare glass unit for use as mate glass
            this.Tran = tr;
            this.R_in = rg; this.R_out = rg;
            this.SubName = substrName;
            this.UnitName = substrName;
            SubstrateThickness = substrThickness / 1000;
            this.MeType = UnitType.Mate;    // define it as a Mateglass
            Components.Add(this);   // add the current unit as a component
        }

        public MonoUnit ConvertMono
        {
            get
            {
                var mg = new MonoUnit(TR_Spd, RO_Spd, RI_Spd, SubName, SubName, SubstrateThickness);
                return mg;
            }
        }

        /// <summary>
        /// glass-air reflectance for a bare base glass
        /// </summary>
        /// <param name="wvl">wavlength (nm)</param>
        /// <returns></returns>
        public double rho_S(double wvl)
        {
            if (Fndx == null)
            {
                Fndx = new ReferenceTable(ISO9050Tables.FloatIndex);     // get the float index values
            }

            return Math.Pow((Fndx.GetYval(wvl) - 1.0) / (Fndx.GetYval(wvl) + 1.0), 2.0);
        }

        /// <summary>
        /// get the internal transmittance of PVB interlayer for a particular PVB
        /// </summary>
        /// <param name="wvl"></param>
        /// <param name="PVBTable">table of PVB internal transmission</param>
        /// <returns></returns>
        double TintLami(double wvl, string PVBTable)
        {
            if (LamiTinternal == null || LamiTinternal.AName != PVBTable)
            {
                LamiTinternal = new ReferenceTable(PVBTable);   // load the table
            }
            return LamiTinternal.GetYval(wvl);
        }

        /// <summary>
        /// internal transmittance at given wavelength: use definitions as in EN410 2011
        /// </summary>
        /// <param name="wvl">wavelength (nm)</param>
        /// <returns>internal transmittance</returns>
        public double Tintern(double wvl)
        {
            // get the actual transmittance
            double tx = TR_Spd.GetYvalPercent(wvl) / 100.0;
            if (tx <= 0.0005)
            {
                return 0;             // in case of fully absorbing region
            }

            double rhos = rho_S(wvl);
            double a = tx * rhos * rhos;
            double b = Math.Pow(1.0 - rhos, 2);
            double tix = (Math.Sqrt(b * b + 4 * a * tx) - b) / (2 * a);
            //			double tix = Math.Pow(Math.Pow(1.0 - rhos, 4.0) + (4.0 * rhos * rhos * tx * tx), 0.5) - Math.Pow(1.0 - rhos, 2.0);
            //			tix /= (2.0 * rhos * rhos * tx);
            return tix;
        }

        /// <summary>
        /// change thickness of base glass
        /// </summary>
        /// <param name="newThickness"> new thickness (mm)</param>
        public void ChangeThickness(double newThickness)
        {
            ChangeThickness(newThickness, "NA", 1.0);
        }

        /// <summary>
        /// change glass thickness and/or build a symmetric lami unit
        /// </summary>
        /// <param name="newThickness">thickness (mm) of glass on either side of the PVB</param>
        /// <param name="PVB">PVB table name for internal absorption</param>
        /// <param name="dLami">relative thickness of PVB to reference in file (1.0)</param>
        public void ChangeThickness(double newThickness, string PVB, double dLami)
        {
            bool buildLami = PVB != "NA";   // set a flag in case for a plain thickness change
            if (newThickness == this.UnitThickness && !buildLami)
            {
                return; // same thickness and no PVB just return
            }

            double[] wl = new double[TR_Spd.Elements];
            double[] Tn = new double[TR_Spd.Elements];
            double[] Rn = new double[TR_Spd.Elements];
            this.TR_Spd.IntensityPercent = false;       // use generic values: 0-1.0
            for (int i = 0; i < TR_Spd.Elements; i++)
            {
                wl[i] = TR_Spd[i];
                // get the original T intern and reflection values
                double rhos = rho_S(wl[i]);
                double tx = TR_Spd.GetYval(wl[i]);
                double tix = Tintern(wl[i]);
                // now calculate the new values and build the nominal values
                double tiy = Math.Pow(tix, newThickness / this.UnitThickness); ;
                if (buildLami)
                {
                    // add the lami T intern and double the glass thickness assuming identical glass on both sides
                    // TinternLami may have half or double thickness of the base reference (0.76 mm = .2 type)
                    tiy = tiy * tiy * Math.Pow(TintLami(wl[i], PVB), dLami);
                }
                double TY = Math.Pow(1.0 - rhos, 2.0) * tiy / (1.0 - rhos * rhos * tiy * tiy);
                double RY = rhos * (1.0 + Math.Pow(1.0 - rhos, 2.0) * tiy * tiy / (1.0 - rhos * rhos * tiy * tiy));
                Tn[i] = TY;
                Rn[i] = RY;
            }
            SpectralData NewTR = new SpectralData(wl, Tn, "FloatT-" + newThickness.ToString("F1"), ColorType.TR, false);
            SpectralData NewRG = new SpectralData(wl, Rn, "FloatR-" + newThickness.ToString("F1"), ColorType.RG, false);
            // assign the new spectra to the base spectra
            this.Tran = NewTR;
            this.R_in = NewRG;
            this.R_out = NewRG;
            // unit thickness is here identical with substrate thickness
            this.UnitThickness = buildLami ? (newThickness * 2 + 0.76 * dLami) : newThickness;
            //			this.SubstrateThickness = newThickness/1000.0;	// set also substrate thickness
            this.SubName = this.SubName + (buildLami ? "-Lami" : "");
            absSolar = SolarAbsorptance();  // calculate new solar absorption
        }


        /// <summary>
        /// add a simulated frit coverage to reduce energy transfer
        /// </summary>
        /// <param name="pctCoverage">percent frit coverage</param>
        public void AddFritCoverage(double pctCoverage)
        {
            double trans = 1.0 - pctCoverage / 100;
            this.TR_Spd.IntensityPercent = false;       // use generic values: 0-1.0
            for (int i = 0; i < TR_Spd.Elements; i++)
            {
                TR_Spd.SetYval(i, TR_Spd.GetYval(TR_Spd[i]) * trans);
            }
            absSolar = SolarAbsorptance();
        }
    }

    /// <summary>
    /// class to calculate a laminated unit
    /// </summary>
    public class LamiUnit : GlassUnit
    {
        // this defines a laminated glass unit - used like a mono glass
        private string LamiName { get; set; } = String.Empty;
        /// <summary>
        /// default PVB name
        /// </summary>
        public const string DefaultPVB = "PVB-076_LAMI";
        public double PVBthickness { get; set; } = 0.75;   //! default PVB thickness
        /// <summary>
        /// returns the name of the laminated mate glass
        /// </summary>
        public static string GetLamiMateGlass { get { return BackGlass.SubName; } }
        /// <summary>
        /// position of stack design in front of PVB
        /// </summary>
        public string? DesignPos2 { get; set; } = "None";
        /// <summary>
        /// design at position after PVB
        /// </summary>
        public string? DesignPos3 { get; set; } = "None";
        /// <summary>
        /// thickness of back glass after PVB
        /// </summary>
        public static double BackGlassThickness { get { return BackGlass.UnitThickness; } }
        /// <summary>
        /// thickness of glass in front of PVB
        /// </summary>
        public static double FrontGlassThickness { get { return FrontGlass.UnitThickness; } }

        public static GlassUnit BackGlass { get; set; }
        public static GlassUnit FrontGlass { get; set; }   
        public static ReferenceTable PVBTint { get; set; } //internal transmission for PVB layer

        
        /// <summary>
        /// Calculate a laminated glass unit with front and back glass
        /// </summary>
        /// <param name="EmbDsg">spectra for coated glass with incidence medium glass</param>
        /// <param name="Mate">the glass unit used as mat glass</param>
        /// <param name="angle">incidence angle</param>
        public LamiUnit(GlassUnit EmbDsg, GlassUnit Mate, double angle)
        {
            // use a default PVB table in case there wer no specific data set
            PVBTint ??= new ReferenceTable(DefaultPVB);
            DesignPos2 = EmbDsg.UnitName;
            double[] wl = new double[EmbDsg.TR_Spd.Elements];
            double[] TLn = new double[EmbDsg.TR_Spd.Elements];
            double[] Rfn = new double[EmbDsg.TR_Spd.Elements];
            double[] Rgn = new double[EmbDsg.TR_Spd.Elements];
            var arad = Math.PI / 180.0 * angle; // get the angle in radians for sin/cos calculations
            Mate ??= BackGlass;  // if Mate is not value use a default declared as BackGlass
            if (Mate.TR_Spd.IntensityPercent)
            {
                Mate.TR_Spd.IntensityPercent = false;
                Mate.RI_Spd.IntensityPercent = false;
                Mate.RO_Spd.IntensityPercent = false;
            }
            for (int i = 0; i < EmbDsg.TR_Spd.wvl.Count; i++)
            {
                wl[i] = EmbDsg.TR_Spd.wvl[i];
                var (t1, r1) = InterfacesClear(Mate.TR_Spd.GetYval(wl[i]), Mate.RO_Spd.GetYval(wl[i]), arad, Mate.UnitThickness);
                var t2 = EmbDsg.TR_Spd.GetYval(wl[i]) * (1.0 - r1); // transmission of stack: correct for glass surface reflection
                t1 *= PVBTint.GetYval(wl[i]); // add the PVB internal transmission to the front glass 
                var den = 1.0 - r1 * t1 * t1 * EmbDsg.RI_Spd.GetYval(wl[i]);
                TLn[i] = t1 * t2 / den; // total transmission
                Rgn[i] = EmbDsg.RO_Spd.GetYval(wl[i]) + r1 * t1 * t1 * t2 * t2 / den; // outside reflection
                Rfn[i] = r1 + t1 * t1 * EmbDsg.RI_Spd.GetYval(wl[i]) / den;
            }
            this.Tran = new SpectralData(wl, TLn, EmbDsg.TR_Spd.AName + "-L", ColorType.TR, false);
            this.R_in = new SpectralData(wl, Rfn, EmbDsg.RI_Spd.AName + "-L", ColorType.RF, false);
            this.R_out = new SpectralData(wl, Rgn, EmbDsg.RO_Spd.AName + "-L", ColorType.RG, false);
            this.UnitName = EmbDsg.UnitName;
            this.LamiName = PVBTint.AName;
            this.UnitThickness = EmbDsg.UnitThickness + Mate.UnitThickness;
            this.MeType = UnitType.Lami;
            Components.Add(this);   // add the current unit as a component
            absSolar = this.SolarAbsorptance(); // calculate solar absorptance
        }

        public LamiUnit(GlassUnit EmbDsg, double angle)
        {
            if (FrontGlass == null || BackGlass == null || PVBTint == null)
            {
                LamiName = "Invalid Parameters";                
                return;
            }
            DesignPos2 = EmbDsg.UnitName;
            double[] wl = new double[EmbDsg.TR_Spd.Elements];
            double[] TLn = new double[EmbDsg.TR_Spd.Elements];
            double[] Rfn = new double[EmbDsg.TR_Spd.Elements];
            double[] Rgn = new double[EmbDsg.TR_Spd.Elements];
            var arad = Math.PI / 180.0 * angle; // get the angle in radians for sin/cos calculations
            if (FrontGlass.TR_Spd.IntensityPercent)
            {
                FrontGlass.TR_Spd.IntensityPercent = false;
                FrontGlass.RI_Spd.IntensityPercent = false;
                FrontGlass.RO_Spd.IntensityPercent = false;
            }
            if (BackGlass.TR_Spd.IntensityPercent)
            {
                BackGlass.TR_Spd.IntensityPercent = false;
                BackGlass.RI_Spd.IntensityPercent = false;
                BackGlass.RO_Spd.IntensityPercent = false;
            }
            for (int i = 0; i < EmbDsg.TR_Spd.wvl.Count; i++)
            {
                wl[i] = EmbDsg.TR_Spd.wvl[i];
                var (t1, r1) = InterfacesClear(FrontGlass.TR_Spd.GetYval(wl[i]), FrontGlass.RO_Spd.GetYval(wl[i]), arad, FrontGlass.UnitThickness);
                var (t3, r3) = InterfacesClear(BackGlass.TR_Spd.GetYval(wl[i]), BackGlass.RO_Spd.GetYval(wl[i]), arad, BackGlass.UnitThickness);
                t3 *= PVBTint.GetYval(wl[i]); // add the PVB internal transmission to the back glass 
                var t2 = EmbDsg.TR_Spd.GetYval(wl[i]) * (1.0 - r1 - r3); // transmission of stack
                //var t2 = (1.0 - EmbDsg.RI_Spd.GetYval(wl[i])); // transmission of stack
                // build the total T and R values
                var den = (1.0 - r1 * t1 * t1 * EmbDsg.RO_Spd.GetYval(wl[i])) * (1.0 - EmbDsg.RI_Spd.GetYval(wl[i]) * t3 * t3 * r3) -
                    r1 * t1 * t1 * t2 * t2 * t3 * t3 * r3;
                TLn[i] = t1 * t2 * t3 / den;
                var rT = t1 * t1 * EmbDsg.RO_Spd.GetYval(wl[i]) * (1.0 - EmbDsg.RI_Spd.GetYval(wl[i]) * t3 * t3 * r3) + t1 * t1 * t2 * t2 * t3 * t3 * r3;
                Rfn[i] = r1 + rT / den;     // front side reflection
                rT = t3 * t3 * EmbDsg.RI_Spd.GetYval(wl[i]) * (1.0 - r1 * t1 * t1 * r1) + t3 * t3 * t1 * t1 * t2 * t2 * r1;
                Rgn[i] = r3 + rT / den; // back side reflection
            }
            this.Tran = new SpectralData(wl, TLn, EmbDsg.TR_Spd.AName + "-L", ColorType.TR, false);
            this.R_in = new SpectralData(wl, Rfn, EmbDsg.RI_Spd.AName + "-L", ColorType.RF, false);
            this.R_out = new SpectralData(wl, Rgn, EmbDsg.RO_Spd.AName + "-L", ColorType.RG, false);
            this.UnitName = EmbDsg.UnitName;
            this.LamiName = PVBTint.AName;
            this.UnitThickness = FrontGlass.UnitThickness + BackGlass.UnitThickness;
            this.MeType = UnitType.Lami;
            Components.Add(this);   // add the current unit as a component
            absSolar = this.SolarAbsorptance(); // calculate solar absorptance

        }

        /// <summary>
        /// calculate internal transmission and interface reflection for clear glass
        /// front and back are identical t1 = t2: adjust angular incidence and thickness correction
        /// based on EN410:2011 B.4.2
        /// </summary>
        /// <param name="tauT">total transmisssion at 0°</param>
        /// <param name="rhoT">total reflectionat 0° </param>
        /// <returns>ti internal transmission - interface reflection</returns>
        private static (double ti, double r1) InterfacesClear(double tauT, double rhoT, double angle, double dglass)
        {
            var b = (1.0 + rhoT * rhoT - tauT * tauT - 2.0 * rhoT) / tauT;
            var t10 = (-b + Math.Sqrt(b * b + 4.0)) / 2.0;
            var r10 = rhoT / (1.0 + tauT * t10);
            if (angle < 0.15)   // less than 10°
            {
                return (t10, r10);
            }
            // calculate the T/R for angular conditions (based on Window documentation)
            var r100 = Math.Sqrt(r10);
            var nd = (1.0 + r100) / (1.0 - r100); // refractive index
            var aglass = Math.Asin(Math.Sin(angle) / nd); // the angle inside glass
            var cosplus = nd *Math.Cos(angle) + Math.Cos(aglass);
            var cosmin = nd *Math.Cos(angle) - Math.Cos(aglass);
            var r1f = (cosmin / cosplus * cosmin / cosplus);
            cosplus = nd * Math.Cos(aglass) + Math.Cos(angle);
            cosmin = nd * Math.Cos(aglass) - Math.Cos(angle);
            r1f += (cosmin / cosplus * cosmin / cosplus);
            r1f *= 0.5;
            // correct the glass absorption
            var tcorr = Math.Pow(t10, dglass / Math.Cos(aglass) / dglass);
            return (tcorr, r1f);
        }

       
        /// <summary>
        /// create a lami unit using two mono glasses with a PVB interleave
        /// the mono units are calculated with glass as exit /entry medium
        /// </summary>
        /// <param name="Outer">outside unit</param>
        /// <param name="pvbMat">name of PVB material file</param>
        /// <param name="Mate">inside unit</param>
        public LamiUnit(MonoUnit Outer, ReferenceTable PVBdata, MonoUnit Mate, double PVBthickness)
        {
            PVBTint = PVBdata;
            // create a dummy "mate" using the pvb internal transmittance and zero reflectance
            double[] wlpvb = new double[Outer.TR_Spd.Elements];
            double[] tintpvb = new double[Outer.TR_Spd.Elements];
            double[] rftpvb = new double[Outer.TR_Spd.Elements];
            for (int i = 0; i < Outer.TR_Spd.Elements; i++)
            {
                wlpvb[i] = Outer.TR_Spd[i];  // get the wavelength based on the outer pane
                tintpvb[i] = PVBTint.GetYval(wlpvb[i]);
                rftpvb[i] = 0.0;
            }
            SpectralData TR_pvb = new SpectralData(wlpvb, tintpvb, "T-PVB", ColorType.TR, false);
            SpectralData R_pvb = new SpectralData(wlpvb, rftpvb, "R-PVB", ColorType.RG, false);
            // create a Mate type for the PVB portion: use a default thickness given by the PVB material
            MateGlass PVB_unit = new (TR_pvb, R_pvb, PVBdata.AName,PVBthickness);
            // now create a list of glass units to calculate the final optical properties
            List<GlassUnit> lunints = new List<GlassUnit>();
            // put the units inside to outside to stay compatible with thermal calculations: changed !!
            //lunints.Add(Mate); lunints.Add(PVB_unit); lunints.Add(Outer);
            lunints.Add(Outer); lunints.Add(PVB_unit); lunints.Add(Mate);
            double[] absSP = new double[3];
            CalcMultiSheetOptics(lunints, out Tran, out R_out, out R_in, out _);
            // set emssivity and coating flag in case of coated outside surfaces
            R_out.IsRFilm = Outer.RO_Spd.IsRFilm;
            R_out.Eps_Hemi = Outer.RO_Spd.Eps_Hemi;
            R_in.IsRFilm = Mate.RI_Spd.IsRFilm;
            R_in.Eps_Hemi = Mate.RI_Spd.Eps_Hemi;
            this.UnitName = Outer.UnitName;
            this.LamiName = PVBdata.AName;
            this.UnitThickness = Outer.UnitThickness + Mate.UnitThickness;
            //			this.SubstrateThickness = Outer.UnitThickness;
            this.MeType = UnitType.Lami;
            this.FrontCoating = Outer.FrontCoating;
            this.BackCoating = Mate.BackCoating;
            this.SubName = Outer.SubName;
            Components.Add(this);   // add the current unit as a component
                                    // need to calculate the final total solar absorption
            absSolar = this.SolarAbsorptance(); // calculate solar absorptance for unit
                                                // save configuration for reconfiguration
            this.SubstrateName = Outer.SubName;
            //this.LMateGlass = Mate.SubName;
            //FrontGlassThickness = Mate.UnitThickness;
            //BackGlassThickness = Outer.UnitThickness;
            this.DesignPos2 = Outer.BackCoating;
            this.DesignPos3 = Mate.FrontCoating;
        }

        public string PVBtype
        {
            get { return PVBTint.AName; }
        }



    }


    /// <summary>
    /// derived class: extends to a IGU unit
    /// inherits spectral / solar calculations from single glass pane
    /// </summary>
    public class IGU_Unit : GlassUnit
    {

        /// <summary>
        /// build a IGU unit based on an outer and inner glass pane
        /// </summary>
        /// <param name="outer">outside glass</param>
        /// <param name="inner">inside glass</param>
        /// <param name="agap">air gap in mm</param>
        public IGU_Unit(GlassUnit outer, GlassUnit inner, double agap, string UnName)
        {
            if (outer.TR_Spd.Elements != inner.TR_Spd.Elements)
            {
                // need to adjust for same range of data
                GlassUnit leadUnit = outer.TR_Spd.Elements > inner.TR_Spd.Elements ? outer : inner; // select the unit with the larger range
                // find the largest number of wvl steps in order to adjust all others to it by extrapolation
                int largeSize = leadUnit.TR_Spd.Elements;
                double[] wl = new double[largeSize];    // create a wavelength base for reference
                for (int i = 0; i < largeSize; i++)
                {
                    wl[i] = leadUnit.TR_Spd[i];   // get the wavelenght base from the larger base
                }
                // add the components to the component list
                if (outer.TR_Spd.Elements < leadUnit.TR_Spd.Elements)
                {
                    Components.Add(Extrapolite(outer, wl));
                }
                else
                {
                    Components.Add(outer);
                }

                if (inner.TR_Spd.Elements < leadUnit.TR_Spd.Elements)
                {
                    Components.Add(Extrapolite(inner, wl));
                }
                else
                {
                    Components.Add(inner);
                }
            }
            else
            {
                Components.Add(outer);
                Components.Add(inner);
            }
            this.gaps.Add(agap / 1000.0);   // add the air gap
            gas.Add(FillingGas.Air);    // add a default gas
                                        // inherit the color scale and calculation standard from the outer glass pane
            this.CIE_Illuminant = outer.CIE_Illuminant;
            this.CIE_Observer = outer.CIE_Observer;
            //			this.CalculationStandard = outer.CalculationStandard;
            this.UnitName = UnName;
            //			CalcIGUSpectral(); // calculate the optical characteristics
            // updated general version: calculates outside in: therefore units need to be in reversed order
            CalcMultiSheetOptics(Components, out Tran, out R_out, out R_in, out absSolar);
            this.MeType = UnitType.IGU;
        }



        /// <summary>
        /// set emissivity value for given surface
        /// </summary>
        /// <param name="eps">emissivity value</param>
        /// <param name="surface">surface 1-outside</param>
        /// <param name="hemi">value is hemispherical emissivity</param>
        /// <returns></returns>
        public override double SetEpsilon(double eps, int surface, bool hemi)
        {
            // set emissivity for surface number: 1 is outside, 0 is a default looking for a film side
            // return the hemisherical value in case it was set or estimated
            if (surface > Components.Count * 2 || surface < 1)
            {
                return 0.837; // invalid parameter
            }

            SpectralData StoSet = Components[1].RO_Spd;     // set a default surface: this may be just glass
            switch (surface)
            {
                case 1:
                    StoSet = Components[0].RO_Spd;
                    break;
                case 2:
                    StoSet = Components[0].RI_Spd;
                    break;
                case 3:
                    StoSet = Components[1].RO_Spd;
                    break;
                case 4:
                    StoSet = Components[1].RI_Spd;
                    break;
                default:
                    // estimate the emissivity for the coated side
                    hemi = true;    // return hemisherical emissivity
                    for (int i = 0; i < 2; i++)
                    {
                        if (Components[i].RO_Spd.IsRFilm) { StoSet = Components[i].RO_Spd; continue; };
                        if (Components[i].RI_Spd.IsRFilm) { StoSet = Components[i].RI_Spd; continue; };
                    }
                    if (StoSet.IsRFilm)
                    {
                        StoSet.IsRFilm = true;  // re-estimate emissivity based on NIR reflectance
                    }
                    eps = StoSet.eps_c();
                    break;
            }
            if (!hemi)
            {
                StoSet.Eps_Hemi = TCalcStandard.CurrentStandard == ThermalStandard.NFRC_2001 ? SpectralData.EpsH_LBNL(eps) : SpectralData.EpsConversionEN(eps, false);
            }
            return StoSet.Eps_Hemi;
        }

       /// <summary>
       /// get the total thickness of the IGU assembly
       /// </summary>
        public override double UnitThickness
        {
            get
            {
                return this.Components[0].UnitThickness + this.Components[1].UnitThickness + this.gaps[0] * 1000.0;
            }
        }

        /// <summary>
        /// reverse inside and outside pane for unit: S2 -> S3
        /// </summary>
        public override void ReverseUnit()
        {
            Components.Reverse();       // reverse the units
                                        // now flip sides of individual components to move coating: S2 moves to S3
                                        // now reverse the inside and outside reflectance spectra
                                        //SpectralData temp = R_out;
                                        //R_out = R_in; R_in= temp;
            for (int i = 0; i < Components.Count; i++)
            {
                Components[i].ReverseUnit();
            }

            CalcMultiSheetOptics(Components, out Tran, out R_out, out R_in, out absSolar);
            //absSolar = this.SolarAbsorptance(); // recalculate solar absorptance
        }

        /// <summary>
        /// calculate the U-Value: heat transfer coefficient
        /// </summary>
        /// <returns>U-value</returns>
        protected override double U_Val(double T_out, double T_in)
        {
            double sh;
            if (!TCalcStandard.ISOcalc)
            {
                return U_NFRC_ISO(false, out sh, 0);  // use the NFRC method, winter
            }
            double he = TCalcStandard.he_U;
            double hi = 3.6 + hr_intSurface(Components[1].RI_Spd.Eps_Hemi);
            double Tgap = (TCalcStandard.Tin_U + TCalcStandard.Tout_U) / 2.0;  // average Temperature
            double hs = 0;  // thermal heat transfer coefficient of the gap hs
            double ht = hcv_ISO(T_out, T_in, gaps[0], gas[0]) + hr(Tgap, 0); // get the convective + radiative transfer coeff
                                                                             // add the thermal transfer coefficent for the glass sheets:  rj = 1.0 m K/W
            hs = 1.0 / ht + Components[1].UnitThickness / 1000.0 + Components[0].UnitThickness / 1000.0;
            //return the U-value:
            if (he == 0)
            {
                return 1.0 / hs; // VIG special case
            }

            double U = 1.0 / (1.0 / he + 1.0 / hi + hs);
            return U;
        }

        /// <summary>
        /// calculate the secondary heat transfer coefficient: ISO based procedure for g-factor SHGC
        /// </summary>
        /// <returns>secondary heat transfer factor</returns>
        private double qi()
        {
            // uses (extern) solar absorptance for inner / outer glass unit
            double ae1 = absSolar[0]; double ae2 = absSolar[1];
            double he = TCalcStandard.he_SHG;    // outside heat transfer factor
                                                 // inside heat transfer factor: convection + radiation at internal surface
            double hi = TCalcStandard.hi_SHG + hr_intSurface(Components[1].RI_Spd.Eps_Hemi);
            //estimate the gap temperature:
            double Tgap = (TCalcStandard.Tout_SHG + TCalcStandard.Tin_SHG) / 2;     // take average temperature
            double hr = hr_int(Tgap, Components[0].RI_Spd.Eps_Hemi, Components[1].RO_Spd.Eps_Hemi);
            double DeltaT = Math.Abs(TCalcStandard.Tin_SHG - TCalcStandard.Tout_SHG);
            // get the convective transfer coeff, radiation is included in the solar term
            double hg = hcv_ISO(TCalcStandard.Tin_SHG, TCalcStandard.Tout_SHG, gaps[0], gas[0]);
            // add the convective and radiative components
            double hs = hg + hr;
            // add the thermal resistance for the two glasses
            hs = 1.0 / hs +
                (Components[1].UnitThickness + Components[0].UnitThickness) / 1000.0;
            double lamb = 1.0 / hs; //  get the total heat transfer coefficient lambda toward the inside
            double qi = (ae1 + ae2) / he + ae2 / lamb;
            qi /= (1.0 / hi + 1.0 / he + 1.0 / lamb);
            return qi;
        }

        /// <summary>
        /// calculate solar absorptance values for outer and inner glass sheet
        /// </summary>
        /// <param name="ae1">A outer glass</param>
        /// <param name="ae2">A inner glass</param>
        protected override double[] SolarAbsorptance()
        {
            string solfile = TCalcStandard.SolarFile;   // get the solar energy data table
            if ((SolarRadiation == null) || (SolarRadiation.AName != solfile))  // load the correct table if not there
            {
                SolarRadiation = new ReferenceTable(solfile);
            }
            // calculate solar absorption for inner / outer sheet in IGU configuration
            MakeSpectraGeneric();
            double ae_out = 0;
            double ae_in = 0;
            double Sl = 0;
            for (int w = 0; w < SolarRadiation.Elements; w++)
            {
                double wl = SolarRadiation[w]; // get the wavelength
                Sl += SolarRadiation.GetYval(wl);
                double a1 = 1.0 - Components[1].TR_Spd.GetYval(wl) - Components[1].RO_Spd.GetYval(wl);
                double a2 = 1.0 - Components[0].TR_Spd.GetYval(wl) - Components[0].RO_Spd.GetYval(wl);
                double a1_ = 1.0 - Components[1].TR_Spd.GetYval(wl) - Components[1].RI_Spd.GetYval(wl);
                ae_out += SolarRadiation.GetYval(wl) * (a1 + (a1_ * Components[1].TR_Spd.GetYval(wl) *
                                                              Components[0].RO_Spd.GetYval(wl)) /
                                                        (1.0 - Components[1].RI_Spd.GetYval(wl) * Components[0].RO_Spd.GetYval(wl)));
                ae_in += SolarRadiation.GetYval(wl) * (a2 * Components[1].TR_Spd.GetYval(wl)) /
                    (1.0 - Components[1].RI_Spd.GetYval(wl) * Components[0].RO_Spd.GetYval(wl));
            }
            ae_out /= Sl;
            ae_in /= Sl;
            double[] sab = new double[Components.Count];
            sab[0] = ae_in; sab[1] = ae_out;
            return sab;
        }

        /// <summary>
        /// returns the g-value according to ISO standard
        /// </summary>
        /// <returns>g value for EU or SHGC for NFRC</returns>
        public override double SolarGain(out double U_summer)
        {
            //absSolar = SolarAbsorptance();  // recalc absorptance as standard may have changed
            double gv = SolarCharacteristic(Tran);
            if (!TCalcStandard.ISOcalc)
            {
                double US = U_NFRC_ISO(true, out gv, TCalcStandard.SolarE);  // get the SHGC calculated according to ASHRAE
                U_summer = US; // * MetricBTU;
            }
            else
            {
                gv += qi();
                U_summer = U_Value_Winter;
            }
            return gv;
        }

        /// <summary>
        /// returns a string indicating the evaluation conditions
        /// </summary>
        /// <returns>thermal calculation standard</returns>
        public override string EvalConditions()
        {
            string etxt = "IGU using " + Components[1].UnitThickness.ToString("F1") + " mm " + Components[1].UnitName + " | ";
            etxt += this.AirGap_mm(0).ToString("F0") + " mm gap " + this.GasFilling(0).ToString() + " | ";
            etxt += Components[0].UnitThickness.ToString("F1") + "mm " + Components[0].UnitName + " ¦ " + TCalcStandard.CurrentStandard.ToString();
            return etxt;
        }

        /// <summary>
        /// reports color scale in string form
        /// </summary>
        /// <returns>CIE color standard</returns>
        public override string ColorScale()
        {
            string clrhdr = "IGU Color: ";
            clrhdr += this.CIE_Illuminant.ToString() + "/";
            if (this.CIE_Observer == CIEObservers.CIE_1931)
            {
                clrhdr += "2°";
            }
            else
            {
                clrhdr += "10°";
            }

            return clrhdr;
        }

    }


    /// <summary>
    /// derived class: extends to a triple IGU unit
    /// inherits spectral / solar calculations from single glass pane
    /// </summary>
    public class Triple_Unit : GlassUnit
    {
        // the spectral chracteristics of the base class are here used as the corresponding

        /// <summary>
        /// build a IGU unit based on an outer and inner glass pane
        /// </summary>
        /// <param name="outer">outside glass</param>
        /// <param name="gap1"> gap in mm </param>
        /// <param name="center">center glass </param>
        /// <param name="gap2"> gap in mm</param>
        /// <param name="inner">inside glass</param>
        /// <param name="UnName">name of unit</param>
        public Triple_Unit(GlassUnit outer, double gap1, GlassUnit center, double gap2, GlassUnit inner,
                           string UnName)
        {
            if (outer.TR_Spd.Elements != inner.TR_Spd.Elements || outer.TR_Spd.Elements != center.TR_Spd.Elements)
            {
                // need to adjust for same range of data
                GlassUnit leadUnit = outer.TR_Spd.Elements > inner.TR_Spd.Elements ? outer : inner; // select the unit with the larger range
                leadUnit = leadUnit.TR_Spd.Elements > center.TR_Spd.Elements ? leadUnit : center;   // verify the center
                                                                                                    // find the largest number of wvl steps in order to adjust all others to it by extrapolation
                int largeSize = leadUnit.TR_Spd.Elements;
                double[] wl = new double[largeSize];    // create a wavelength base for reference
                for (int i = 0; i < largeSize; i++)
                {
                    wl[i] = leadUnit.TR_Spd[i];   // get the wavelenght base from the larger base
                }
                // add the components to the component list
                if (outer.TR_Spd.Elements < leadUnit.TR_Spd.Elements)
                {
                    Components.Add(Extrapolite(outer, wl));
                }
                else
                {
                    Components.Add(outer);
                }

                if (center.TR_Spd.Elements < leadUnit.TR_Spd.Elements)
                {
                    Components.Add(Extrapolite(center, wl));
                }
                else
                {
                    Components.Add(center);
                }

                if (inner.TR_Spd.Elements < leadUnit.TR_Spd.Elements)
                {
                    Components.Add(Extrapolite(inner, wl));
                }
                else
                {
                    Components.Add(inner);
                }
            }
            else
            {
                Components.Add(outer);
                Components.Add(center);
                Components.Add(inner);  // internal list starts with inner pane
            }
            // also gaps and gas in forward direction
            this.gaps.Add(gap1 / 1000.0); gas.Add(FillingGas.Air);  // add a default gas / gap
            this.gaps.Add(gap2 / 1000.0); gas.Add(FillingGas.Air);  // add a default gas / gap
                                                                    // inherit the color scale and calculation standard from the outer glass pane
            this.CIE_Illuminant = outer.CIE_Illuminant;
            this.CIE_Observer = outer.CIE_Observer;
            //			this.CalculationStandard = outer.CalculationStandard;
            this.UnitName = UnName;
            // calculate the optical characteristics of total unit
            CalcMultiSheetOptics(Components, out Tran, out R_out, out R_in, out absSolar);
            this.MeType = UnitType.Triple;
        }


        /// <summary>
        /// get/set hickness of the component return in mm
        /// </summary>
        public override double UnitThickness
        {
            get
            {
                double d = 0;
                foreach (GlassUnit co in Components)
                {
                    d += co.UnitThickness;
                }
                foreach (double gd in gaps)
                {
                    d += gd * 1000;
                }
                return d;
            }
            //			set { this.UnitThickness = value;}
        }

        /// <summary>
        /// reverse inside and outside pane for unit: S2 -> S3
        /// </summary>
        public override void ReverseUnit()
        {
            Components.Reverse();		// reverse the units and the gaps / gas definitions
            gaps.Reverse();
            gas.Reverse();
            // now flip sides of individual components to move coating: S2 moves to S3
            for (int i = 0; i < Components.Count; i++)
            {
                Components[i].ReverseUnit();
            }
            // now reverse the inside and outside reflectance spectra
            //SpectralData temp = R_out;
            //R_out = R_in; R_in = temp;
            CalcMultiSheetOptics(Components, out Tran, out R_out, out R_in, out absSolar);
        }


        /// <summary>
        /// calculate the U-Value: heat transfer coefficient
        /// </summary>
        /// <returns>U-value</returns>
        protected override double U_Val(double T_out, double T_in)
        {
            double sh ;
            if (!TCalcStandard.ISOcalc)
            {
                return U_NFRC_ISO(false, out sh, 0);  // use the NFRC method
            }
            double he = TCalcStandard.he_U;
            double hi = 3.6 + hr_intSurface(Components[2].RI_Spd.Eps_Hemi);
            double[] hs = hsGaps();  // thermal heat transfer coefficient of the gaps hs
                                     //return the U-value:
            double U = (1.0 / he + 1.0 / hi);
            // add the thermal transfer coefficients and the glass resistance
            for (int i = 0; i < hs.Length; i++)
            {
                U += 1.0 / hs[i] + (Components[i].UnitThickness + Components[i + 1].UnitThickness) / 2000.0;
                // add the rest for the inner and outer pane
                if (i == 0)
                {
                    U += Components[0].UnitThickness / 2000.0;
                }

                if (i == Components.Count - 2)
                {
                    U += Components[i + 1].UnitThickness / 2000.0;
                }
            }
            return 1.0 / U;
        }


        /// <summary>
        /// calculate the secondary heat transfer coefficient: ISO based procedure for g-factor SHGC
        /// </summary>
        /// <returns>secondary heat transfer factor</returns>
        private double qi()
        {
            double he = TCalcStandard.he_SHG;    // outside heat transfer factor
                                                 // inside heat transfer factor: convection + radiation at internal surface
            double hi = TCalcStandard.hi_SHG + hr_intSurface(Components[2].RI_Spd.Eps_Hemi);
            //estimate the gap temperatures:
            double Tgap = 0.5 * (TCalcStandard.Tout_SHG + TCalcStandard.Tin_SHG);   // average gap temperature
            double DeltaT = Math.Abs(TCalcStandard.Tin_SHG - TCalcStandard.Tout_SHG);
            // estimate the convective and radiative heat transfer for the air gaps
            double[] hcv = hsGaps();
            // now build the total internal heat transfer coefficient
            double[] h = new double[Components.Count + 1];
            //heat transfer coeffients of ouiter and inner surface
            h[0] = 1.0 / he; h[Components.Count] = 1.0 / hi;
            // get the 1/Lambda include glass resistance
            for (int i = 0; i < hcv.Length; i++)
            {
                h[i + 1] = 1.0 / hcv[i] + (Components[i].UnitThickness + Components[i + 1].UnitThickness) / 2000.0;
                // add the rest for the inner and outer pane
                if (i == 0)
                {
                    h[i + 1] += Components[0].UnitThickness / 2000.0;
                }

                if (i == Components.Count - 2)
                {
                    h[i + 1] += Components[i + 1].UnitThickness / 2000.0;
                }
            }
            double qz = 0; double qn = 0;
            for (int i = 0; i < h.Length; i++)
            {
                qz += h[i];     // add all transfer coefficients
            }
            for (int i = 0; i < Components.Count; i++)
            {
                double term = 0;
                for (int j = i; j < Components.Count; j++)
                {
                    term += absSolar[j];
                }
                qn += term * h[i];
            }
            double qin = qn / qz;   // secondary heat transfer coefficient
            return qin;
        }

        /// <summary>
        /// thermal transfer coefficient based on EN673:1997 p16 Appendix B
        /// </summary>
        /// <returns>thermal conductance inside to outside</returns>
        private double[] hsGaps()
        {
            // based on EN 673:1997 p 16: iterative procedure to obtain thermal conductance for gaps
            // average gap temperature is assumed to be 283 K for winter  & summer conditions
            double[] deltaTs = new double[Components.Count - 1];    // delta T for each space
            double[] hcvs = new double[Components.Count - 1]; // convective transfer coefficents
            double[] hcv_n = new double[Components.Count - 1]; // new calculated values for iteration
                                                               // initialize with 15° delta T total for unit
            for (int i = 0; i < deltaTs.Length; i++)
            {
                deltaTs[i] = 15.0 / (Components.Count - 1);   // initialize with 15° delta
            }

            double hSum = 0;
            // initialize the starting values
            double deltaTT = 0;
            for (int i = 0; i < hcvs.Length; i++)
            {
                // start with inside temperature, which is high
                hcvs[i] = hcv_ISO(TCalcStandard.Tin_SHG - deltaTT - deltaTs[i], TCalcStandard.Tin_SHG - deltaTT,
                                  gaps[i], gas[i]);
                // add the radiative heat transfer part
                hcvs[i] += hr_int(283, Components[i].RI_Spd.Eps_Hemi, Components[i + 1].RO_Spd.Eps_Hemi);
                hSum += 1.0 / hcvs[i];      // sum up thermal resistance to compare
                deltaTT += deltaTs[i];      // increment the total gap temperature
            }
            double iterationlimit = 0.001;          // criteria for end of iteration
            double deltah = 0;
            int loopcount = 0;
            int maxiterations = 10;             // max iteration to avoid endless loop
            do
            {
                double suh = 0;
                deltaTT = 0;
                // get new delta Ts
                for (int j = 0; j < deltaTs.Length; j++)
                {
                    deltaTs[j] = (15.0 * 1.0 / hcvs[j]) / hSum;
                }
                // caluclate the new hcv's based on the new temperatures
                for (int i = 0; i < hcvs.Length; i++)
                {
                    hcvs[i] = hcv_ISO(TCalcStandard.Tin_SHG - deltaTT - deltaTs[i], TCalcStandard.Tin_SHG - deltaTT,
                                      gaps[i], gas[i]);
                    // add the radiative heat transfer part
                    hcvs[i] += hr_int(283, Components[i].RI_Spd.Eps_Hemi, Components[i + 1].RO_Spd.Eps_Hemi);
                    suh += 1.0 / hcvs[i];       // get the new sum
                    deltaTT += deltaTs[i];
                }
                deltah = Math.Abs(suh - hSum);
                hSum = suh;     // save the new sum of thermal resistance values
            } while (loopcount < maxiterations && deltah > iterationlimit);
            return hcvs;
        }

        /// <summary>
        /// calculate solar absorptance values for all sheets in the triple glass assembly
        /// </summary>
        /// <returns>array for solar abs: inside to out</returns>
        protected override double[] SolarAbsorptance()
        {
            //based on ISO 9050:2003 p13
            string solfile = TCalcStandard.SolarFile;   // get the solar energy data table
            if ((SolarRadiation == null) || (SolarRadiation.AName != solfile))  // load the correct table if not there
            {
                SolarRadiation = new ReferenceTable(solfile);
            }
            // calculate solar absorption for inner / center and outer sheet in IGU configuration
            MakeSpectraGeneric();
            double ae_out = 0;
            double ae_center = 0;
            double ae_in = 0;
            double Sl = 0;
            for (int w = 0; w < SolarRadiation.Elements; w++)
            {
                double wl = SolarRadiation[w]; // get the wavelength
                double sol = SolarRadiation.GetYval(wl);    // get the solar radiation
                Sl += sol;
                double a1 = 1.0 - Components[2].TR_Spd.GetYval(wl) - Components[2].RO_Spd.GetYval(wl);
                double a2 = 1.0 - Components[1].TR_Spd.GetYval(wl) - Components[1].RO_Spd.GetYval(wl);
                double a3 = 1.0 - Components[0].TR_Spd.GetYval(wl) - Components[0].RO_Spd.GetYval(wl);
                double a1_ = 1.0 - Components[2].TR_Spd.GetYval(wl) - Components[2].RI_Spd.GetYval(wl);
                double a2_ = 1.0 - Components[1].TR_Spd.GetYval(wl) - Components[1].RI_Spd.GetYval(wl);
                double ann = (1.0 - Components[2].RI_Spd.GetYval(wl) * Components[1].RO_Spd.GetYval(wl)) *
                    (1.0 - Components[1].RI_Spd.GetYval(wl) * Components[0].RO_Spd.GetYval(wl)) -
                    Math.Pow(Components[1].TR_Spd.GetYval(wl), 2.0) * Components[2].RI_Spd.GetYval(wl) *
                    Components[0].RO_Spd.GetYval(wl);
                ae_out += sol * (((Components[2].TR_Spd.GetYval(wl) * a1_ * Components[1].RO_Spd.GetYval(wl) *
                                   (1.0 - Components[1].RI_Spd.GetYval(wl) * Components[0].RO_Spd.GetYval(wl)) +
                                   Components[2].TR_Spd.GetYval(wl) * Math.Pow(Components[1].TR_Spd.GetYval(wl), 2) *
                                   a1_ * Components[0].RO_Spd.GetYval(wl)) / ann) + a1);
                ae_center += sol * (Components[2].TR_Spd.GetYval(wl) * a2 *
                                    (1.0 - Components[1].RI_Spd.GetYval(wl) * Components[0].RO_Spd.GetYval(wl)) +
                                    Components[2].TR_Spd.GetYval(wl) * Components[1].TR_Spd.GetYval(wl) * a2_ *
                                    Components[0].RO_Spd.GetYval(wl)) / ann;
                ae_in += sol * (Components[2].TR_Spd.GetYval(wl) * Components[1].TR_Spd.GetYval(wl) * a3) / ann;

            }
            ae_out /= Sl;
            ae_in /= Sl;
            ae_center /= Sl;
            double[] sab = new double[Components.Count];
            sab[0] = ae_in; sab[1] = ae_center; sab[2] = ae_out;
            return sab;
        }

        /// <summary>
        /// returns the g-value according to ISO standard
        /// </summary>
        /// <param name="U_summer">summer conditions</param>
        /// <returns>g-value or SHGC</returns>
        public override double SolarGain(out double U_summer)
        {
            //absSolar = SolarAbsorptance();	// recalc as standard may have changed
            double gv = SolarCharacteristic(Tran);
            if (!TCalcStandard.ISOcalc)
            {
                double US = U_NFRC_ISO(true, out gv, TCalcStandard.SolarE);  // get the SHGC calculated according to ASHRAE
                U_summer = US;
            }
            else
            {
                gv += qi();
                U_summer = U_Value_Winter;
            }
            return gv;
        }



        /// <summary>
        /// returns a string indicating the evaluation conditions
        /// </summary>
        /// <returns>thermal standard</returns>
        public override string EvalConditions()
        {
            string etxt = "IGU using " + Components[1].UnitThickness.ToString("F1") + " mm " + Components[1].UnitName + " | ";
            etxt += this.AirGap_mm(0).ToString("F0") + " mm gap " + this.GasFilling(0).ToString() + " | ";
            etxt += Components[0].UnitThickness.ToString("F1") + "mm " + Components[0].UnitName + " ¦ " + TCalcStandard.CurrentStandard.ToString();
            return etxt;
        }

        /// <summary>
        /// reports color scale in string form
        /// </summary>
        /// <returns>CIE color standard</returns>
        public override string ColorScale()
        {
            string clrhdr = "IGU Color: ";
            clrhdr += this.CIE_Illuminant.ToString() + "/";
            if (this.CIE_Observer == CIEObservers.CIE_1931)
            {
                clrhdr += "2°";
            }
            else
            {
                clrhdr += "10°";
            }

            return clrhdr;
        }

    }

    #endregion
    #region Support classes to display color and thermal properties
    /// <summary>
    /// class to decipher and hold a set of thermal performance data for display in an ObjectListview
    /// </summary>
    public class ThermalData
    {
        public static bool UseIPunits { get; set; } = true; //< report imperial units for NFRC type U-Value

        /// <summary>
        /// returns the unit string for the U-value for display purposes
        /// </summary>
        public static string U_Units
        {
            get
            {
                if (TCalcStandard.Units == ThermalUnits.SI || !UseIPunits)
                {
                    return "W /(m\u00B2 K)";
                }
                else
                {
                    return "BTU/(ft\u00B2h F)";
                }
            }
        }

        /// <summary>
        /// build a class structure for thermal data display in a ObjectListView
        /// </summary>
        /// <param name="gps">IGU unit</param>
        /// <param name="lbl">title of unit</param>
        /// <param name="reverse">reversed unit parameters</param>
        public ThermalData(GlassUnit gps, string lbl, bool reverse)
        {   // assign the data for glass unit
            // need to switch configuration for NFRC calculation in case of multiple glazings: calculates out -> in
            this.Label = lbl;                                                       //
            if (reverse)
            {
                gps.ReverseUnit();
            }
            var isIP = TCalcStandard.Units == ThermalUnits.IP && UseIPunits;
            GVAL = gps.SolarGain(out double USum);
            USUMMER = !isIP ? USum : USum * TCalcStandard.MetricBTU;
            UVAL = !isIP ? gps.U_Value_Winter : gps.U_Value_Winter * TCalcStandard.MetricBTU;
            TUV = gps.T_UV;
            RUV = gps.R_UV_OUT;
            TVIS = gps.tauVIS;
            RVIS = gps.rho_o;   // visible reflectance outside
            RSOL = gps.rho_Solar();
            TSOL = gps.tau_Solar();
            ASOL = 1.0 - TSOL - RSOL;
            SMEL = gps.TR_Spd.MelanopicEffect(); 
            //USUMMER = gps.U_Value_Summer;
            if (reverse)
            {
                gps.ReverseUnit();    // flip it back to original
            }

            hlbl[6] = gps.SolarGainType;    // get the type: g-value or SHGC
        }

        static string[] hlbl = { "TVis", "Rvis", "TSol", "Rsol", "ASol", "TUV", "g-val", "SC", "LSG", "UVal", "Usum", "MT"};
        /// <summary>
        /// Title of current thermal data set
        /// </summary>
        public string Label { get; set; }
        // create properties
        /// <summary>
        /// UV transmission
        /// </summary>
        public double TUV { get; set; }
        /// <summary>
        /// UV reflection
        /// </summary>
        public double RUV { get; set; }
        /// <summary>
        /// visible transmission
        /// </summary>
        public double TVIS { get; set; }
        /// <summary>
        /// visible reflection
        /// </summary>
        public double RVIS { get; set; }
        /// <summary>
        /// solar transmission
        /// </summary>
        public double TSOL { get; set; }
        /// <summary>
        /// solar reflection
        /// </summary>
        public double RSOL { get; set; }
        /// <summary>
        /// solar absorption
        /// </summary>
        public double ASOL { get; set; }
        /// <summary>
        /// g-value
        /// </summary>
        public double GVAL { get; set; }
        /// <summary>
        /// winter U-value
        /// </summary>
        public double UVAL { get; set; }
        /// <summary>
        /// summer U-value
        /// </summary>
        public double USUMMER { get; set; }
        public double SMEL { get; set; }
        /// <summary>
        /// light to solar gain LSG
        /// </summary>
        public double LSG
        {
            get
            {
                if (GVAL > 0)
                {
                    return TVIS / GVAL;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// shading coefficient
        /// </summary>
        public double SC { get { return GVAL * 1.15; } }
        /// <summary>
        /// units for U-value: metric - BTU
        /// </summary>
        //public string U_Units { get; set; }

        /// <summary>
        /// return thermal property values in order
        /// </summary>
        /// <param name="i">index of parameter</param>
        /// <returns></returns>
        public double this[int i]
        {
            get
            {
                double v = 0;
                switch (i)
                {
                    case 0: v = TVIS; break;
                    case 1: v = RVIS; break;
                    case 2: v = TSOL; break;
                    case 3: v = RSOL; break;
                    case 4: v = ASOL; break;
                    case 5: v = TUV; break;
                    case 6: v = GVAL; break;
                    case 7: v = SC; break;
                    case 8: v = LSG; break;
                    case 9: v = UVAL; break;
                    case 10: v = USUMMER; break;
                    case 11: v = SMEL; break;
                    default: v = 0; break;

                }
                return v;
            }
        }

        /// <summary>
        /// header title
        /// </summary>
        /// <param name="i">position in sequence</param>
        /// <returns>title</returns>
        public static string HdrLabel(int i)
        {
            if (i < hlbl.Length)
            {
                return hlbl[i];
            }
            else
            {
                return "NA";
            }
        }

    }

    #endregion
}
