﻿/// <summary>
/// Utility module for OptiTherm library
/// 
/// </summary>
using MathNet.Numerics.Interpolation;
using MathNet.Numerics.Statistics;
using System;

namespace OptiTherm
{
    public static class OptiUtils
    {
        static readonly string ReferencesPath = "ColorStandards"; //!< directory to read color standard tables from
        /// <summary>
        /// OptiTherm error messages transfers
        /// </summary>
        public static event EventHandler<string>? OptiThermMessage;

        private static IInterpolation PhotopicSpline { get; set; }

        public static void OptiThermMessageSend(string msg)

        {
            OptiThermMessage?.Invoke(sender: "OptiTherm", msg);
        }

        /// <summary>
        /// get the Folder holding the reference standards for Color and thermal calculations
        /// </summary>
        internal static string References
        {
            get
            {
                var app = AppDomain.CurrentDomain.BaseDirectory;
                return System.IO.Path.Combine(app, ReferencesPath);
            }
        }

        /// <summary>
        /// prepare a weighted tolerance band over the given spectrum 
        /// using the Photopic function to reduce fitting tolernace over the visible/color region
        /// a factor of 1 is reducing the tolerance from 1.0 to 0.5 at peak sensitivity
        /// </summary>
        /// <param name="wvl">the wavelength range to match</param>
        /// <param name="enhanceBy">the enhancement factor</param>
        /// <returns></returns>
        public static double[] PhotopicWeight(double[] wvl, double enhanceBy, bool enhanceUV = true)
        {
            if(PhotopicSpline is null)
            {
                var tbl = new ReferenceTable("PhotopicFunction");
                var wvlUV = tbl.wvl;
                var IntUV = tbl.Int;
                var wstep = tbl.wvl[1]-tbl.wvl[0];
                if (enhanceUV)
                {
                    while (wvlUV[0] > 350.0)
                    {
                        wvlUV.Insert(0, wvlUV[0] - wstep);
                        IntUV.Insert(0,0);
                    }
                    // add the correction factor
                    const double UVstart = 500.0; // enhancemnt start wavelength
                    const double UVlift = 0.06 / 100.0; // factor to lift the values below 500 nm
                    for (int i = 0; i < wvlUV.Count; i++)
                    {
                        var correction = wvlUV[i] < 350 || wvlUV[i] > UVstart ? 0 : (wvlUV[i] - 350) * UVlift;
                        IntUV[i] = IntUV[i] + correction;
                    }
                }
                // build a spline function for interpolation
                PhotopicSpline = MathNet.Numerics.Interpolate.Linear(wvlUV, IntUV);
                //PhotopicSpline = MathNet.Numerics.Interpolate.Linear(tbl.wvl, tbl.Int);

            }
            double[] weight = new double[wvl.Length];
            for (int i = 0; i < wvl.Length; i++)
            {
                weight[i] = 1.0 /(PhotopicSpline.Interpolate(wvl[i]) * enhanceBy + 1.0);
            }
            return weight;
        }
    }
}
