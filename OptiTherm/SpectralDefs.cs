﻿/* ====================================================================
*
* classes to handle and arrange measuremnt data and spectral data
* transfer from DICT_Colors Feb-08-2022
* split off from SA into OptiTherm with base data structures
*
* Author: Anton Dietrich
* Date:
*
* Copyright (C) 2008-2022 Anton Dietrich
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*  ======================================================================
*/

using System.Collections;
using System.Globalization;
using System.Text;
using System.Text.Json;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace OptiTherm
{
    /// <summary>
    /// base class to hold a set of spectral data files:
    /// wavelength vs intensity
    /// </summary>
    public class SpectralData : ICloneable
    {
        public string Filename { get; set; } = "NA";    //!< file name of data file to read
        // base class to hold spectral data in a wvl - intensity table
        protected bool sorted = false;      // assume always generic intensity data content
        protected bool isPercent = false;    // intensity data are in %
        //public ColorType SpectraType { get; set; } = ColorType.TR;     // set a spectral type
        protected LabYData MyColor = new();
        public bool isFilmsideR { get; set; } = false;  //!< true -> is a coated surface
        public double Emissivity { get; set; } = 0.837;    //!< default emissivity is base glass: hemisherical
        bool isSmoothed = false;        // flag to avoid double smoothing of data sets
        public double IncidenceAngle { get; set; } = 0.0;   //!< incidence angle the spectra were created or measured

        public int Elements { get { return wvl.Count; } }   //!< number of data pairs

        public string AName { get; set; } = "NA";   //!< title of spectral data set
        public ColorType TypeOfSpectra { get; set; } = ColorType.TR;    //!< type of data set T/RF/RG
        public static CIELightSources LightSource { get; set; } = CIELightSources.C;
        public static CIEObservers Observer { get; set; } = CIEObservers.CIE_1931;

        public List<double> wvl = new();    //!< list of wavelength data

        public List<double> Int = new();    //!< list of intensity data
        private const double VisLimit = 750;    // nm smoothing boundary between VIS and NIR


        /// <summary>
        /// empty constructor for serialization
        /// </summary>
        public SpectralData() { }

        /// <summary>
        /// initializes and empty data set: uses only if set is inherited
        /// for loading reference type spectra
        /// </summary>
        /// <param name="name">file name</param>
        /// <param name="SpType">Type of spectra T/R/G</param>
        /// <param name="Percent">values are scaled in percent</param>
        protected SpectralData(string name, ColorType SpType, bool Percent)
        {
            //just generate a default instance using a Name: data are not yet initialized
            AName = name;
            isPercent = Percent;
            TypeOfSpectra = SpType;       // set the type of spectra and the incidence angle
            MyColor.TRType = SpType;
        }


        /// <summary>
        /// create a spectral data set using a raw PE text file or similar as input
        /// used only for inheritance to measurement data
        /// </summary>
        /// <param name="PEfile">full file path</param>
        /// <param name="SpType">type of spectra</param>
        public SpectralData(string PEfile, ColorType SpType)
        {
            TypeOfSpectra = SpType;       // set the type of spectra
            MyColor.TRType = SpType;
            if (!File.Exists(PEfile))
            {
                AName = "NA";
                return;
            }
            string dname = Path.GetFileNameWithoutExtension(PEfile);
            string fext = Path.GetExtension(PEfile);
            // if it is a .sp file check for a binary type and read it
            if (fext == ".sp")
            {
                if (ReadPEPEfile(PEfile))
                {
                    AName = dname;
                    return;
                }
            }
            // try to read a text file
            List<double> wvln = new List<double>();
            List<double> Y = new List<double>();
            StreamReader txt = new (PEfile);
            string? dl;
            Boolean DSection = true;    // by default assume files have just a data section
                                        // try to find out if this contains a "#Data" section like true .SP files or just plain data
            while ((dl = txt.ReadLine()) != null)
            {
                if (dl.ToUpper().StartsWith("#DATA"))
                {
                    DSection = false;
                    break;
                }
            }
            // re-position the streamreader to file begin
            txt.BaseStream.Seek(0, SeekOrigin.Begin);
            txt.DiscardBufferedData(); // need to flush old data buffer
                                       // a csv file may have , as decimal point (german notation)
            var isComma = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == ",";
            var isCSV = fext.ToLower() == ".csv" && isComma;
            bool isPct = false; // flag to scan if data in percent or generic notation
            while ((dl = txt.ReadLine()) != null)
            {
                if (!DSection)
                {
                    //skip header data, just look for data section
                    if (dl.ToUpper().Contains("#DATA"))
                    {
                        DSection = true;
                    }
                }
                else        // read the data lines
                {
                    // check if the data section has a comma as decimal separator
                    Match comma = Regex.Match(dl, @"\d{1,},\d{1,}\s\d{1,},\d");
                    dl = comma.Success ? dl.Replace(',', '.') : dl;

                    //dl = isCSV ? dl.Replace(',', '.') : dl;   // change decimal point in case of German notation
                    string[] dpp;
                    // read the data
                    char[] separ = new char[] { '\t', ',', ' ', ';' };
                    dpp = dl.Split(separ, StringSplitOptions.RemoveEmptyEntries);
                    if (dl.Length > 0)
                    {
                        float intensity = 0;
                        bool isv = float.TryParse(dpp[0].Trim(), out float wvl);
                        if (isv)
                        {
                            isv = float.TryParse(dpp[1].Trim(), out intensity);
                        }
                        if (isv)
                        {
                            if (intensity < 0)
                            {
                                intensity = 0; // eliminate negative values
                            }

                            isPct |= intensity > 1.0;  // if any value > 1.0 data are likely in percent
                            if (SpType != ColorType.TR && wvl < 350)
                            {
                                ;   // skip reflection data below 350
                            }
                            else
                            {
                                wvln.Add(wvl);
                                Y.Add(intensity);
                            }
                        }
                    }
                }
            }
            if (wvln.Count > 0)
            {
                wvl = wvln;
                if (isPct)      // make data generic if they were read as percent
                {
                    for (int i = 0; i < Y.Count; i++)
                    {
                        Y[i] /= 100;
                    }
                    isPercent = false;
                }
                Int = Y;
                this.GetYvalPercent(500.0); // make sure array is sorted
                AName = dname;
            }
        }

        /// <summary>
        /// create a spectral data set in % or 0-1 Format: use a double array as input
        /// </summary>
        /// <param name="aname">Name</param>
        /// <param name="inPercent">true if data in %</param>
        public SpectralData(double[] wl, double[] intens, string aname, ColorType SpType, bool inPercent)
        {
            isPercent = inPercent; // set the flag
            wvl = new List<double>(wl); // new Single[wl.Length];
            Int = new List<double>(intens); // new Single[wl.Length];
            AName = aname;
            TypeOfSpectra = SpType;
            MyColor.TRType = SpType;
            // set default incidence angel depending on spectra type: 0 or 8 °
            //			IncidenceAngle = SpType == ColorType.TR ? 0.0 : 8.0;
        }

        /// <summary>
        /// create a spectral data set in % or 0-1 Format: use a generic list as input
        /// </summary>
        /// <param name="aname">Name</param>
        /// <param name="inPercent">true if data in %</param>
        public SpectralData(List<double> wl, List<double> intens, string aname, ColorType SpType, bool inPercent)
        {
            isPercent = inPercent; // set the flag
            wvl = wl;
            Int = intens;
            AName = aname;
            TypeOfSpectra = SpType;
            MyColor.TRType = SpType;
        }

        /// <summary>
        /// Create a deep copy clone of spectral data
        /// </summary>
        /// <returns>Copy of current SpectralData</returns>
        public object Clone()
        {
            var nwvl = new List<double>();
            var ny = new List<double>();
            for (int i = 0; i < wvl.Count; i++)
            {
                nwvl.Add(wvl[i]);
                ny.Add(Int[i]);
            }

            var cln = new SpectralData(nwvl, ny, this.AName, this.TypeOfSpectra, isPercent)
            {
                IncidenceAngle = IncidenceAngle,
                Emissivity = Emissivity,
                isFilmsideR = isFilmsideR,
                AName = AName
            };
            return cln;
        }


        /// <summary>
        /// cut the data series off
        /// </summary>
        /// <param name="lowwvl">low wavelenght cut off</param>
        /// <param name="hiwvl">high wavelength cut off</param>
        public void CutRange(double lowwvl, double hiwvl)
        {
            // check and reset to actual limits
            lowwvl = wvl[0] < lowwvl ? lowwvl : wvl[0];
            hiwvl = wvl[wvl.Count - 1] > hiwvl ? hiwvl : wvl[wvl.Count - 1];
            var lndx = wvl.FindIndex(av => av == lowwvl);
            var hndx = wvl.FindIndex(av => av == hiwvl);
            var size = hndx - lndx + 1;
            //var nwvl = new List<double>();
            //var nint = new List<double>();
            var nwvl = wvl.GetRange(lndx, size);
            var nint = Int.GetRange(lndx, size);
            //Array.Copy(wvl, lndx, nwvl, 0, size);
            //Array.Copy(Int, lndx, nint, 0, size);
            // replace the original data
            wvl = nwvl;
            Int = nint;
        }


        /// <summary>
        /// serialize the data series for saving to database
        /// uses JsonSerializer
        /// </summary>
        /// <returns>byte array serialized data</returns>
        public byte[] SerializeData()
        {
            var wvld = new List<double>();
            var intd = new List<double>();
            for (int i = 0; i < wvl.Count; i++)
            {
                wvld.Add(wvl[i]);
                intd.Add(Int[i]);
            }
            var nsp = new SpectralData(wvld, intd, this.AName, this.TypeOfSpectra, isPercent);
            nsp.ConvertToGeneric();
            var soptions = new JsonSerializerOptions()
            {
                IncludeFields = true,
                IgnoreReadOnlyFields = false,
                IgnoreReadOnlyProperties = false
            };
            var spser = JsonSerializer.Serialize<SpectralData>(nsp, soptions);
            //var jsser = JsonSerializer.SerializeToUtf8Bytes(nsp, typeof (SpectralData), soptions);
            byte[] bary = Encoding.UTF8.GetBytes(spser);
            return bary;
        }

        /// <summary>
        /// de-serialize a data set read from a db blob
        /// </summary>
        /// <param name="bldata">data blob from db</param>
        /// <returns></returns>
        static public SpectralData Spdeserialize(byte[] bldata)
        {
            //var ros = new ReadOnlySpan<byte>(bldata);
            var jrd = new Utf8JsonReader(bldata);
            var soptions = new JsonSerializerOptions()
            {
                IncludeFields = true,
                IgnoreReadOnlyFields = false,
                IgnoreReadOnlyProperties = false
            };
            var spd = System.Text.Json.JsonSerializer.Deserialize<SpectralData>(ref jrd, soptions);
            return spd;
        }


        /// <summary>
        /// create a color CIE data structure based on spectra
        /// </summary>
        /// <param name="LS">light source to use</param>
        /// <param name="obs">CIE observer to use</param>
        /// <returns></returns>
        public LabYData CIEcolor(CIELightSources LS, CIEObservers obs)
        {
            MyColor.TRType = this.TypeOfSpectra;
            if (MyColor.ls != LS || MyColor.obs != obs || MyColor.TRType == ColorType.Ref)
            {
                // recalc in case there is a change or for a new spectral set
                MyColor = CIEColor.CalcCIEcolor(this, LS, obs);
            }
            return MyColor;
        }

        /// <summary>
        /// get the melanopic effect of the current spectrum
        /// </summary>
        /// <returns></returns>
        public double MelanopicEffect()
        {
            return CIEColor.MelanopticImpact(this);
        }

        /// <summary>
        /// get the intensity values for a specific wavelength
        /// </summary>
        /// <param name="wl">wavelength</param>
        /// <returns></returns>
        public double GetYval(double wl)
        {
            if (!sorted)
            {
                SortXYdata();
            }
            return LookUp(wl);
        }

        /// <summary>
        /// set a specific intensity value for a given wavelenght index
        /// </summary>
        /// <param name="wl">index of wavelenght</param>
        /// <param name="vl">intensity value</param>
        public void SetYval(int wl, double vl)
        {
            if (wl < Int.Count)
            {
                Int[wl] = vl;
            }
        }

        /// <summary>
        /// get the intensity as a Percentage value
        /// </summary>
        /// <param name="wl"></param>
        /// <returns></returns>
        public double GetYvalPercent(double wl)
        {
            if (isPercent)
            {
                return GetYval(wl);
            }
            else
            {
                return GetYval(wl) * 100.0;
            }
        }
        public double this[int i]   //!<  return wavelenght at index i
        {
            get
            {
                if (i >= 0 && i < wvl.Count)
                {
                    return wvl[i];
                }
                else
                {
                    return -1;
                }
            }

        }

        /// <summary>
        /// sort on ascending wavelenght data
        /// </summary>
        protected void SortXYdata()
        {
            var war = wvl.ToArray();
            var vary = Int.ToArray();
            Array.Sort(war, vary, 0, war.Length); // sort the array if not yet sorted
            sorted = true;
            wvl = new List<double>(war);
            Int = new List<double>(vary);
            sorted = true; // set the sorted flag

        }
        // private methods
        protected double LookUp(double wl)
        {
            // obtain the corresponding Y value for a given wvl: if needed extrapolate
            if (wl > wvl[^1])
            {
                return (double)Int[wvl.Count - 1];
            }

            if (wl < wvl[0])
            {
                return (double)Int[0];
            }
            // now we have to extrapolate:
            int pos = wvl.FindIndex(V => V >= wl);
            if (wvl[pos] == wl)
            {
                return (double)Int[pos];
            }

            double rval = (Int[pos] - Int[pos - 1]) / (wvl[pos] - wvl[pos - 1]) * (wl - wvl[pos - 1]) + Int[pos - 1];
            return rval;

        }

        /// <summary>
        /// convert intensity to percent
        /// </summary>
        public bool IntensityPercent
        {
            get { return isPercent; }
            set
            {
                if (value)
                {
                    ConvertToPercent();
                }
                else
                {
                    ConvertToGeneric();
                }
            }
        }

        /// <summary>
        /// convert intensity to generic (0..1)
        /// </summary>
        private void ConvertToGeneric()
        {
            if (isPercent && Int != null)  // data are in Percentage format
            {
                for (int i = 0; i < Int.Count; i++)
                {
                    Int[i] /= 100.0F; // make it generic
                }
                isPercent = false;
            }
        }

        private void ConvertToPercent()
        {
            if (!isPercent)  // data are in Percentage format
            {
                for (int i = 0; i < Int.Count; i++)
                {
                    Int[i] *= 100.0; // make it generic
                }
                isPercent = true;
            }
        }

        /// <summary>
        /// indicates this is a film side reflectance:
        /// </summary>
        public bool IsRFilm
        {
            get { return isFilmsideR; }
            set
            {
                isFilmsideR = value;
                if (Emissivity > 0.80)  // estimate only if not yet set to value below raw glass
                {
                    if (value && (wvl[wvl.Count - 1] >= 2400))
                    {
                        Emissivity = eps_c();       // estimate emissivity based on NIR reflectance
                    }
                }
            }
        }

        /// <summary>
        /// get / set hemisherical emissivity for reflectamce spectrum
        /// </summary>
        public double Eps_Hemi
        {
            get { return Emissivity; }
            set { Emissivity = value; }
        }

        /// <summary>
		/// convert emissivity from normal to hemisherical and reverse
		/// based on EN673:1997 A.2
        /// updated to EN 12898 draft 10 - 2011 - using formula
		/// </summary>
		/// <param name="epsn">epsilon  value</param>
        /// <param name="reverse">true: convert back to normal</param>
		public static double EpsConversionEN(double epsn, bool reverse)
        {

            if (reverse)
            {
                return 0.8139 * epsn + 0.478 * epsn * epsn - 0.2139 * epsn * epsn * epsn;
            }
            else
            {
                return 1.1887 * epsn - 0.4967 * epsn * epsn + 0.2452 * epsn * epsn * epsn;
            }
        }


        /// <summary>
        /// convert normal to hemisherical emissivity
        /// EN references
        /// </summary>
        /// <param name="epsn">normal emissivity</param>
        /// <returns>EN hemisherical emissivity</returns>
        public static double EpsH_EN(double epsn)
        {
            return EpsConversionEN(epsn, false);
        }

        /// <summary>
        /// convert hemisherical to normal emissivity
        /// EN references
        /// </summary>
        /// <param name="epsh"></param>
        /// <returns></returns>
        public static double EpsN_EN(double epsh)
        {
            return EpsConversionEN(epsh, true);
        }

        /// <summary>
        /// calculate hemisherical emissivity according to LBNL formulas
        /// </summary>
        /// <param name="epsn">measured normal emissivity</param>
        /// <returns>NFRC hemispherical emissivity</returns>
        public static double EpsH_LBNL(double epsn)
        {
            var epsH = 0.0;
            if (epsn > 0.8)
            {
                epsH = 0.1569 * epsn + 3.7669 * Math.Pow(epsn, 2) - 5.4398 * Math.Pow(epsn, 3) + 2.4733 * Math.Pow(epsn, 4);
            }
            else
            {
                epsH = 1.3217 * epsn - 1.8766 * Math.Pow(epsn, 2) + 4.6586 * Math.Pow(epsn, 3)
                - 5.8349 * Math.Pow(epsn, 4) + 2.7406 * Math.Pow(epsn, 5);
            }
            return epsH;
        }

        /// <summary>
        /// calculate normal emissivity based on reversed LBNL formulas
        /// </summary>
        /// <param name="epsh">NFRC hemisherical emissivity</param>
        /// <returns>normal emissivity NFRC</returns>
        public static double EpsN_LBNL(double epsh)
        {
            double en = 0.0;
            if (epsh < 0.6)
            {
                //=-0.000276 +0.75328*B3 + 1.04493*B3^2-1.55612*B3^3  +1.008956*B3^4
                en = -0.000276 + 0.75328 * epsh + 1.04493 * epsh * epsh - 1.55612 * Math.Pow(epsh, 3) + 1.008956 * Math.Pow(epsh, 4);
            }
            else
            {
                en = 0.02203 + 1.645023 * epsh - 3.365435 * Math.Pow(epsh, 2) + 5.26458 * Math.Pow(epsh, 3) - 2.53412 * Math.Pow(epsh, 4);
            }
            return en;
        }


        /// <summary>
        /// obtain the extrapolated correction factor for Eps-Normal to Eps-Hemi
        /// based on EN673:1997 A.2 or convert revers
        /// </summary>
        /// <param name="epsn">epsilon normal</param>
        /// <param name="reverse">revert from normal to hemi</param>
        private static double EpsNcorrection(double epsn, bool reverse)
        {
            // use a polynomial approximation to the tabble to convert to and from normal /hemi
            epsn = epsn > 1.0 ? epsn / 100 : epsn;
            var corr = 0.85; // default start value
            if (reverse)
            {
                // =A22*0.805578 + 0.499308*A22^2 - 0.2290093*A22^3 + 0.00075
                corr = epsn * 0.805578 + 0.499308 * epsn * epsn - 0.2290093 * Math.Pow(epsn, 3) + 0.00075;
            }
            else
            {
                // 1.1887*A19-0.496736*A19^2+ 0.245172*A19^3
                corr = 1.1887 * epsn - 0.496736 * epsn * epsn + 0.245172 * Math.Pow(epsn, 3);
            }
            return corr;
        }


        /// <summary>
        /// estimates corrected emissivity based on NIR reflectance value
        /// conversion epsn to eps hemispherical based on ERN673 table approximated by 4th order polynomial
        /// </summary>
        /// <returns>rough estimation of emissivity</returns>
        public double eps_c()
        {
            // average over 2200 to 2400 nm -> convert from %
            double rNIR = (GetYvalPercent(2300.0) + GetYvalPercent(2400.0) + GetYvalPercent(2200.0)) / 300.0;
            if (IsRFilm)
            {
                double en = 1.0 - rNIR;
                //				double sf = 1.438 * Math.Pow(en, 4.0) - 3.0282 * Math.Pow(en, 3.0) + 2.4168 * Math.Pow(en, 2.0) - 1.0988 * en + 1.2386;
                double sf = EpsNcorrection(en, false);       // correct for hemispherical
                return Math.Round(sf - 0.004, 3, MidpointRounding.ToEven);
            }
            else
            {
                return 0.89; // return std value for uncoated glass
            }
        }

        /// <summary>
        /// save current spectral data array to a aspect file format using the default name
        /// if fpath is not valid it will call a save file dialog
        /// </summary>
        /// <param name="dirname">save directory path</param>
        /// <returns>success</returns>
        public bool SaveToAspectFile(string dirname, SpectralData sdata)
        {
            string fname = sdata.AName + ".dat";
            if (!Directory.Exists(dirname))       // don't know where to save the file so ask user
            {
                return false;  // no valid file name given
            }
            else
            {
                fname = Path.Combine(dirname, fname);      // add the path to the file name
            }

            if (File.Exists(fname))
            {
                File.Delete(fname);      // delete a file with the same name: overwrite
            }

            FileStream dStream = new FileStream(fname, FileMode.OpenOrCreate, FileAccess.Write);
            BinaryWriter bwrt = new BinaryWriter(dStream, System.Text.Encoding.ASCII);
            if (!IntensityPercent)
            {
                IntensityPercent = true;
            }

            string outpt = "[GENERAL]\r\n";
            bwrt.Write((char[])outpt.ToCharArray());
            outpt = "TITLE=" + Path.GetFileName(fname) + "\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "TYP=4\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "TIME=" + DateTime.Now.Date.ToString("MM/dd/yyyy") + " / " + DateTime.Now.ToString("t") + "\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "XUNITS=nm\r\n" + "YUNITS=%T\r\n" + "ZUNITS=\r\n" + "XSTARTTIME=\r\n" + "YSTARTTIME=\r\n" +
                "ZSTARTTIME=\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "FIRSTX=" + sdata.wvl[0].ToString("F2") + "\r\n" + "LASTX=" + sdata.wvl[wvl.Count - 1].ToString("F2") + "\r\n" +
                "NPOINTS=" + sdata.wvl.Count.ToString("0") + "\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "NCYCL=1\r\n" + "[MESS]\r\n" + "ORIGIN=OPTOPLEX II C File\r\n" + "MESS_INT=0.0\r\n" +
                "MESS_AKK=0\r\n" + "MODULE_NR=0\r\n" + "MODULE2_NR=0\r\n";
            bwrt.Write(outpt.ToCharArray());
            outpt = "[USER]\r\n" + "NOTE=\r\n" + "[EXTERN]\r\n" + "TYPE=Manual File Uncorrected (RAW SPEC)\r\n" +
                "[HISTORY]\r\n" + "[DATA]\r\n" + "XDATA=\r\n";
            bwrt.Write(outpt.ToCharArray());
            for (int i = 0; i < sdata.wvl.Count; i++)
            {
                bwrt.Write(wvl[i]);
            }

            outpt = "\r\nYDATA=\r\n";
            bwrt.Write(outpt.ToCharArray());
            for (int i = 0; i < sdata.Int.Count; i++)
            {
                bwrt.Write(Int[i]);
            }
            //bwrt.Write('\n');
            bwrt.Flush();
            bwrt.Close();
            dStream.Close();
            return true;
        }


        /// <summary>
        /// saves the current spectral data in a WVASE compatible format to the directory given
        /// </summary>
        /// <param name="fpath">directory to save into (default will be user My Documents)</param>
        /// <param name="wmin">min wavelength nm </param>
        /// <param name="wmax">max wavelength nm </param>
        /// <param name="wstep">wavelength step in nm </param>
        /// <returns>the full file path of the output file</returns>
        public string SaveToWvase(string fpath, double wmin, double wmax, double wstep)
        {
            string fname = AName + ".dat";
            // don't know where to save the file so use the user directory
            if (!Directory.Exists(fpath))
            {
                fpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            fname = fpath + Path.DirectorySeparatorChar + fname;      // add the path to the file name
            if (File.Exists(fname))
            {
                File.Delete(fname);      // delete a file with the same name: overwrite
            }

            bool pctstyle = IntensityPercent; // remember initial format
                                              // use 0-1 scaling for output
            if (IntensityPercent)
            {
                IntensityPercent = false;
            }

            FileStream dStream = new FileStream(fname, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter wrt = new StreamWriter(dStream, System.Text.Encoding.ASCII);
            string output = TypeOfSpectra == ColorType.TR ? "Transmission" : "Reflection";
            output += " of " + this.AName + " for Ellipsometer";
            wrt.WriteLine(output);
            wrt.WriteLine("nm");
            string tid = TypeOfSpectra == ColorType.TR ? "uT" : "uRb";
            for (double wl = wmin; wl <= wmax; wl += wstep)
            {
                output = tid + "\t" + wl.ToString("F1") + "\t" + this.IncidenceAngle.ToString("F0") + "\t" +
                    this.GetYval(wl).ToString("F5");
                wrt.WriteLine(output);
            }
            IntensityPercent = pctstyle; // set flag back to entry situation
            wrt.Flush();
            wrt.Close();
            dStream.Close();
            return fname;
        }


        /*// <summary>
        /// use a RBF multilayer approximation to fit and smooth data set
        /// </summary>
        /// <param name="layers">nbr of layers to use </param>
        /// <param name="step">initial wvl step </param>
        /// <returns> status of RBF approximation</returns>
        public int RBF_ML_Smooth(int layers, double step)
        {
            if (isSmoothed)
            {
                return 0;   // already applied
            }

            double reference = 550; // center reference for the RBF
                                    // create the base model
            alglib.rbfcreate(2, 1, out alglib.rbfmodel amodel);
            // create the raw data array: split data in VIS /NIR in case it is a full scan > 1000 nm
            int sndx = wvl.Count;  // default
            if (wvl[sndx - 1] > 1000)
            {
                // find the index for 800 nm
                sndx = wvl.FindIndex(val => val > 800);
            }
            double[,] xy0 = new double[sndx, 3];
            for (int i = 0; i < sndx; i++)
            {
                xy0[i, 0] = wvl[i];
                xy0[i, 1] = reference;
                xy0[i, 2] = Int[i];
            }
            alglib.rbfsetpoints(amodel, xy0);
            // solve it using original settings
            alglib.rbfsetalgomultilayer(amodel, step, layers, 0.01);
            alglib.rbfbuildmodel(amodel, out alglib.rbfreport report);
            for (int i = 0; i < sndx; i++)
            {
                Int[i] = (float)alglib.rbfcalc2(amodel, wvl[i], reference);
            }
            if (sndx < wvl.Count)
            {
                // now match the NIR section
                int nirnbr = wvl.Count - sndx + 5; // use 5 points overlap
                reference = 1500;   // use 1500 nm as center
                xy0 = new double[nirnbr, 3];
                for (int i = 0; i < nirnbr; i++)
                {
                    xy0[i, 0] = wvl[i + sndx - 5];
                    xy0[i, 1] = reference;
                    xy0[i, 2] = Int[i + sndx - 5];
                }
                alglib.rbfsetpoints(amodel, xy0);
                // solve it using 3 layers less and new center
                layers = layers - 3 > 0 ? layers - 3 : 1;
                alglib.rbfsetalgomultilayer(amodel, step, layers, 0.01);
                alglib.rbfbuildmodel(amodel, out report);
                for (int i = sndx; i < wvl.Count; i++)
                {
                    Int[i] = (float)alglib.rbfcalc2(amodel, wvl[i], reference);
                }
            }
            isSmoothed = true;  // flag to avoid double smoothing
            return report.terminationtype;
        }

        */

        /// <summary>
        /// use a Savitzky-Golay filter to smooth the measurement data
        /// </summary>
        /// <param name="points">number of side points to use</param>
        /// <param name="order">polynomial order</param>
        public void SG_Smooth(int points, int order)
        {
            if (isSmoothed) return;
            var boundary = wvl.FindIndex(w => w > VisLimit);
            var visPart = Int.Skip(0).Take(boundary).ToArray();
            var nirPart = Int.Skip(boundary).ToArray();
            var sgfilter = new SavitzkyGolayFilter(points, order);
            // filter only the NIR part
            var sgVis = sgfilter.Process(visPart);
            var sgNir = sgfilter.Process(nirPart);
            // patch the data together
            // adjust the joint section between VIS and NIR
            var delta = (sgVis[^1] - sgNir[0]) * 0.25;
            sgVis[^1] -= delta;
            sgNir[0] += delta;
            Int = sgVis.Concat(sgNir).ToList();
            isSmoothed = true; // set the flag to avoid double runs
        }

        /// <summary>
        /// calculate a NIR weight factor based on a gaussian response observer
        /// centered around 700 nm
        /// </summary>
        /// <returns></returns>
        public double NIRSW()
        {
            // prepare a gaussion observer centered at 720 nm
            const double sigma = 75 * 75;   // sigma^2 for gaussian
            const double cntrp = 720;   // center wavelength for gaussian distribution
            double fact = 1.0 / Math.Sqrt(Math.PI * sigma);
            //			fact *= Math.Exp(-1.0*Math.Pow((wavel - cntrp),2)/(2* sigma));
            // integrate over the spctrum in 5 nm steps from 500 to 900 nm using a gaussian observer
            double normalize = isPercent ? 100.0 : 1.0;
            double intss = 0;
            double ref100 = 0;
            for (double i = 500.0; i < 905; i += 5)
            {
                double sf = 5 * fact * Math.Exp(-1.0 * Math.Pow((i - cntrp), 2) / (2 * sigma));
                intss += GetYval(i) / normalize * sf;
                ref100 += sf;
            }
            intss /= ref100;  // scale it to 1
            return intss;
        }


        /// <summary>
        /// read data from a binary PerkinElmer export file *.sp
        /// </summary>
        /// <param name="fnm">file name</param>
        /// <returns>if success reading spectral data</returns>
        bool ReadPEPEfile(string fnm)
        {
            /*
             * adapted from Matlab version:
            Reads in spectra from PerkinElmer block structured files.
            This version supports 'Spectrum' SP files.
            Note that earlier 'Data Manager' formats are not supported.
            data:  1D array of doubles
            xAxis: vector for abscissa(e.g.Wavenumbers).
            misc: miscellanous information in name, value pairs
            Copyright(C)2007 PerkinElmer Life and Analytical Sciences
            Stephen Westlake, Seer Green

            History
            2007 - 04 - 24 SW     Initial MatLabversion
            2018 - 05 - 26 AD C# implementation */
            // Block IDs
            const Int16 DSet2DC1DIBlock = 120;
            //Int16 HistoryRecordBlock = 121;
            //Int16 InstrHdrHistoryRecordBlock = 122;
            //Int16 InstrumentHeaderBlock = 123;
            //Int16 IRInstrumentHeaderBlock = 124;
            //Int16 UVInstrumentHeaderBlock = 125;
            //Int16 FLInstrumentHeaderBlock = 126;
            // Data member IDs
            //int DataSetDataTypeMember = -29839;
            const Int16 DataSetAbscissaRangeMember = -29838;
            //const Int16 DataSetOrdinateRangeMember = -29837;
            const Int16 DataSetIntervalMember = -29836;
            const Int16 DataSetNumPointsMember = -29835;
            //const Int16  DataSetSamplingMethodMember = -29834;
            const Int16 DataSetXAxisLabelMember = -29833;
            const Int16 DataSetYAxisLabelMember = -29832;
            //const Int16  DataSetXAxisUnitTypeMember = -29831;
            //const Int16  DataSetYAxisUnitTypeMember = -29830;
            //const Int16  DataSetFileTypeMember = -29829;
            const Int16 DataSetDataMember = -29828;
            const Int16 DataSetNameMember = -29827;
            //const Int16  DataSetChecksumMember = -29826;
            //const Int16  DataSetHistoryRecordMember = -29825;
            //const Int16  DataSetInvalidRegionMember = -29824;
            const Int16 DataSetAliasMember = -29823;
            //const Int16  DataSetVXIRAccyHdrMember = -29822;
            //const Int16  DataSetVXIRQualHdrMember = -29821;
            //const Int16  DataSetEventMarkersMember = -29820;
            /*
            Type code IDs
            int ShortType = 29999;
            int UShortType = 29998;
            int IntType = 29997;
            int UIntType = 29996;
            int LongType = 29995;
            int BoolType = 29988;
            int CharType = 29987;
            int CvCoOrdPointType = 29986;
            int StdFontType = 29985;
            int CvCoOrdDimensionType = 29984;
            int CvCoOrdRectangleType = 29983;
            int RGBColorType = 29982;
            int CvCoOrdRangeType = 29981;
            int DoubleType = 29980;
            int CvCoOrdType = 29979;
            int ULongType = 29978;
            int PeakType = 29977;
            int CoOrdType = 29976;
            int RangeType = 29975;
            int CvCoOrdArrayType = 29974;
            int EnumType = 29973;
            int LogFontType = 29972;
            */
            // local vars
            var YdataList = new List<double>();
            var XdataList = new List<double>();
            if (!File.Exists(fnm))
            {
                return false;
            }
            string description, xLabel, yLabel, alias, originalName;
            int xLen = 0;
            int innerCode = 0;
            double xDelta = 0;
            double x0 = 0;
            double xEnd = 0;
            using (var rdr = new BinaryReader(File.Open(fnm, FileMode.Open)))
            {
                // check the file header if this is a PEPE type file
                var signature = new string(rdr.ReadChars(4));
                if (!signature.Contains("PEPE"))
                {
                    return false;
                }
                description = new string(rdr.ReadChars(40));
                // the rest of the file is a list of blocks
                while (rdr.BaseStream.Position < rdr.BaseStream.Length)
                {
                    var blockID = rdr.ReadInt16();
                    var blockzize = rdr.ReadInt32();
                    if (rdr.BaseStream.Position >= rdr.BaseStream.Length)
                    {
                        break;
                    }
                    switch (blockID)
                    {
                        case DSet2DC1DIBlock:
                            ;// Wrapper block.Read nothing.
                            break;
                        case DataSetAbscissaRangeMember:
                            innerCode = rdr.ReadInt16();
                            // _ASSERTE(CvCoOrdRangeType == nInnerCode);
                            x0 = rdr.ReadDouble();
                            xEnd = rdr.ReadDouble();
                            break;
                        case DataSetIntervalMember:
                            innerCode = rdr.ReadInt16();
                            xDelta = rdr.ReadDouble();
                            break;
                        case DataSetNumPointsMember:
                            innerCode = rdr.ReadInt16();
                            xLen = rdr.ReadInt32();
                            break;
                        case DataSetXAxisLabelMember:
                            innerCode = rdr.ReadInt16();
                            var Len = rdr.ReadInt16();
                            xLabel = new string(rdr.ReadChars(Len));
                            break;
                        case DataSetYAxisLabelMember:
                            innerCode = rdr.ReadInt16();
                            var len = rdr.ReadInt16();
                            yLabel = new string(rdr.ReadChars(len));
                            break;
                        case DataSetAliasMember:
                            innerCode = rdr.ReadInt16();
                            len = rdr.ReadInt16();
                            alias = new string(rdr.ReadChars(len));
                            break;
                        case DataSetNameMember:
                            innerCode = rdr.ReadInt16();
                            len = rdr.ReadInt16();
                            originalName = new string(rdr.ReadChars(len));
                            break;
                        case DataSetDataMember:
                            innerCode = rdr.ReadInt16();
                            var dlen = rdr.ReadInt32();
                            // innerCode should be CvCoOrdArrayType
                            // len should be xLen * 8
                            xLen = xLen == 0 ? dlen / 8 : xLen;
                            for (int i = 0; i < xLen; i++)
                            {
                                var dp = rdr.ReadDouble();
                                YdataList.Add((float)dp);
                                XdataList.Add((float)x0);
                                x0 += xDelta; ;
                            }
                            break;
                        default:
                            //  unknown block, just seek past it
                            rdr.BaseStream.Seek(blockzize, SeekOrigin.Current);
                            break;
                    }
                }
            }
            // sort the data
            var wary = XdataList.ToArray();
            var yary = YdataList.ToArray();
            Array.Sort(wary, yary, 0, XdataList.Count); // sort the array
            wvl = new List<double>(wary);
            Int = new List<double>(yary);
            sorted = true;
            return wvl.Count > 0 ? true : false;
        }

    }


    /// <summary>
    /// reads and hold a reference table of X vs Y (Z) data from a text data file
    /// based on SpectralData base class
    /// </summary>
    public class ReferenceTable : SpectralData
    {
        struct DataPair { public float xv; public float yv; };

        /// <summary>
        /// read the Table data from Files Tbl and populate the x/y arrays with them
        /// </summary>
        /// <param name="Tbl">Filename of Table</param>
        public ReferenceTable(string Tbl)
            : base(Tbl, ColorType.Ref, false)
        {

            string tbpath = OptiUtils.References;
            ReadTableData(Tbl, tbpath, 1);  // try to read the data from a reference file on the disk
        }

        /// <summary>
        /// read data from a multi column tab limited txt file
        /// </summary>
        /// <param name="Tbl">file name of txt file</param>
        /// <param name="colToread">column to read Y data from</param>
        public ReferenceTable(string Tbl, int colToread) : base(Tbl, ColorType.Ref, false)
        {
            var tbpath = Path.GetDirectoryName(Tbl);
            if (string.IsNullOrEmpty(tbpath))
            {
                tbpath = OptiUtils.References;
            }
            else
            {
                Tbl = Path.GetFileName(Tbl);
            }
            //			string tbpath = NewFSTst.Properties.Settings.Default.CalReferences;
            ReadTableData(Tbl, tbpath, colToread);  // try to read the data from a reference file on the disk
        }

        /// <summary>
        /// read a tab limited reference x,y table from a ascii file
        /// </summary>
        /// <param name="fname">filename with extension</param>
        /// <param name="fpath">file path</param>
        public ReferenceTable(string fname, string fpath)
            : base(fname, ColorType.Ref, false)
        {
            ReadTableData(fname, fpath, 1);        // just read the table from a file
        }


        public ReferenceTable(string Tbl, double[] wl, double[] val)
        {
            if (wl.Length > val.Length)
            {
                return; // data do not match
            }
            for (int i = 0; i < wl.Length; i++)
            {
                wvl.Add(wl[i]);
                Int.Add(val[i]);
            }
            sorted = false;
            AName = Tbl;  // set the name of the arrays
            isPercent = false; //generic mode in this case
        }

        /// <summary>
        /// Read TblName from Application Directory if it exists and populate the array(s)
        /// return the nbr of data points in the table
        /// </summary>
        /// <param name="TblName"></param>
        /// <param name="col">column to read</param>
        /// <returns>number of data points read</returns>
        protected int ReadTableData(string TblName, string tblPath, int col)
        {
            string tfn = tblPath;
            if (!Directory.Exists(tfn))
            {
                tfn = OptiUtils.References;      // try plain installation directory
            }

            tfn += Path.DirectorySeparatorChar + TblName; // build the filename
            if (Path.GetExtension(tfn) == "")       // no extension: add the default .REF
            {
                tfn += ".REF";
            }

            if (!File.Exists(tfn))
            {
                // ask user for file location
                OptiUtils.OptiThermMessageSend("can't find reference file: " + TblName);
                return 0;
            }
            if (tfn != "")
            {
                // open the file and get the contents
                string? dl;
                string[] clms;
                var culture = Application.CurrentCulture;
                var txt = new StreamReader(tfn);
                if ((dl = txt.ReadLine()) != null)
                {
                    ArrayList dList = new ArrayList();
                    DataPair dp;
                    bool isv;
                    while ((dl = txt.ReadLine()) != null)
                    {
                        // read all the data
                        clms = dl.Split('\t');
                        if (clms.Length > 0)
                        {
                            dp.yv = 0; dp.xv = 0;
                            isv = float.TryParse(clms[0], NumberStyles.Float, CultureInfo.InvariantCulture,  out dp.xv);
                            int clrd = col < clms.Length ? col : 1;
                            if (isv)
                            {
                                isv = float.TryParse(clms[clrd], NumberStyles.Float, CultureInfo.InvariantCulture, out dp.yv);
                            }
                            if (isv)
                            {
                                dList.Add(dp); // add the data pair to the list
                            }
                        }
                    }
                    if (dList.Count > 0)
                    {
                        wvl = new List<double>();
                        Int = new List<double>(); // create the arrays
                        for (int i = 0; i < dList.Count; i++)
                        {
                            dp = (DataPair)dList[i];
                            wvl.Add(dp.xv);
                            Int.Add(dp.yv);
                        }
                        sorted = false;
                        AName = TblName;  // set the name of the arrays
                        AName = Path.GetFileNameWithoutExtension(tfn);
                        isPercent = false; //no percent mode in this case
                        return wvl.Count;
                    }
                }
            }
            return 0;
        }       // end of Rd Table

    }


}
