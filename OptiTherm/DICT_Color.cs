﻿﻿/* ====================================================================
 *
 * Author: Anton Dietrich
 * Date:
 *
 * Copyright (C) 2008-2022 Anton Dietrich
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 Modifications:
 * 2011-08-05: added color difference calculations CIE94 and CIELAB2000 in "CIE_Tables"
 * 2011-08-09: added CRI calculation according to EN410
 * 2011-09-06: add option to calculate color differences in "ColorSet"
 * 2011-09-12: upgrade alglib to 3.4 -> no DLL, direct links to source code
 * 2011-09-19: moved Measdata class to DICT.color from RDDataFile
 * 2011-11-15: corrected DE2000 calculation: dH term was missing
 * 2012-04-20: correction in color calculation: low value exceptions adjusted
 * 2013-08-05: add alglib based rbf_ml smoothing to spectraldata
 * 2022-03-19: transfer module to OptiTherm library
 *  ======================================================================
*/

using System.Reflection;
using System;
using System.Collections.Generic;
using System.Data;

namespace OptiTherm
{
    /// <summary>
    /// CIE ligh sources
    /// </summary>
    public enum CIELightSources { none, A, C, D65 };
    /// <summary>
    /// CIE Observers
    /// </summary>
    public enum CIEObservers { none, CIE_1931, CIE_1964 };
    /// <summary>
    /// color reference unit
    /// </summary>
    public enum ColorType {
        TR, //!< transmittance color
        RF, //!< reflectance film side color
        RG, //!< reflectance glass side color
        IGU_Rout, //!< outside reflectance IG unit color
        IGU_Rin,  //!< inside reflectance IG unit color
        Ref //!< reference color for delta E calculations
    };
    /// <summary>
    /// measurement spectra type
    /// </summary>
    public enum MeasType {
        NA, //!< no data found
        RG, //!< glass side reflectance
        RF, //!< film side reflectance
        TR, //!< transmittance
        PSI, //!< ellipsometry psi values
        DELTA, //!< ellipsometry delta values
        RGAA, //!< glass side angular data
        RFAA  //!< film side angular data
    };
    public enum SortDirection { Asc, Desc };  // sort order use in Comparer for ArrayList Sorter


    /// <summary>
    /// hold a CIE L*, a*, b* color data set: calculates Y if just L* is available
    /// </summary>
    [Serializable]
    public struct LabYData
    {
        public CIELightSources ls;
        public CIEObservers obs;
        public ColorType TRType;        //!< type of color: T/RF/RG
        double Yv;
        double Lv;
        public double astar;    //!< CIE a*
        public double bstar;    //!< CIE b*
        public double Y //!< CIE Y: calculated if only L* is present
        {
            get
            {   // calculate Y from L* if not present
                if ((Yv < 0.001) && (Lv > 0)) { return Math.Exp(Math.Log((Lv + 16) / 116) * 3) * 100; }
                else
                {
                    return Yv;
                }
            }
            set
            {
                Yv = value;
                Lv = (116.0 * Math.Pow(Yv / 100.0, (1.0 / 3.0)) - 16);
            } // set the corresponding L* in case of a change

        }
        public double Lstar //!< CIE L*
        {
            get
            {   // calculate L* from Y if not present:  116 * (Y / 100) ^ (1 / 3) - 16
                if ((Lv < 0.01) && (Yv > 0)) 
                { 
                    return Yv > 0.008856 ? (116.0 * Math.Pow(Yv / 100.0, (1.0 / 3.0)) - 16): 903.3*Yv; 
                }
                else
                {
                    return Lv;
                }
            }
            set
            {
                Lv = value;
                Yv = Math.Exp(Math.Log((Lv + 16) / 116) * 3) * 100;
            }
        }
        public double X;    //!< CIE X
        public double Z;    //!< CIE Z
        public double u
        {
            get { return 4.0 * X / (X + 15.0 * Y + 3.0 * Z); }
        }
        public double v
        {
            get
            {
                return 6.0 * Y / (X + 15.0 * Y + 3.0 * Z);
            }
        }

        public double x
        {
            get
            {
                return X / (X + Y + Z);
            }
        }
        public double y
        {
            get
            {
                return Y / (X + Y + Z);
            }
        }

        /// <summary>
        /// get CIE Delta E on reference
        /// </summary>
        /// <param name="reference">reference color </param>
        /// <returns>CIE delta E</returns>
        public double DeltaE(LabYData reference)
        {
            return Math.Sqrt(Math.Pow(reference.Lstar - this.Lstar, 2) + Math.Pow(reference.astar - this.astar, 2) +
                             Math.Pow(reference.bstar - this.bstar, 2));
        }
        /// <summary>
        /// CIE delta C: color difference
        /// </summary>
        /// <param name="reference">reference color</param>
        /// <returns>CIE delta C</returns>
        public double DeltaC(LabYData reference)
        {
            return Math.Sqrt(Math.Pow(reference.astar - this.astar, 2) +
                             Math.Pow(reference.bstar - this.bstar, 2));
        }

        public string ClrType { get { return TRType.ToString(); } }
        private double deltaCIE;

        /// <summary>
        /// CIE 2000 color difference: delta E00
        /// </summary>
        public double dCIE00
        {
            get { return deltaCIE; }
            set { deltaCIE = value; }
        }
        public double CRI;

    }


    /// <summary>
    /// class to decipher and hold a set of color data T,RF,RG for display in an ObjectListview
    /// </summary>
    public class ColorSet
    {
        ColorSet? RefColor;       // holds a reference color set if available during creation
        static string[] Hdrs = {"TrY","TrL","Tra", "Trb",
            "RfY", "RfL", "Rfa", "Rfb",
            "RgY", "RgL", "Rga", "Rgb"};
        // relabel in case for new front/back color labeling
        static string[] HdrsFB = {"TrY","TrL","Tra", "Trb",
            "RbkY", "RbkL", "Rbka", "Rbkb",
            "RftY", "RftL", "Rfta", "Rftb",
            "CRI-T", "dE00"};

        /// <summary>
        /// return a header label for front/back labeling
        /// </summary>
        /// <param name="pos">position of value in list</param>
        /// <returns>title of color value</returns>
        public static string HdrYLab(int pos)
        {
            if (pos < Hdrs.Length && pos >= 0)
            {
                return Hdrs[pos];
            }
            else
            {
                return "NA";
            }
        }
        /// <summary>
        /// return a header label for front/back labeling
        /// </summary>
        /// <param name="pos">position 0-8</param>
        /// <returns>color label</returns>
        public static string HdrYLabFB(int pos)
        {
            if (pos < HdrsFB.Length && pos >= 0)
            {
                return HdrsFB[pos];
            }
            else
            {
                return "NA";
            }
        }

        /// <summary>
        /// return the color value for the given header title
        /// </summary>
        /// <param name="hdr">tile as in HdrYLab or HdrYab</param>
        /// <returns></returns>
        public double? cval(string hdr)
        {
            Type cst = this.GetType();
            PropertyInfo? cstinf = cst.GetProperty(hdr);
            return cstinf == null ? 0 : (double)cstinf.GetValue(this, null);
        }
        public static string HdrYab(int pos)
        {
            if (pos > 8 || pos < 0)
            {
                return "NA";
            }

            int rp = (pos % 3) > 0 ? 1 : 0; // it is the Y position - otherwise the a*,b*
            int set = pos + rp + (int)Math.Floor(pos / 3.0);     // gets the new string position
            return Hdrs[set];
        }

        /// <summary>
        /// create a set of color values out of LabYData structure
        /// </summary>
        /// <param name="TR">transmission</param>
        /// <param name="RF">film side reflection</param>
        /// <param name="RG">glass side reflection</param>
        public ColorSet(LabYData TR, LabYData RF, LabYData RG)
        {   // assign the color values to local data
            this.TRD = TR;
            this.RFD = RF;
            this.RGD = RG;
        }

        /// <summary>
        /// create a set of color values out of LabYData structure
        /// </summary>
        /// <param name="TR">transmission</param>
        /// <param name="RF">film side reflection</param>
        /// <param name="RG">glass side reflection</param>
        /// <param name="label">title</param>
        public ColorSet(LabYData TR, LabYData RF, LabYData RG, string label)
        {   // assign the color values to local data
            this.Label = label;
            this.TRD = TR;
            this.RFD = RF;
            this.RGD = RG;
        }

        /// <summary>
        /// assign the color values to local data and calculate color differences
        /// </summary>
        /// <param name="TR">transmission</param>
        /// <param name="RF">film side reflection</param>
        /// <param name="RG">glass side reflection</param>
        /// <param name="label">title </param>
        /// <param name="reference">reference color</param>
        public ColorSet(LabYData TR, LabYData RF, LabYData RG, string label, ColorSet reference)
        {   // assign the color values to local data and calculate color differences
            this.Label = label;
            this.TRD = TR;
            this.RFD = RF;
            this.RGD = RG;
            TRD.dCIE00 = CIEColor.dE_2000(reference.ColorTR, TRD);
            RFD.dCIE00 = CIEColor.dE_2000(reference.ColorRF, RFD);
            RGD.dCIE00 = CIEColor.dE_2000(reference.ColorRG, RGD);
            RefColor = reference;
        }
        public string Label { get; set; } = String.Empty;
        private LabYData TRD, RFD, RGD;

        /// <summary>
        /// chnage the current ligh source and/or observer
        /// </summary>
        /// <param name="ls">light source</param>
        /// <param name="obs">observer</param>
        public void ModifyLightSource(CIELightSources ls, CIEObservers obs)
        {
            TRD.ls = ls;
            RFD.ls = ls;
            RGD.ls = ls;
            TRD.obs = obs;
            RFD.obs = obs;
            RGD.obs = obs;
        }
        // create properties
        public double TrY { get { return TRD.Y; } set { TRD.Y = value; } }
        public double TrL { get { return TRD.Lstar; } set { TRD.Lstar = value; } }
        public double Tra { get { return TRD.astar; } set { TRD.astar = value; } }
        public double Trb { get { return TRD.bstar; } set { TRD.bstar = value; } }
        public double CRI { get { return TRD.CRI; } set { TRD.CRI = value; } }
        public double dE00_T
        {
            get { return TRD.dCIE00; }
            set { TRD.dCIE00 = value; }
        }

        /// <summary>
        /// delta E transmitted color
        /// </summary>
        public double deltaET { get { return TRD.DeltaE(RefColor.ColorTR); } }
        // film side reflection
        public double RfY { get { return RFD.Y; } set { RFD.Y = value; } }
        public double RfL { get { return RFD.Lstar; } set { RFD.Lstar = value; } }
        public double Rfa { get { return RFD.astar; } set { RFD.astar = value; } }
        public double Rfb { get { return RFD.bstar; } set { RFD.bstar = value; } }

        /// <summary>
        /// delta E 2000 front side color
        /// </summary>
        public double dE00_B
        {
            get { return RFD.dCIE00; }
            set { RFD.dCIE00 = value; }
        }

        /// <summary>
        /// delta E 2000 backside color
        /// </summary>
        public double deltaEBk { get { return RFD.DeltaE(RefColor.ColorRF); } }
        // glass side reflection
        public double RgY { get { return RGD.Y; } set { RGD.Y = value; } }
        public double RgL { get { return RGD.Lstar; } set { RGD.Lstar = value; } }
        public double Rga { get { return RGD.astar; } set { RGD.astar = value; } }
        public double Rgb { get { return RGD.bstar; } set { RGD.bstar = value; } }
        public double dE00_F
        {
            get { return RGD.dCIE00; }
            set { RGD.dCIE00 = value; }
        }
        public double deltaEFt { get { return RGD.DeltaE(RefColor.ColorRG); } }

        /// <summary>
        /// sheet resistance
        /// </summary>
        public double RSQR { get; set; } = 0.0;


        /// <summary>
        /// use an indexer to return or set values directly
        /// preserve sequence: T->L*,a*,b* RF(..), RG(..)
        /// </summary>
        /// <param name="i">index of table</param>
        /// <returns>color value</returns>
        public double this[int i]
        {
            get
            {
                double rv;
                switch (i)
                {
                    case 0: rv = TRD.Lstar; break;
                    case 1: rv = TRD.astar; break;
                    case 2: rv = TRD.bstar; break;
                    case 3: rv = RFD.Lstar; break;
                    case 4: rv = RFD.astar; break;
                    case 5: rv = RFD.bstar; break;
                    case 6: rv = RGD.Lstar; break;
                    case 7: rv = RGD.astar; break;
                    case 8: rv = RGD.bstar; break;
                    default: rv = 0; break;
                }
                return rv;
            }
            set
            {
                switch (i)
                {
                    case 0: TRD.Lstar = value; break;
                    case 1: TRD.astar = value; break;
                    case 2: TRD.bstar = value; break;
                    case 3: RFD.Lstar = value; break;
                    case 4: RFD.astar = value; break;
                    case 5: RFD.bstar = value; break;
                    case 6: RGD.Lstar = value; break;
                    case 7: RGD.astar = value; break;
                    case 8: RGD.bstar = value; break;
                    default: return;
                }
            }
        }

        public LabYData ColorTR { get { return TRD; } } //!< transmitted color
        public LabYData ColorRF { get { return RFD; } } //!< film side reflection color
        public LabYData ColorRG { get { return RGD; } } //!< glass side reflection color


        public double DeltaETR(ColorSet ClrRef)
        {
            return TRD.DeltaE(ClrRef.ColorTR);
        }

        public double DeltaERF(ColorSet ClrRef)
        {
            return RFD.DeltaE(ClrRef.ColorRF);
        }

        public double DeltaERG(ColorSet ClrRef)
        {
            //			double tst = CIE2000_test();	// just a verification test
            return RGD.DeltaE(ClrRef.ColorRG);

        }

    }


    /// <summary>
    /// Class to handle Color calculations and conversions
    /// </summary>
    public static class CIEColor
    {
        // use a common set of reference tables: will be overwritten as required
        static private ReferenceTable? Ill = null, CIEX = null, CIEY = null, CIEZ = null, smel = null;
        static private CIELightSources Ill_Loaded = CIELightSources.none;
        static private CIEObservers Observer = CIEObservers.none;
        // hold the CRI test sample spectra
        static private List<ReferenceTable> CRIreferences = new();
        /// <summary>
        /// CRI reference values U*, V*, W* for light source D65 color test samples
        /// </summary>
        private static readonly double[,] CRI_UVR = new double[,]
        {
            {31.92, 8.41, 60.48},
            {15.22, 23.76,59.73},
            {-8.34, 36.29, 61.08},
            {-33.29, 18.64, 60.25},
            {-26.82, -6.55, 61.41},
            {-18.80, -28.8, 60.52},
            {9.77, -26.5, 60.14},
            {28.78, -16.24, 61.83}
        };

        private static readonly string MelanopicSpectrum = "MelanopicSpF5";

        /// <summary>
        /// static class to get references to the table file names for Illuminants and Observers
        /// </summary>
        private static class CIE_Tables
        {
            public const string CIE_31X = "CIE31X", CIE_31Y = "CIE31Y", CIE_31Z = "CIE31Z";
            public const string CIE_64X = "CIE64X", CIE_64Y = "CIE64Y", CIE_64Z = "CIE64Z";
            public const string IllumA = "ILLUMA", IllumC = "ILLUMC", IllumD65 = "ILLUMD65";
            public const string CRI_test_Samples = "CRI-Sample-Spectra";
        }

        /// <summary>
        /// returns Illuminant file name (no extension)
        /// </summary>
        /// <param name="ls">Illuminant</param>
        /// <returns></returns>
        public static string IlluminantFile(CIELightSources ls)
        {
            string rst;
            switch (ls)
            {
                case CIELightSources.A:
                    rst = CIE_Tables.IllumA;
                    break;
                case CIELightSources.C:
                    rst = CIE_Tables.IllumC;
                    break;
                case CIELightSources.D65:
                    rst = CIE_Tables.IllumD65;
                    break;
                default:
                    rst = "";
                    break;

            }
            return rst;
        }

        /// <summary>
        /// returns CIE reference for color calculation for given observer mode
        /// </summary>
        /// <param name="obs">observer</param>
        /// <param name="XYZ">required table X,Y,Z</param>
        /// <returns></returns>
        private static string CIEStdObserver(CIEObservers obs, char XYZ)
        {
            string rst;
            if (obs == CIEObservers.CIE_1931)
            {
                // use 2° observer
                rst = XYZ switch
                {
                    'X' => CIE_Tables.CIE_31X,
                    'Y' => CIE_Tables.CIE_31Y,
                    'Z' => CIE_Tables.CIE_31Z,
                    _ => "",
                };
            }
            else
            {// use 10° observer
                rst = XYZ switch
                {
                    'X' => CIE_Tables.CIE_64X,
                    'Y' => CIE_Tables.CIE_64Y,
                    'Z' => CIE_Tables.CIE_64Z,
                    _ => "",
                };
            }
            return rst;
        }


        /// <summary>
        /// verify if tables for Illuminant and Observer are loaded
        /// if not load the correct tables
        /// </summary>
        /// <param name="ls">Illumninant</param>
        /// <param name="obs">Observer</param>
        /// <returns></returns>
        private static bool VerifyTables(CIELightSources ls, CIEObservers obs)
        {
            if (ls == CIELightSources.none || obs == CIEObservers.none)
            {
                return false; // no valid selection
            }

            bool result = true;
            if (ls != Ill_Loaded)
            {
                // load correct illuminant Table
                try
                {
                    Ill = new ReferenceTable(IlluminantFile(ls));
                }
                catch
                {
                    OptiUtils.OptiThermMessageSend("Can't read Illuminant for color calculation: Error reading Reference");
                    result = false;
                }
                if (Ill.AName == IlluminantFile(ls)) { Ill_Loaded = ls; }
            }
            // now try to load required references for color calculation: X,Y,Z
            if (obs != Observer)
            {
                try
                {
                    CIEX = new ReferenceTable(CIEStdObserver(obs, 'X'));
                    CIEY = new ReferenceTable(CIEStdObserver(obs, 'Y'));
                    CIEZ = new ReferenceTable(CIEStdObserver(obs, 'Z'));
                    Observer = obs;
                    result &= true;
                }
                catch
                {
                    OptiUtils.OptiThermMessageSend("Can't read reference files for color calculation: Error reading Reference");
                    result = false;
                }

            }

            return result;
        }

        /// <summary>
        /// calculate the melanopic impact value for the given spectrum
        /// based on pre-liminary standard: DIN/TS 5031-100
        /// </summary>
        /// <param name="spd">table name for the melanopic spectrum</param>
        /// <returns>TM or RM based on D65 spectral data </returns>
        public static double MelanopticImpact(SpectralData spd)
        {
            if(smel == null)
            {
                try
                {
                    smel = new ReferenceTable(MelanopicSpectrum);
                }
                catch
                {
                    OptiUtils.OptiThermMessageSend("Can't find table for melanoptic spectral power: Error reading Reference");
                    return 0.0;
                }
            }
            double sum = 0;
            double smsum = 0;
            for (int i = 0; i < smel.Elements; i++)
            {
                var wl = smel[i];
                var ssm = smel.GetYval(wl);
                sum += ssm * spd.GetYval(wl);
                smsum += ssm;
            }
            var SM = sum / smsum;
            return SM;
        }

        public static LabYData CalcCIEcolor(SpectralData spd, CIELightSources ls, CIEObservers obs)
        {
            // calculate CIE L*, a*, b* color for a given Spectral Data set
            LabYData clr = new LabYData();
            if (spd[0] > 380 || spd[spd.Elements - 1] < 740)
            {
                OptiUtils.OptiThermMessageSend("Spectral data too short for color calculation!: CIE color calculation");
                return clr;
            }

            if (VerifyTables(ls, obs))
            {
                double Xn = 0, Yn = 0, Zn = 0;
                double X = 0, Y = 0, Z = 0;
                double dl = 5.0;    // delta Lambda for calculation
                for (int w = 380; w < 785; w += (int)dl)
                {
                    double phi = Ill.GetYval(w); // * dl;
                    Yn += phi * CIEY.GetYval(w);
                    Xn += phi * CIEX.GetYval(w);
                    Zn += phi * CIEZ.GetYval(w);
                    phi *= spd.GetYvalPercent(w) / 100.0;
                    X += phi * CIEX.GetYval(w);
                    Y += phi * CIEY.GetYval(w);
                    Z += phi * CIEZ.GetYval(w);
                }
                double k = 100.0 / Yn;
                Xn *= k;
                Yn *= k;
                Zn *= k;
                X *= k;
                Y *= k;
                Z *= k;
                double Xr, Yr, Zr;
                if (X / Xn < 0.008856) // use exception calculation for LAB
                {
                    Xr = 7.87 * (X / Xn) + 16.0 / 116.0;
                }
                else
                {
                    Xr = Math.Pow((X / Xn), 1.0 / 3.0);
                }
                // use exception calculation for L*: based on Hunt page 275
                if (Y / Yn <= 0.008856)
                {
                    Yr = 7.87 * (Y / Yn) + 16.0 / 116.0;
                    clr.Lstar = 903.3 * Y / Yn;
                }
                else
                {
                    Yr = Math.Pow((Y / Yn), 1.0 / 3.0);
                    clr.Lstar = 116.0 * Math.Pow((Y / Yn), 1.0 / 3.0) - 16.0;
                }
                if (Z / Zn < 0.008856) // use exception calculation for LAB
                {
                    Zr = 7.87 * (Z / Zn) + 16.0 / 116.0;
                }
                else
                {
                    Zr = Math.Pow((Z / Zn), 1.0 / 3.0);
                }
                // use exception calculation for a* / b* if any ratio is smaller
                if ((X / Xn <= 0.008856) || (Y / Yn <= 0.008856) || (Z / Zn <= 0.008856))
                {
                    //						clr.Lstar = 903.3 * Y / Yn;
                    clr.astar = 500.0 * (Xr - Yr);
                    clr.bstar = 200.0 * (Yr - Zr);
                }
                else
                {
                    //						clr.Lstar = 116.0 * Math.Pow((Y / Yn), 1.0 / 3.0) - 16.0;
                    clr.astar = 500 * (Math.Pow((X / Xn), 1.0 / 3.0) - Math.Pow((Y / Yn), 1.0 / 3.0));
                    clr.bstar = 200 * (Math.Pow((Y / Yn), 1.0 / 3.0) - Math.Pow((Z / Zn), 1.0 / 3.0));
                }

                clr.Y = Y;
                clr.X = X;
                clr.Z = Z;
                clr.obs = obs;
                clr.ls = ls;
            }
            clr.TRType = spd.TypeOfSpectra;
            clr.dCIE00 = 0.0;   // set it to 0 as default if no difference is calculated
            clr.ls = ls;
            clr.obs = obs;
            return clr;
        }

        /// <summary>
        /// returns a delta E
        /// </summary>
        /// <param name="spA">color value</param>
        /// <param name="spB">reference color value</param>
        /// <returns>CIE delta E</returns>
        public static double DeltaEstar(LabYData spA, LabYData spB)
        {
            // calculate a delta E for two color pairs
            double de = 0;
            de += Math.Pow(spA.Lstar - spB.Lstar, 2.0);
            de += Math.Pow(spA.astar - spB.astar, 2.0);
            de += Math.Pow(spA.bstar - spB.bstar, 2.0);
            return Math.Sqrt(de);
        }

        private static bool sameColorScale(LabYData one, LabYData two)
        {
            if (one.ls != two.ls || one.obs != two.obs)
            {
                OptiUtils.OptiThermMessageSend("Different color scales detected!: Color difference calculation");
                return false; // not same color scale
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// calculate color difference according to CIE94: CIE TC 1-29
        /// based on Hunter "Easy Match QC" manual
        /// </summary>
        /// <param name="reference">reference color</param>
        /// <param name="sample">sample color </param>
        /// <returns>delta E94</returns>
        public static double dE_CIE94(LabYData reference, LabYData sample)
        {
            // return the color difference to a reference sample based on CIE-TC 1-29
            const double Kl = 1.0;  // weighting ratios: coatings 1:1.1
            const double Kc = 1.0;
            const double Kh = 1.0;
            if (!sameColorScale(sample, reference))
            {
                return -1.0;
            }
            double dL = sample.Lstar - reference.Lstar;
            double CstarStd = Math.Sqrt(Math.Pow(reference.astar, 2.0) + Math.Pow(reference.bstar, 2.0));
            double dC = Math.Sqrt(Math.Pow(sample.astar, 2.0) + Math.Pow(sample.bstar, 2.0)) - CstarStd;
            double dE = sample.DeltaE(reference);
            double dH = 0;
            if (Math.Pow(dE, 2) > (Math.Pow(dC, 2.0) + Math.Pow(dL, 2.0)))
            {
                dH = Math.Sqrt(Math.Pow(dE, 2.0) - Math.Pow(dC, 2.0) - Math.Pow(dL, 2.0));
            }

            double sc = 1.0 + 0.045 * CstarStd;
            double sh = 1.0 + 0.015 * CstarStd;
            double de94 = Math.Sqrt(Math.Pow(dL / Kl, 2.0) + Math.Pow(dC / sc / Kc, 2.0) + Math.Pow(dH / Kh / sh, 2.0));
            return de94;
        }

        /// <summary>
        /// calculate the color difference according to CIE DE2000
        /// based on paper from Sharma, Wu and Dalal
        /// </summary>
        /// <param name="reference">reference  color</param>
        /// <param name="sample">sample color</param>
        /// <returns>delta E00</returns>
        public static double dE_2000(LabYData reference, LabYData sample)
        {
            // return the color difference to a reference sample based on CIE-TC 1-29
            const double Kl = 1.0;  // weighting ratios: coatings 1:1.1
            const double Kc = 1.0;
            const double Kh = 1.0;
            const double zerotol = 0.00001; // assumed to be zero
            double pi2rad = Math.PI / 180.0;        // convert to radians for sin/cos
            if (!sameColorScale(sample, reference))
            {
                return -1.0;
            }
            double dL = sample.Lstar - reference.Lstar;
            // calculate the C / h values for the two samples
            double CabAvg = (Math.Sqrt(Math.Pow(reference.astar, 2.0) + Math.Pow(reference.bstar, 2.0)) +
                             Math.Sqrt(Math.Pow(sample.astar, 2.0) + Math.Pow(sample.bstar, 2.0))) / 2.0;
            double G = 0.5 * (1.0 - Math.Sqrt(Math.Pow(CabAvg, 7.0) / (Math.Pow(CabAvg, 7.0) + Math.Pow(25.0, 7.0))));
            double a1d = (1.0 + G) * reference.astar;
            double a2d = (1.0 + G) * sample.astar;
            double C1d = Math.Sqrt(a1d * a1d + reference.bstar * reference.bstar);
            double C2d = Math.Sqrt(a2d * a2d + sample.bstar * sample.bstar);
            double h1d = Math.Abs(reference.bstar) < zerotol && Math.Abs(a1d) <= zerotol ?
                0.0 : Math.Atan2(reference.bstar, a1d);
            double h2d = Math.Abs(sample.bstar) < zerotol && Math.Abs(a2d) <= zerotol ?
                0.0 : Math.Atan2(sample.bstar, a2d);
            h1d = h1d < 0 ? h1d + Math.PI * 2.0 : h1d;  // shift to 0-360° from -180 to +180
            h2d = h2d < 0 ? h2d + Math.PI * 2.0 : h2d;
            h1d *= 180.0 / Math.PI;     // convert to degree
            h2d *= 180.0 / Math.PI;
            double dLd = sample.Lstar - reference.Lstar;
            // condition for C1d*C2d == 0
            double dCd = C2d - C1d;
            double dhd = 0.0;
            double hdAvg = (h1d + h2d);
            // sort out the dhd and hdavg depending on nonzero C1d*C2d
            if (!(Math.Abs(C1d * C2d) < zerotol))
            {
                if (Math.Abs(h2d - h1d) <= 180.0)
                {
                    dhd = h2d - h1d;
                    hdAvg = (h1d + h2d) / 2.0;
                }
                else
                {
                    if (h2d - h1d > 180.0)
                    {
                        dhd = h2d - h1d - 360.0;
                    }
                    else if (h2d - h1d < -180)
                    {
                        dhd = h2d - h1d + 360.0;
                    }
                    // now treat hdavg
                    if (Math.Abs(h1d - h2d) > 180)
                    {
                        if (h2d + h1d < 360)
                        {
                            hdAvg = (h1d + h2d + 360.0) / 2.0;
                        }
                        else
                            if (h2d + h1d >= 360)
                        {
                            hdAvg = (h1d + h2d - 360.0) / 2.0;
                        }
                    }
                }
            }
            double dHd = 2.0 * Math.Sqrt(C1d * C2d) * Math.Sin(dhd / 2.0 * pi2rad);
            // now put the color difference together
            double LdAvg = (reference.Lstar + sample.Lstar) / 2.0;
            double CdAvg = (C1d + C2d) / 2.0;
            double T = 1.0 - 0.17 * Math.Cos((hdAvg - 30.0) * pi2rad) + 0.24 * Math.Cos((2.0 * hdAvg) * pi2rad) +
                0.32 * Math.Cos((3.0 * hdAvg + 6.0) * pi2rad) - 0.20 * Math.Cos((4.0 * hdAvg - 63.0) * pi2rad);
            double dTheta = 30.0 * Math.Exp(-1.0 * Math.Pow((hdAvg - 275.0) / 2.0, 2.0));
            double Rc = 2.0 * Math.Sqrt(Math.Pow(CdAvg, 7.0) / (Math.Pow(CdAvg, 7.0) + Math.Pow(15.0, 7.0)));
            double Sl = 1.0 + (0.015 * Math.Pow(LdAvg - 50.0, 2.0) / Math.Sqrt(20.0 + Math.Pow(LdAvg - 50.0, 2.0)));
            double Sc = 1.0 + 0.045 * CdAvg;
            double Sh = 1.0 + 0.015 * CdAvg * T;
            double RT = -1.0 * Math.Sin((2.0 * dTheta) * pi2rad) * Rc;
            double dE00 = Math.Sqrt(Math.Pow(dLd / Kl / Sl, 2.0) + Math.Pow(dCd / Kc / Sc, 2.0) +
                                    Math.Pow(dHd / Kh / Sh, 2.0) + RT * (dCd / Kc / Sc) * (dHd / Kh / Sh));
            return dE00;
        }

        private static double ctf(double u, double v)
        {
            if (v == 0)
            {
                return 0;
            }

            double _ct = (4.0 - u - 10.0 * v) / v;
            return _ct;
        }

        private static double dtf(double u, double v)
        {
            if (v == 0)
            {
                return 0;
            }

            double _dt = (1.708 * v + 0.404 - 1.481 * u) / v;
            return _dt;
        }

        private static double uf(double X, double Y, double Z)
        {
            double cv = 4.0 * X / (X + 15.0 * Y + 3.0 * Z);
            return cv;
        }
        private static double vf(double X, double Y, double Z)
        {
            double cv = 6.0 * Y / (X + 15.0 * Y + 3.0 * Z);
            return cv;
        }

        private static bool XYZ(SpectralData sp, out double X, out double Y, out double Z)
        {
            // calculate the X, Y Z raw values for a given spectrum: CRI
            X = 0; Y = 0; Z = 0;
            if (!VerifyTables(CIELightSources.D65, CIEObservers.CIE_1931))
            {
                return false;
            }

            double dl = 10.0;    // delta Lambda for CRI calculation
            for (int w = 380; w < 790; w += (int)dl)
            {
                double phi = Ill.GetYval(w);    // light source data have already the dl included
                phi *= sp.GetYvalPercent(w) / 100.0;
                X += CIEX.GetYval(w) * phi;
                Y += CIEY.GetYval(w) * phi;
                Z += CIEZ.GetYval(w) * phi;
            }
            return true;
        }


        /// <summary>
        /// calculate the CRI according to the EN410:1998 procedure
        /// </summary>
        /// <param name="sample">spectral data for sample</param>
        /// <returns>CRI</returns>
        public static double CRI_EN410(SpectralData sample)
        {
            if (CRIreferences.Count < 8)
            {
                // get the sample references if not yet loaded
                for (int i = 0; i < 8; i++)
                {
                    ReferenceTable rt = new(CIE_Tables.CRI_test_Samples, i + 1);
                    CRIreferences.Add(rt);
                }
            }
            // test the calculation using the EN green glass data
            //			SpectralData tstsample = new ReferenceTable("GreenTestLiteEN410");
            //			sample = tstsample;
            XYZ(sample, out double Xt, out double Yt, out double Zt);    // get the XYZ coordiantes based on 10 nm steps
            double ct = ctf(uf(Xt, Yt, Zt), vf(Xt, Yt, Zt));
            double dt = dtf(uf(Xt, Yt, Zt), vf(Xt, Yt, Zt));
            double Ra = 0.0;    // final Ra
            double[] wl = new double[sample.Elements + 1];
            double[] vl = new double[sample.Elements + 1];
            for (int i = 0; i < 8; i++)
            {
                // calculate the individual sample colors
                // create a spectral data set with the modified spectrum
                for (int e = 0; e < sample.Elements - 1; e++)
                {
                    double wvl = sample[e];
                    double cv = sample.GetYval(wvl) * CRIreferences[i].GetYval(wvl);
                    wl[e] = wvl; vl[e] = cv;
                }
                SpectralData sps = new SpectralData(wl, vl, "Csample", ColorType.TR, false);
                XYZ(sps, out double Xi, out double Yi, out double Zi);
                double cti = ctf(uf(Xi, Yi, Zi), vf(Xi, Yi, Zi));
                double dti = dtf(uf(Xi, Yi, Zi), vf(Xi, Yi, Zi));
                double utdash = (10.872 + 0.8802 * cti / ct - 8.2544 * dti / dt) /
                    (16.518 + 3.2267 * cti / ct - 2.0636 * dti / dt);
                double vtdash = 5.520 / (16.518 + 3.2267 * cti / ct - 2.0636 * dti / dt);
                double Wti = 25.0 * Math.Pow(100.0 * Yi / Yt, 1.0 / 3.0) - 17.0;
                double Uti = 13.0 * Wti * (utdash - 0.1978);
                double Vti = 13.0 * Wti * (vtdash - 0.3122);
                double dEi = Math.Sqrt(Math.Pow(Uti - CRI_UVR[i, 0], 2.0) +
                                       Math.Pow(Vti - CRI_UVR[i, 1], 2.0) + Math.Pow(Wti - CRI_UVR[i, 2], 2.0));
                Ra += (100.0 - 4.6 * dEi);
            }
            Ra /= 8.0;
            return Ra;
        }

    }



}
